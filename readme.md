# Naming conventions

Please, distinguish between `Resource` (singular), `Resources` (plural).

## Presenters

- `createComponent___()`
- `formSuccess___()` - form success handlers
- `can...()` - a method validatingACL & current user


## Managers

### Setter
- `addResource()`
- `deleteResource()`
- `setResource...()` - set a field/group of data to an entity *setinstitutionAddress()*

### Getters

#### Single resource

- `getResource()` - a single entity
- `getResourceField()` - one field or group of data


#### Multiple resources

- `getResources()` - multiple entities
- `getResourcesBy...()`
- `getResourcesOf...()`

#### Related resources

- `getRelated...` - Foreign entities related to the resource
	- `getRelatedResources()`
	- `getRelatedResourcesBy...()`

#### Form options & defaults

- `getForm...` - returns data to CheckboxList or Select

	- `getFormOptions...()` - nested array with optgroups
	- `getFormArray...()` - plain array of resources
	- `getFormDefaults...()` - array of defailt data to forms

#### DataGrid Data

- `getAdmin...` - data for DataGrid
	- `getAdminResources()`



### Condition
- `is...()` - check one particular value
- `resourceExists()`
- `resourceHas...()`
- `resourceCan...()`

### Controle & Exception
- `...Check()` - a method that controls something and throws errors
- `resourceExistenceCheck()` - checks if a resource checks and throws `\App\Model\MissingItemException` or `\App\Model\MissingRelatedItemException`

### Never use:
- `getField()` => `getResourceField()`
- 
# Forms

## Exceptions

See `app/Models/Exceptions.php` for data-related exceptions thrown by models.

### `BaseException`

- `@param string $message` 
- `@param string $table` - DB table related to this error
- `@param int $code`
- `@param array $data` - array of additional data/parameters

## 'select one' and 'add_new' in forms

Allways use `BaseManager::init...Container()` to add default actions to options:
```
[
	0 => $select_one_label,
	"add" => $add_new_label,
	... (data from the DB)
]
```

`getFormOptions...()` should call `BaseManager::initOptionsContainer()`.

`getFormArray...()` should call `BaseManager::initArrayContainer()`.

In form handling: 
- `(int) 0` == nothing selected => error
- `(string) "add"` == a new entry should be added

# Development

## Requirements

- Web Project for Nette 3.1 requires PHP 7.2
- Local dev is recommended in a docker image.
- there are strange proto-migrations at `BasePresenter::checkDB`


## Installation

The best way to install Web Project is using Composer. If you don't have Composer yet,
download it following [the instructions](https://doc.nette.org/composer). Then use command:

	composer create-project nette/web-project path/to/install
	cd path/to/install


Make directories `temp/` and `log/` writable.


## Web Server Setup

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

	php -S localhost:8000 -t www

Then visit `http://localhost:8000` in your browser to see the welcome page.

For Apache or Nginx, setup a virtual host to point to the `www/` directory of the project and you
should be ready to go.

**It is CRITICAL that whole `app/`, `config/`, `log/` and `temp/` directories are not accessible directly
via a web browser. See [security warning](https://nette.org/security-warning).**


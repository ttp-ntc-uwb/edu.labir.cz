-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Počítač: database:3306
-- Vytvořeno: Pon 22. úno 2021, 10:49
-- Verze serveru: 5.7.27
-- Verze PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `edun`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `institution`
--

CREATE TABLE `institution` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `street` varchar(255) COLLATE latin2_czech_cs DEFAULT NULL,
  `psc` varchar(255) COLLATE latin2_czech_cs DEFAULT NULL,
  `orient` int(11) DEFAULT NULL,
  `ico` varchar(255) COLLATE latin2_czech_cs DEFAULT NULL,
  `note` longtext COLLATE latin2_czech_cs,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `institution_type`
--

CREATE TABLE `institution_type` (
  `institution_id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `locality`
--

CREATE TABLE `locality` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `slug` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `depth` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `navic`
--

CREATE TABLE `navic` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `slug` varchar(255) COLLATE latin2_czech_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE latin2_czech_cs NOT NULL COMMENT 'Used as login',
  `name` varchar(255) COLLATE latin2_czech_cs DEFAULT NULL,
  `surname` varchar(255) COLLATE latin2_czech_cs DEFAULT NULL,
  `password` varchar(255) COLLATE latin2_czech_cs DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_action`
--

CREATE TABLE `user_action` (
  `user_id` int(11) NOT NULL,
  `hash` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `type` int(11) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_institution`
--

CREATE TABLE `user_institution` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `institution_id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `role_other` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `note` varchar(255) COLLATE latin2_czech_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin2_czech_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_subject`
--

CREATE TABLE `user_subject` (
  `user_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `institution`
--
ALTER TABLE `institution`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `locality`
--
ALTER TABLE `locality`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `user_institution`
--
ALTER TABLE `user_institution`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `institution`
--
ALTER TABLE `institution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `locality`
--
ALTER TABLE `locality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `user_institution`
--
ALTER TABLE `user_institution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

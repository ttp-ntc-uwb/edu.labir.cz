import "./shared";

// Nette-forms
import "./js/vendor/nette-forms";

// Naja
import "./js/vendor/naja";

// DataGrid
import "./js/vendor/datagrid";
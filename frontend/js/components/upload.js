import $ from "jquery";

$(document).ready(function(){
  $("input[type=file]").on("input",function(){

    let filename = $(this).val().split(/(\\|\/)/g).pop();

    let container = $(this).closest(".custom-file");

    container.find(".custom-file-label").html(filename);
    
  });
});

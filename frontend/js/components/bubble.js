/**
 * Example component
 * 
 * Description of its purpose.
 */


import $ from "jquery";


$(document).ready(function(){
  // Run the code here

  let bQuotes = $(".t-bubble--quote");

  const quotes = [];

  bQuotes.each(function(){
    let obj = {};
    obj.quote = $(this);
    obj.wrapper = obj.quote.closest(".t-bubble");
    obj.more = obj.wrapper.find(".t-bubble--more");
    obj.button = obj.wrapper.find(".btn");
    obj.expanded = false;

    obj.button.on("click",function(){
      if ( obj.expanded ) {
        obj.expanded = false;
        obj.more.removeClass("t-bubble--more__expanded");
        $(this).html("Číst více");
      } else {
        obj.expanded = true;
        obj.more.addClass("t-bubble--more__expanded");
        $(this).html("Sbalit");
      }

    });

    quotes.push(obj);
  });

});
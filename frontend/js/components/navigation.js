/**
 * Custom navigation
 * 
 * Description of its purpose.
 */


import $ from "jquery";

$(document).ready(function(){
  
  const triggerActiveClass = "is-active";
  const containerActiveClass = "t-nav--part__right__expanded";
  const elementActiveClass = "t-nav__mobiexpand";

  const nav = {};

  // Main elements stored as jQuery objects
  nav.element = $(".t-nav");
  nav.trigger = $(nav.element.find(".t-nav--trigger")[0]);
  nav.hamburger = $(nav.element.find(".hamburger")[0]);
  nav.part = $(nav.element.find(".t-nav--part__right")[0]);
  nav.carets = [];

  let d = $(".t-nav--menu li span.expander");

  d.each(function(){
    let caret = {};
    caret.trigger = $(this);
    caret.li = caret.trigger.closest("li");
    caret.expanded = false;
    caret.toggle = function(){
      let activeClass = "expanded";
      if (caret.expanded) {
        caret.expanded = false;
        caret.li.removeClass(activeClass);
      } else {
        caret.expanded = true;
        caret.li.addClass(activeClass);
      }
    }
    nav.carets.push(caret);
  });

  nav.carets.map((caret)=>{
    caret.trigger.on("click",function(){
      caret.toggle();
    });
  });

  // Check if main elements are present
  nav.ready = nav.element.length>0 && nav.trigger.length>0 && nav.part.length>0 ? true : false;

  // Set properties
  nav.expanded = false;

  // Mobile toggling
  nav.toggleMobile = function(){
    if ( nav.expanded ) {
      nav.element.removeClass( elementActiveClass );
      nav.hamburger.removeClass( triggerActiveClass );
      nav.part.removeClass( containerActiveClass );
      nav.expanded = false;
    } else {
      nav.element.addClass( elementActiveClass );
      nav.hamburger.addClass( triggerActiveClass );
      nav.part.addClass( containerActiveClass );
      nav.expanded = true;
    }
  }

  // Proceed only when ready
  if ( nav.ready ) {

    nav.trigger.on("click", ()=>{
      nav.toggleMobile();
    });

  } else {
    console.log("nav is not ready");
  }

});
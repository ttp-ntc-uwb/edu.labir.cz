/**
 * Implementation of jQuery-datepicker
 */

import 'jquery-datetimepicker';
import $ from "jquery";

$(document).ready( function(){

  const datepickers = document.querySelectorAll(".t-datepicker");

  if ( datepickers.length > 0 ) {

    $.datetimepicker.setLocale( 'cs' );

    datepickers.forEach( ( item, index ) => {

      $( item ).datetimepicker({format:"Y-m-d H:i",formatDate:"Y-m-d",formatTime:"H:i"});

    } );

  }

});
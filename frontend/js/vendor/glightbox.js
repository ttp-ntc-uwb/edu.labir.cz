import $ from "jquery";

import GLightbox from "../../../node_modules/glightbox/dist/js/glightbox";

$(document).ready(function(){

    const lightbox = GLightbox({
        touchNavigation: true,
        loop: true,
        autoplayVideos: true,
        selector: ".t-glightbox"
    });

});
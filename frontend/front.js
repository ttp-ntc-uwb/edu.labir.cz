/**
 * Omnipresent frontend code
 * 
 */

// The frontend CSS
import "./scss/app.scss";

/**
 * 1. Vendor libraries
 */

// Shared code
import "./shared";

// Bootstrap 4.5.2
import "./js/vendor/bootstrap";

// Animate on scroll
import "./js/vendor/aos";

// jQuery Datepicker
import "./js/vendor/jquery-datepicker";

// Lazyloading
import "./js/vendor/lazyload";

// Simple lightbox
import "./js/vendor/simple-lightbox";

// Glightbox
import "./js/vendor/glightbox";

/**
 * 2. Frontend components
 */

// Menu functionality
import "./js/components/navigation";

// Bubble functionality
import "./js/components/bubble";


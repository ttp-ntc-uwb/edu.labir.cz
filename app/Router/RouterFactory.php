<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{

		$locale = '[<locale=cs cs|en>/]';

		$router = new RouteList;
		

		$router->addRoute( $locale . 'institution/edit/<id>', 'Institution:edit');

		/**
		 * Front: Static
		 */
		$router->addRoute( $locale . "edukit/streming", "Front:Static:stream" );
		$router->addRoute( $locale . "edukit/camera", "Front:Static:camera" );
		$router->addRoute( $locale . "edukit/software", "Front:Static:software" );
		$router->addRoute( $locale . "docs", "Front:Documentation:default" );
		$router->addRoute( $locale . "about", "Front:Static:about" );
		$router->addRoute( $locale . "about/involve", "Front:Static:zapojit" );
		$router->addRoute( $locale . "about/aims", "Front:Static:cileProjektu" );
		$router->addRoute( $locale . "about/activities", "Front:Static:aktivity" );
		$router->addRoute( $locale . "about/testimonials", "Front:Static:reference" );
		$router->addRoute( $locale . "about/team", "Front:Static:tym" );
		$router->addRoute( $locale . "contact", "Front:Static:contact" );
		$router->addRoute( $locale . "news", "Front:Static:aktualne" );
		$router->addRoute( $locale . "experiment/inspirations", "Front:Static:inspirations" );
		$router->addRoute( $locale . "news/termotalent", "Front:Static:soutez" );

		/**
		 * Front: Documentation
		 */
		$router->addRoute( $locale . "docs/camera", "Front:Documentation:camera" );
		$router->addRoute( $locale . "docs/camera/analysis", "Front:Documentation:analyzy" );
		$router->addRoute( $locale . "docs/camera/photos", "Front:Documentation:snimky" );
		$router->addRoute( $locale . "docs/camera/videos", "Front:Documentation:videa" );
		$router->addRoute( $locale . "docs/camera/gallery", "Front:Documentation:galerie" );
		$router->addRoute( $locale . "docs/camera/rtsp", "Front:Documentation:rtsp" );

		/**
		 * Front: Sign
		 */

		$router->addRoute( $locale . 'login', 'Front:Sign:login');

		$router->addRoute( $locale . 'logout', 'Front:Sign:logout');

		$router->addRoute( $locale . 'password/request', 'Front:Sign:request');

		$router->addRoute( $locale . 'password/restore/<hash>', 'Front:Sign:restore');

		$router->addRoute( $locale . 'register/confirm/<hash>', 'Front:Sign:registerConfirm');

		$router->addRoute( $locale . 'register', 'Front:Sign:register');

		/**
		 * Admin
		 */

		/**
		 * Admin: User
		 */

		$router->addRoute( $locale . 'admin/user', 'Admin:User:overview');

		$router->addRoute( $locale . 'admin/user/add', 'Admin:User:add');
		
		
		$router->addRoute( $locale . 'locality/edit/<id>', 'Locality:edit');
		
		$router->addRoute( $locale . 'locality/delete/<id>', 'Locality:delete');
		
		$router->addRoute( $locale . 'user/passrowd-reset[/<hash>]', 'User:passwordReset $hash');

		$router->addRoute( $locale . 'profile[/<id>]', 'User:detail');

		$router->addRoute( $locale . 'settings/base[/<id>]', 'Admin:User:settings');

		$router->addRoute( $locale . 'settings/password[/<id>]', 'Admin:User:settingsPassword');

		$router->addRoute( $locale . 'settings/subjects[/<id>]', 'Admin:User:settingsSubjects');

		$router->addRoute( $locale . 'settings/institutions/overview[/<id>]', 'Admin:User:settingsUserInstitutions');

		$router->addRoute( $locale . 'settings/institutions/add[/<id>]', 'Admin:User:settingsUserInstitutionAdd');

		$router->addRoute( $locale . 'settings/institutions/edit[/<id>]', 'Admin:User:settingsUserInstitutionEdit');




		/**
		 * Admin: Locality
		 */
		$router->addRoute( $locale . "admin/locality", "Admin:Locality:default" );

		$router->addRoute( $locale . "admin/locality/add-city", "Admin:Locality:addCity" );

		$router->addRoute( $locale . "admin/locality/add-region", "Admin:Locality:addRegion" );

		$router->addRoute( $locale . "admin/locality/add-country", "Admin:Locality:addCountry" );

		$router->addRoute( $locale . "admin/locality/edit/<id>", "Admin:Locality:edit" );

		$router->addRoute( $locale . "admin/locality/delete/<id>", "Admin:Locality:addCity" );

		/**
		 * Temporary experiments 
		 */

		$router->addRoute( $locale . "experiments", "Admin:Experiment:default" );

		/**
		 * Experiments
		 */
		$router->addRoute( $locale . "experiment/<slug>", "Admin:Experiment:detail" );

		$router->addRoute( $locale . "experiments/category/<slug>", "Admin:Experiment:category" );

		$router->addRoute( $locale . '<presenter>/<action>[/<id>]', 'Front:Static:default');

		$router->addRoute( $locale . '', "Front:Static:default" );


		return $router;
	}
}

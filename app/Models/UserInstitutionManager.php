<?php 

namespace App\Model;

use Nette;
use App\Model\BaseManager;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;

class UserInstitutionManager extends BaseManager
{

  const TABLE_BASE = "user_institution",
    TABLE_USER = "user",
    TABLE_INSTITUTION = "institution",
    REL_ID = "id",
    REL_USER_ID = "user_id",
    REL_INSTITUTION_ID = "institution_id",
    REL_ROLE = "role",
    REL_ROLE_OTHER = "role_other",
    REL_VERIFIED = "verified",
    REL_NOTE = "note";

  const ROLES = [
    0 => "other",
    1 => "student",
    2 => "pedagogue"
  ];

  /** @var Nette\Database\Explorer */
  private $database;
  
  /** @var Nette\Localisation\ITranslator */
  private $translator;


  public function __construct(
    Nette\Database\Explorer $database,
    Nette\Localization\Translator $translator
  )
	{
    $this->database = $database;
    $this->translator = $translator;
  }




  // Setters



  /**
   * @throws \App\Model\DupliciteItemException The relation already exists
   */
  public function addRelationBetween( int $user_id, int $institution_id, int $role, ?string $role_other = null, ?string $note = null, ?bool $verified = false, ?int $code = 0 ): ActiveRow
  {
    if ( $this->relationExists( $user_id, $institution_id ) ) {
      throw new DupliciteItemException("This relation already exists!", $code);
    }
    return $this->database->table( self::TABLE_BASE )
      ->insert( [
        self::REL_USER_ID => $user_id,
        self::REL_INSTITUTION_ID => $institution_id,
        self::REL_ROLE => $role,
        self::REL_ROLE_OTHER => $role_other,
        self::REL_NOTE => $note,
        self::REL_VERIFIED => $verified
      ] );
  }


  /**
   * Set a role to a relation
   * @param int $relation_id
   * @param int $role ID of the role - see `self::ROLES`
   * @param string|null $role_other This value shall save even if the value is null
   * @param int|null $code
   * @throws \App\Model\MissingItemException The relation does not exist
   */
  public function setRelationRole(int $relation_id, int $role, ?string $role_other = null, ?int $code = 0): void
  {
    
    // Check if the relation exists
    $this->checkRelationEntityExistence( $relation_id, $code );

    // Check if the role is valid
    if ( ! $this->isValidRole( $role ) ) {
      throw new DictionaryValidationException("The role is invalid!", $code, ["type"=>"invalid_role"]);
    }

    $this->database->table( self::TABLE_BASE )
      ->where( self::REL_ID, $relation_id )
      ->update([
        self::REL_ROLE => $role,
        self::REL_ROLE_OTHER => $role_other
      ]);
  }


  /**
   * Set a new user and institution to a given relation
   * @param int $relation_id
   * @param int $user_id
   * @param int @institution_id
   * @throws \App\Model\MissingItemException Relation between the two entities does not exist.
   */
  public function setRelationTargets( int $relation_id, int $user_id, int $institution_id, int $code = 0 ): void
  {

    // Check if the relation exists
    $this->checkRelationEntityExistence( $relation_id, $code );

    $this->database->table( self::TABLE_BASE )
      ->where( self::REL_ID, $relation_id )
      ->update([
        self::REL_USER_ID => $user_id,
        self::REL_INSTITUTION_ID => $institution_id
      ]);
  }

  public function setRelationVerification( int $relation_id, bool $state ): void
  {
    $this->database->table( self::TABLE_BASE )
      ->where( self::REL_ID, $relation_id )
      ->update([
        self::REL_VERIFIED => $state
      ]);
  }


  /**
   * Set a note to a relation
   * @param int $relation_id
   * @param string $note
   * @param int|null $code
   * @throws \App\Model\MissingItemException The relation does not exist.
   */
  public function setRelationNote( int $relation_id, string $note, ?int $code = 0 ): void
  {
    // Check if the relation exists
    $this->checkRelationEntityExistence( $relation_id, $code );

    $this->database->table( self::TABLE_BASE )
      ->where( self::REL_ID, $relation_id )
      ->update([
        self::REL_NOTE => $note
      ]);
  }

  public function deleteRelation( int $relation_id ): void
  {
    $this->database->table( self::TABLE_BASE )
      ->get( $relation_id )
      ->delete();
  }






  // GETTERS






  /**
   * Get a relation by its ID
   * @param int $relation_id
   * @return ActiveRow
   * @throws \App\Model\MissingItemException Relation does not exist
   */
  public function getRelation( int $relation_id, ?int $code = 0 ): ActiveRow
  {
    $data = $this->database->table( self::TABLE_BASE )
      ->get( $relation_id );

    if ( $data == null ) {
      throw new MissingItemException("Relationship does not exist", $code);
    }

    return $data;
  }


  /**
   * Get a relation between user and institution
   * @param int $user_id
   * @param int $institution_id
   * @param int|null $code
   * @return ActiveRow
   * @throws \App\Model\MissingItemException Relation between these two entities does not exist!
   */
  public function getRelationBetween( int $user_id, int $institution_id, ?int $code = 0 ): ActiveRow
  {

    $data = $this->database->table( self::TABLE_BASE )
      ->where( self::REL_USER_ID, $user_id )
      ->where( self::REL_INSTITUTION_ID, $institution_id);
    if ( $data->count() == 0 ) {
      throw new MissingItemException("Relation does not exist!", $code);
    }
    return $data->fetch();
  }

  public function getInstitutionName( int $institution_id ): string
  { 
    return $this->database->table( self::TABLE_INSTITUTION )
      ->get( $institution_id )
      ->name;
  }


  /**
   * Get all relations of a single user
   * @param int $user_id
   * @return Selection
   */
  public function getUserRelations( int $user_id ): Selection
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::REL_USER_ID, $user_id );
  }

  public function getUserRelationsToInstitutions( int $user_id ): array
  {
    $relations = $this->getUserRelations( $user_id );

    $output = [];

    foreach ( $relations as $relation ) {

      $rel = [
        "role" => $this->getUserRoleNameInInstitution( $relation->user_id, $relation->institution_id ),
        "institution_id" => $relation->institution_id,
        "user_id" => $user_id,
        "name" => $this->getInstitutionName( $relation->institution_id )
      ];

      $output[] = $rel;

    }

    return $output;
  }

  public function getAdminUserInstitutions( int $user_id ): array
  {
    $relations = $this->getUserRelations( $user_id );

    $output = [];

    foreach ( $relations as $relation ) {

      $institutionName = $this->database->table( self::TABLE_INSTITUTION )
        ->get( $relation->institution_id )
        ->name;

      $output[ $relation->id ] = [
        "id" => $relation->id,
        "role" => $this->getUserRoleNameInInstitution( $relation->user_id, $relation->institution_id ),
        "role_id" => $relation->role,
        "role_other" => $relation->role_other,
        "institution" => $institutionName
      ];

    }

    return $output;

  }


  /**
   * Get user role ID in a relationship to an institution
   * @param int $user_id
   * @param int $institution_id
   * @param int|null $code
   * @throws \App\Model\MissingItemException The relation does not exist!
   * @uses self::getRelationBetween()
   */
  public function getUserRoleInInstitution( int $user_id, $institution_id, int $code = 0 ): int
  {
    
    $relation = $this->getRelationBetween( $user_id, $institution_id, $code );
    
    return $relation->role;

  }


  /**
   * Get the user role text in an institution (translated or role_other)
   * @param int $user_id
   * @param int $institution_id
   * @param int|null $code
   * @throws \App\Model\MissingItemException The relation does not exist!
   * @uses self::getRelationBetween()
   */
  public function getUserRoleNameInInstitution( int $user_id, $institution_id, int $code = 0 ): string
  {
    
    $relation = $this->getRelationBetween( $user_id, $institution_id, $code );

    if ( $relation->role !== 0 ) {
      return $this->getRoleName( $relation->role );
    }

    return $relation->role_other;

  }


  /**
   * Get all user roles IDs
   * @param int $user_id
   * @return int[]
   */
  public function getUserRolesSum( int $user_id ): array
  {
    $data = $this->getUserRelations( $user_id );
    $output = [];
    foreach ( $data as $relation ) {
      if ( ! in_array( $relation->role, $output ) ) {
        $output[] = $relation->role;
      }
    }
    return $output;
  }


  /**
   * Return IDs of entities related to an entity
   * @param string $referenced_table self::TABLE_*
   * @param int $referencing_id ID
   * @return int[] Array of related entities IDs
   */
  public function getRelatedEntitiesIds( string $referenced_table, int $referencing_id ): array
  {
    
    // Get a proper relation field
    $rel_field = false;
    $return_field = false;
    switch ( $referenced_table ) {
      case self::TABLE_INSTITUTION:
        $rel_field = self::REL_USER_ID;
        $return_field = self::REL_INSTITUTION_ID;
        break;
      case self::TABLE_USER:
        $rel_field = self::REL_INSTITUTION_ID;
        $return_field = self::REL_USER_ID;
        break;
    }

    // Proceed only when a field is given
    if ( $rel_field ) {

      $relations = $this->database->table( $referenced_table )
        ->where( $rel_field, $referencing_id );

      $ids = [];

      foreach ( $relations as $relation ) {
        $ids[] = $relation[ $return_field ];
      }

      return $ids;

    }

    return [];
  
  }


  /**
   * Get array of IDs of institutions related to a user
   * @param int $user_id
   * @return int[]
   */
  public function getUserInstitutionsIds( int $user_id ): array
  {
    return $this->getRelatedEntitiesIds( self::TABLE_INSTITUTION, $user_id );
  }


  /**
   * Get array of IDs of users related to an institution
   * @param int $user_id
   * @return int[]
   */
  public function getInstitutionUsersIds( int $institution_id ): array
  {
    return $this->getRelatedEntitiesIds( self::TABLE_USER, $institution_id );
  }


  /**
   * Return institutions related to a user
   * @param int $user_id
   * @return Selection
   */
  public function getUserInstitutions( int $user_id ): Selection
  {
    
    $ids = $this->getUserInstitutionsIds( $user_id );
    
    return $this->database->table( self::TABLE_INSTITUTION )
      ->where( "id", $ids );

  }


  /**
   * Return users related to an institution
   * @param int $institution_id
   * @return Selection
   */
  public function getInstitutionsUsers( int $institution_id ): Selection
  {
    
    $ids = $this->getInstitutionUsersIds( $institution_id );
    
    return $this->database->table( self::TABLE_USER )
      ->where( "id", $ids );

  }


  /**
   * Return array of translated role names
   * for the usage in forms
   * @return string[]
   */
  public function getFormArrayAvailableRoles(): array
  {
    return array_map( function( $role ) {
      return $this->getRoleName( $role );
    }, self::ROLES );
  }


  /**
   * Get a translated role name by its ID or by its slug
   * @param int|string $role
   * @return string
   */
  public function getRoleName( $role ): string
  {

    if ( gettype( $role ) === "integer" ) {
      $role = self::ROLES[ $role ];
    }

    return $this->translator->translate( "institution.role.".$role.".name" );

  }


  /**
   * Get the ID of a role slug
   * @param string $role_slug
   * @param int
   */
  public function getRoleId( string $role_slug ): int
  {
    return array_search( $role_slug, self::ROLES );
  }

  

  

  // Conditions



  

  /**
   * Check if the relation exists
   * @param int $user_id
   * @param int $institution_id
   * @return bool
   */
  public function relationExists( int $user_id, int $institution_id ): bool
  {
    $data = $this->database->table( self::TABLE_BASE )
      ->where( self::REL_USER_ID, $user_id )
      ->where( self::REL_INSTITUTION_ID, $institution_id);
    
    return $data->count() > 0;
  }


  /**
   * Control if the role is valid
   * @param int $role_id
   * @return bool
   */
  public function isValidRole( int $role_id ): bool
  {
    return in_array( $role_id, array_keys(self::ROLES ) );
  }

  
  /**
   * Check if a relation exists and throw eventual error
   * @param int $user_id
   * @param int $institution_id
   * @param int|null $code
   * @throws \App\Model\MissingItemException The relation does not exist
   */
  public function checkRelationExistence( int $user_id, int $institution_id, ?int $code = 0 ): void
  {
    if ( ! $this->relationExists( $user_id, $institution_id ) ) {
      throw new MissingItemException( "The relation does not exist!", $code, ["type"=>"missing_rel"] );
    }
  }

  /**
   * Check if a relation exists and throw eventual error
   * @param int $relation_id
   * @param int|null $code
   * @throws \App\Model\MissingItemException The relation does not exist
   */
  public function checkRelationEntityExistence( int $relation_id, ?int $code = 0 ): void
  {

    $entity = $this->database->table( self::TABLE_BASE )
      ->get( $relation_id );
    if ( $entity == null ) {
      throw new MissingItemException( "The relation does not exist!", $code, ["type"=>"missing_rel"] );
    }
  }


  /**
   * Does a user have at least one relation where he is in a given role?
   * @param int $user_id
   * @param int|string $role
   * @param int $code
   * @throws \App\Model\DictionaryValidationException invalid role provided
   */
  public function userHasRole( int $user_id, $role, ?int $code = 0 ): bool
  {

    $roles = $this->getUserRolesSum( $user_id );

    // Validate and normalize the role
    if ( gettype( $role ) === "string" ) {
      if ( in_array( $role, self::ROLES ) ) {
        $role = $this->getRoleId( $role );
      } else {
        throw new DictionaryValidationException("Invalid role name", $code );
      }
    } else if ( gettype( $role ) === "integer" ) {
      if ( ! $this->isValidRole( $role ) ) {
        throw new DictionaryValidationException("Invalid role ID", $code );
      }
    } else {
      throw new DictionaryValidationException("Invalid role argument", $code );
    }

    foreach ( $roles as $item ) {
      if ( $role === $item ) {
        return true;
      }
    }

    return false;

  }


  /**
   * @param int $user_id
   * @param int $institution_id
   * @param int|string $role
   * @return bool
   * @throws \App\Model\MissingItemException - Relation does not exist.
   * @todo PHP8: $role => int|string union type
   */
  public function userHasRoleInInstitution( int $user_id, int $institution_id, $role, int $code = 0 ): bool
  {
    $data = $this->database->table( self::TABLE_BASE )
      ->where( self::REL_USER_ID, $user_id )
      ->where( self::REL_INSTITUTION_ID, $institution_id );
    
    if ( $data->count() == 0 ) {
      throw new MissingItemException("Relation does not exist!", $code);
    }

    if ( gettype( $role ) === "string" ) {
      $role = $this->getRoleId( $role );
    }

    return $data->fetch()[ self::REL_ROLE ] === $role;
    
  }









  


}
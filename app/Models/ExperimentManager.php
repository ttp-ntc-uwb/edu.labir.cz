<?php
namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\Strings;
use Nette\Database\Explorer;
use Nette\Database\Table\Selection;
use Nette\Utils\Random;

class ExperimentManager extends BaseManager {

  const TABLE_BASE = "experiment",
    EXP_ID = "id",
    EXP_NAME = "name",
    EXP_SLUG = "slug",
    EXP_EXCERPT = "excerpt",
    EXP_TEASER = "teaser",
    EXP_TEXT = "text",
    EXP_PUBLIC = "public",
    EXP_PUBLISHED = "published",
    EXP_EMAIL = "email",
    EXP_DIFFICULTY = "difficulty",
    EXP_THUMB = "thumb",
    EXP_WEIGHT = "weight",
    TABLE_USER = "user_experiment",
    REL_USER_ID = "user_id",
    REL_EXPERIMENT_ID = "experiment_id",
    TABLE_REL_EXPERIMENT = "experiment_taxonomy",
    REL_TAXONOMY_ID = "taxonomy_id";

  const EXCERPT_LENGTH = 25;

  const TABLE_SUBJECTS = "experiment_subject";

  const TABLE_TAGS = "experiment_tag";

  /** @var Explorer */
  public $database;

  public function __construct(
    Explorer $database
  )
  {
    $this->database = $database;
  }

  /**
   * Create an experiment with basic information
   */
  public function addExperiment( $values )
  {

    $payload = $this->formatPayload( $values );

    return $this->database->table( self::TABLE_BASE )
      ->insert( $payload );
    
  }

  public function deleteExperiment( $experiment_id )
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $experiment_id )
      ->delete();
  }

  public function formatPayload( $values ): array
  {

    // The most important data
    $payload = [
      self::EXP_NAME => $values->name,
      self::EXP_TEXT => $values->text
    ];

    // Assign or fabricate the excerpt
    if ( empty( $values->excerpt ) ) {

      $payload[ self::EXP_EXCERPT ] = $this->trimWordsCount( $values->text, self::EXCERPT_LENGTH );

    } else {
      
      $payload[ self::EXP_EXCERPT ] = strip_tags( $values->excerpt );

    }

    // Assign or fabricate the slug
    if ( empty( $values->slug ) ) {
      $payload[ self::EXP_SLUG ] = $this->formatUniqueSlug( $values->name );
    } else {
      $payload[ self::EXP_SLUG ] = $this->formatUniqueSlug( $values->slug );
    }

    if ( isset( $values->public ) ) {
      if ( ! empty( $values->public ) ) {
        $payload[ self::EXP_PUBLIC ] = $values->public;
      } else {
        $payload[ self::EXP_PUBLIC ] = false;
      }
    } else {
      $payload[ self::EXP_PUBLIC ] = false;
    }

    if ( isset( $values->published ) ) {
      if ( ! empty( $values->published ) ) {
        $payload[ self::EXP_PUBLISHED ] = $values->published;
      } else {
        $payload[ self::EXP_PUBLISHED ] = false;
      }
    } else {
      $payload[ self::EXP_PUBLISHED ] = false;
    }

    // Assign facultative values
    if ( !empty( $values->teaser ) ) {
      $payload[ self::EXP_TEASER ] = $values->teaser;
    }
    if ( !empty( $values->email ) ) {
      $payload[ self::EXP_EMAIL ] = $values->email;
    }

    if ( !empty( $values->difficulty ) ) {
      $payload[ self::EXP_DIFFICULTY ] = intval( $values->difficulty );
    }

    if ( !empty( $values->weight ) ) {
      $payload[ self::EXP_WEIGHT ] = intval( $values->weight );
    } else {
      $payload[ self::EXP_WEIGHT ] = 0;
    }

    return $payload;

  }


  public function setDifficulty( int $id, int $difficulty ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_DIFFICULTY => $difficulty
      ]);
  }

  public function setWeight( int $id, int $weight ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_WEIGHT => $weight
      ]);
  }


  public function setText( int $id, string $text ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_TEXT => $text
      ]);
  }

  public function setExcerpt( int $id, string $excerpt ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_EXCERPT => $this->trimWordsCount( $excerpt, self::EXCERPT_LENGTH )
      ]);
  }

  public function setTeaser( int $id, string $teaser ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_TEASER => $teaser
      ]);
  }

  public function setPublished( int $id, bool $published ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_PUBLISHED => $published
      ]);
  }

  public function setPublic( int $id, bool $public ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_PUBLIC => $public
      ]);
  }

  public function setName( int $id, string $name ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_NAME => $name
      ]);
  }

  public function setThumb( int $id, string $image ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $id )
      ->update([
        self::EXP_THUMB => $image
      ]);
  }

  public function setSlug( int $id, string $slug )
  {

    $previous = $this->getExperiment( $id );

    $slug = Strings::webalize( $slug );

    if ( $slug != $previous->slug ) {
      $this->database->table( self::TABLE_BASE )
        ->where( self::EXP_ID, $id )
        ->update([
          self::EXP_SLUG => $this->formatUniqueSlug( $slug ),
        ]);
    }
    
  }

  public function setExperimentUsers( int $experiment_id, array $users ): void
  {

    $previous = $this->getExperimentUserIds( $experiment_id );

    $add = array_diff( $users, $previous );
    foreach ( $add as $user ) {
      $this->addExperimentUser( $experiment_id, $user );
    }

    $remove = array_diff( $previous, $users );

    foreach ( $remove as $user ) {
      $this->deleteExperimentUser( $experiment_id, $user );
    }

  }

  public function userIsAuthorOf( int $user_id, int $experiment_id ): bool
  {
    return $this->database->table( self::TABLE_USER )
      ->where( self::REL_USER_ID, $user_id )
      ->where( self::REL_EXPERIMENT_ID, $experiment_id )
      ->count() > 0;
  }

  public function addExperimentUser( int $experiment_id, int $user_id ): void
  {
    if ( ! $this->userIsAuthorOf($user_id, $experiment_id) ) {
      $this->database->table( self::TABLE_USER )
        ->insert([
          self::REL_EXPERIMENT_ID => $experiment_id,
          self::REL_USER_ID => $user_id
        ]);
    }
  }

  public function deleteExperimentUser( int $experiment_id, int $user_id ): void
  {
    $this->database->table( self::TABLE_USER )
      ->where( self::REL_USER_ID, $user_id )
      ->where( self::REL_EXPERIMENT_ID, $experiment_id )
      ->delete();
  }

  public function getUserExperimentsIds( int $user_id ): array
  {
    $data = $this->database->table( self::TABLE_USER )
      ->where( self::REL_USER_ID, $user_id );
    $ids = [];

    foreach ( $data as $rel ) {
      $ids[] = $rel->experiment_id;
    }

    return $ids;
  }

  public function getExperimentUserIds( int $experiment_id ): array
  {
    $data = $this->database->table( self::TABLE_USER )
      ->where( self::REL_EXPERIMENT_ID, $experiment_id );
    $ids = [];

    foreach ( $data as $rel ) {
      $ids[] = $rel->user_id;
    }

    return $ids;
  }

  public function getUserExperiments( int $user_id ): Selection
  {
    $ids = $this->getUserExperimentsIds( $user_id );
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_ID, $ids )
      ->order( self::EXP_NAME );
  }


  public function getExperiment( int $id ): ?ActiveRow
  {
    return $this->database->table( self::TABLE_BASE )
      ->get( $id );
  }

  public function getExperiments( ?int $count = null, ?int $excluded_id = null, ?bool $public = false, ?bool $rand = true ): Selection
  {
    $query = $this->database->table( self::TABLE_BASE );

    $query->where( self::EXP_PUBLISHED, true );

    if ( $public == true ) {
      $query->where( self::EXP_PUBLIC, true );
    }

    if ( !empty( $excluded_id ) ) {
      $query->where( self::EXP_ID . " !=", $excluded_id );
    }

    if ( $rand ) {
      $query->order( "RAND()" );
    } else {
      $query->order( self::EXP_WEIGHT. " DESC" );
    }

    if ( $count != null ) {
      $query->limit( $count );
    }

    return $query;

  }

  public function getFrontendExperiments( ?bool $public_only = false ): Selection
  {

    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_PUBLISHED, true );

    if ( $public_only ) {
      $query->where( self::EXP_PUBLIC, true );
    }

    return $query;

  }

  public function getFrontendExperimentsByTaxonomy( int $taxonomy_id, ?bool $public_only = false, ?string $order_by = "weight", ?string $order = "ASC" ): Selection
  {

    $query = $this->getFrontendExperiments( $public_only, $order_by, $order );

    $query->where( self::EXP_ID, $this->getExperimentsInTaxonomyIds( $taxonomy_id ) );

    $query->order( $order_by . " " . $order );

    return $query;

  }

  public function getFrontendExperimentsRandom( ?bool $public_only = false, ?int $limit = 4, ?array $excluded = [] ): Selection
  {
    
    $query = $this->getFrontendExperiments( $public_only );

    $query->order( "RAND()" );

    $query->limit( $limit );

    if ( count( $excluded ) > 0 ) {
      foreach ( $excluded as $excluded_id ) {

        $query->where( self::EXP_ID . " !=", intval( $excluded_id ) );

      }
    }

    return $query;
  }

  /**
   * Load IDS of experiments related to a taxonomy
   * @deprecated This method hould be replaced by a join
   */
  private function getExperimentsInTaxonomyIds( int $taxonomy_id ): array
  {
    $query = $this->database->table( self::TABLE_REL_EXPERIMENT )
      ->where( self::REL_TAXONOMY_ID, $taxonomy_id );

    $output = [];

    foreach ( $query as $rel ) {
      $output[] = $rel->experiment_id;
    }

    return $output;

  }

  /*

  public function getFrontendExperiments( ?bool $public = null, ?int $taxonomy_id = null): Selection
  {
    
    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_PUBLISHED, true );

    if ( $public != null ) {
      $query->where( self::EXP_PUBLIC, $public );
    }

    if ( $taxonomy_id != null ) {
      $relations = $this->database->table( self::TABLE_REL_EXPERIMENT )
        ->where( self::REL_TAXONOMY_ID, $taxonomy_id );
      
      $ids = [];
      foreach ( $relations as $relation ) {
        $ids[] = $relation->experiment_id;
      }

      \Tracy\Debugger::barDump( $ids );

      $query->where( self::EXP_ID, $ids );

    }

    $query->order( self::EXP_WEIGHT . " DESC" );

    return $query;

  }

  */

  public function getExperimentBySlug( string $slug ): ?ActiveRow
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_SLUG, $slug )
      ->fetch();
  }

  public function getAdminExperiments(): Selection
  {
    return $this->database->table( self::TABLE_BASE );
  }





  /**
   * Strip a string to maximum words count removing all tags
   */
  private function trimWordsCount( $text, ?int $limit = 10 ): string
  {
    
    $text = strip_tags( $text );

    if (str_word_count($text, 0) > $limit) {
      $words = str_word_count($text, 2);
      $pos   = array_keys($words);
      $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
  }

  public function isAvailableSlug( string $slug ): bool
  {
    
    $data = $this->database->table( self::TABLE_BASE )
      ->where( self::EXP_SLUG, $slug );

    \Tracy\Debugger::barDump( $slug );

    return $data->count() == 0;

  }

  private function formatUniqueSlug( string $slug ): string
  {

    $baseSlug = Strings::webalize( $slug );

    if ( $this->isAvailableSlug( $baseSlug ) ) {
      return $baseSlug;
    }

    // Search for increased tags
    for ( $i = 1; $i < 100; $i++ ) {
      $s = implode( "-", [ $baseSlug, $i ] );

      if ( $this->isAvailableSlug( $s ) ) {
        return $s;
      }

    }

    return implode( "-", [$baseSlug,Random::generate(10)]);

  }

  

}
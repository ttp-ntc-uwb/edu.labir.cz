<?php

namespace App\Model;

use Nette;

/**
 * Common methods used in all managers
 */
class BaseManager {

  use Nette\SmartObject;


  /**
   * Initialise a nested array of options with optional actions
   * 
   * Keys are the following:
   * 0 => select_one
   * "add" => add_new
   * 
   * @param string|null $select_one_label
   * @param string|null $add_new_label
   * @param string $actions_label
   * @return array
   */
  public function initOptionsContainer( string $select_one_label = null, string $add_new_label = null, $actions_label = "Akce" ): array
  {
    $output = [];

    // Init the optgroup
    if ( !empty( $add_new_label ) || !empty( $select_one_label ) ) {
      $output[ $this->formatLabel( $actions_label ) ] = [];
    }

    // Add "select one" action
    if ( !empty( $select_one_label ) ) {
      $output[ $this->formatLabel( $actions_label ) ][0] = $this->formatLabel( $select_one_label );
    }

    // Add "add new" action
    if ( !empty( $add_new_label ) ) {
      $output[ $this->formatLabel( $actions_label ) ]["add"] = $this->formatLabel( $add_new_label );
    }

    return $output;

  }

  /**
   * Initialise a simple array of options with optional actions
   * 
   * Keys are the following:
   * 0 => select_one
   * "add" => add_new
   * 
   * @param string|null $select_one_label
   * @param string|null $add_new_label
   * @return array
   */
  public function initArrayContainer( string $select_one_label = null, $add_new_label = null ): array
  {
    $output = [];

    // Add "select one" action
    if ( !empty( $select_one_label ) ) {
      $output[0] = $this->formatLabel( $select_one_label );
    }

    // Add "add new" action
    if ( !empty( $add_new_label ) ) {
      $output["add"] = $this->formatLabel( $add_new_label );
    }

    return $output;
  }

  private function formatLabel( string $label ): string
  {
    return "- " . $label ." -";
  }


  
}
<?php

namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Schema\Schema;
use Nette\Utils\DateTime;
use Nette\Schema\Expect;
use stdClass;

/**
 * Handles all operations against the request table
 */
final class LogManager extends BaseManager
{

  /** @var Nette\Database\Explorer */
  public $database;

  /** @var Nette\Localization\Translator $translator */
  public $translator;

  const TABLE_BASE = "log",
    LOG_ID = "id",
    LOG_TYPE = "type",
    LOG_USER_ID = "user_id",
    LOG_STAATE = "state",
    LOG_RELATED_ID = "related_id",
    LOG_TIME = "time",
    LOG_DATA = "data";

  /**
   * Log types definitions
   *
   * Necessary for data output and validation.
   * 
   * Add new type:
   * 1. add it here
   * 2. add it to log.cs.neon
   */
  const TYPES = [
    0 => "user_registration_self",
    1 => "user_registration_other",
    2 => "user_email_verification",
    3 => "user_password_recovery",
    4 => "experiment_created"
  ];

  /**
   * Assign roles to actions
   * used in USerNotificationFormFactory
   */
  const ACL = [
    "admin" => [
      0, 1, 2, 3, 4
    ],
    "content_creator" => [
      4
    ]
  ];

  public function __construct(
    Nette\Database\Explorer $database,
    Nette\Localization\Translator $translator
  )
	{
    $this->database = $database;
    $this->translator = $translator;
  }

  /**
   * Create a new log entry returning its value
   * @throws 
   */
  public function addLog( int $type, int $user_id, ?int $related_entity_id = null, ?array $data = null, ?string $time =null ): ActiveRow
  {

    // Format the time
    try {
      $time = (string) DateTime::from( $time );
    } catch ( \Exception $e ) {
      $time = (string) DateTime::from(0);
    }

    $item = [
      self::LOG_TYPE => $type,
      self::LOG_USER_ID => $user_id,
      self::LOG_RELATED_ID => $related_entity_id,
      self::LOG_DATA => json_encode( $data ),
      self::LOG_TIME => $time
    ];

  
    return $this->database->table( self::TABLE_BASE )
      ->insert( $item );

  }

  /**
   * Get a formatted log entry
   * @param int $id
   * @return \stdClass id, type, type_name, user_id, user_email, data, state, time, message
   */
  public function getLog( int $id ): \stdClass
  {
    $entry = $this->getLogRaw( $id );

    $email = $this->database->table( "user" )
      ->get( $entry->user_id )->email;

    $data = array_merge(
      $entry->toArray(),
      [
        "user_email" => $email,
        "type_name" => $this->getLogTranslatedType( $entry->type ),
        "message" => $this->getLogTranslatedMessage( $entry->type, json_decode( $entry->data, true ) )
      ]
    );

    return Nette\Utils\ArrayHash::from($data);

  }

  public function getLogRaw( int $id ): ?ActiveRow
  {
    return $this->database->table( self::TABLE_BASE )
      ->get( $id );
  }

  /**
   * Format the type name
   * @param int $type
   * @return string
   * @uses actions.cs.neon
   */
  public function getLogTranslatedType( int $type ): string
  {
    return $this->translator->translate( "action." . self::TYPES[ $type ] . "name" );
  }

  /**
   * Format the readable log message based on the data
   * @param int $type
   * @param array $data
   * @return string
   */
  public function getLogTranslatedMessage( int $type, ?array $data = null ): string
  {
    $data = empty( $data ) ? [] : $data;

    return $this->translator->translate( "actions." . self::TYPES[ $type ] . ".log_format", $data );
  }

  /**
   * Administrative overview of logs
   * @param int $type
   * @param int $user_id
   * @param int $related_id
   * @return array
   * @deprecated Should be transformed into Selection
   */
  public function getAdminLogs( ?int $type = null, ?int $user_id = null, ?int $related_id = null ): array
  {

    // The base query
    $query = $this->database->table( self::TABLE_BASE );

    // Add optional type filter
    if ( $type != null ) {
      $query->where( self::LOG_TYPE, $type );
    }

    // Add optional user argument
    if ( $user_id != null ) {
      $query->where( self::LOG_USER_ID, $user_id );
    }

    // Add optional related entity argument
    if ( $related_id != null ) {
      $query->where( self::LOG_RELATED_ID, $related_id );
    }

    // Convert raw data to formatted log entries

    $output = [];

    foreach ( $query as $item ) {

      $output[ $item->id ] = $this->getLog( $item->id );

    }

    return $output;

  }



}
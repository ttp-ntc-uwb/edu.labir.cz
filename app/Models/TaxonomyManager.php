<?php

namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Database\Explorer;
use Nette\Localization\ITranslator;

final class TaxonomyManager extends BaseManager {


  public const TABLE_BASE = "taxonomy",
    TAX_ID = "id",
    TAX_NAME = "name",
    TAX_TYPE = "type",
    TAX_SLUG = "slug",
    TAX_DESCRIPTION = "description",
    TABLE_REL_USER = "user_taxonomy",
    TABLE_REL_EXPERIMENT = "experiment_taxonomy",
    TABLE_EXPERIMENT = "experiment",
    REL_EXPERIMENT_ID = "experiment_id",
    REL_USER_ID = "user_id",
    REL_TAXONOMY_ID = "taxonomy_id",
    REL_TYPE = "taxonomy_type";

  public const TYPE_SUBJECT = 0;
  public const TYPE_TAG = 1;
  public const TYPE_FOLDER = 2;
  public const TYPE_LABEL = 3;

  /** @var Explorer */
  public $database;

  /** @var ITranslator */
  private $translator;

  public function __construct(
    Explorer $database,
    ITranslator $translator
  ) {
    $this->database = $database;
    $this->translator = $translator;
  }

  public function addTaxonomy( int $type, string $name, string $slug, ?string $description = null ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->insert([
        self::TAX_TYPE => $type,
        self::TAX_NAME => $name,
        self::TAX_SLUG => $this->formatSlug( $slug ),
        self::TAX_DESCRIPTION => $description
      ])->id;
  }

  /**
   * @throws \App\Model\MissingItemException
   */
  public function getTaxonomy( int $id ): ?ActiveRow
  {
    $data = $this->database->table( self::TABLE_BASE )
      ->get( $id );

    if ( $data == null ) {
      throw new MissingItemException("Taxonomy does not exist", 0);
    }

    return $data;
  }

  /**
   * @throws \App\Model\DictionaryValidationException The type does not exist
   * @throws \App\Model\MissingItemException The experiment was not found
   */
  public function getTaxonomyBySlug( string $slug, ?int $type = null ): ?ActiveRow
  {

    if ( $type != null ) {
      if ( ! $this->isValidType( $type ) ) {
        throw new DictionaryValidationException( "Not valid type", 0 );
      }
    }

    $data = $this->database->table( self::TABLE_BASE )
      ->where( self::TAX_SLUG, $slug );

    if ( $type != null ) {
      $data->where( self::TAX_TYPE, $type );
    }

    // $data->fetch();

    if ( $data->count() == 0 ) {
      throw new MissingItemException("The taxonomy was not found", 0 );
    }

    return $data->fetch();
    
  }

  public function getFormArrayTaxonomies( int $type ): array
  {
    $output = [];
    foreach ( $this->getTaxonomiesOfType( $type ) as $term ) {
      $output[ $term->id ] = $term->name;
    }
    return $output;
  }

  public function getExperimentTaxonomiesOfType( int $id, int $type ): Selection
  {
    $relations = $this->database->table( self::TABLE_REL_EXPERIMENT )
      ->where( self::REL_EXPERIMENT_ID, $id )
      ->where( self::REL_TYPE, $type );

    $ids = [];
    foreach ( $relations as $relation ) {
      $ids[] = $relation->taxonomy_id;
    }

    return $this->database->table( self::TABLE_BASE )
      ->where( "id", $ids );
  }

  public function getExperimentTaxonomiesOfTypeIds( int $id, int $type ): array
  {
    $subjects = $this->getExperimentTaxonomiesOfType( $id, $type );

    $ids = [];

    foreach ( $subjects as $subject ) {
      $ids[] = $subject->id;
    }

    return $ids;

  }

  public function getUserTaxonomiesOfType( int $id, int $type ): Selection
  {
    $relations = $this->database->table( self::TABLE_REL_USER )
      ->where( self::REL_USER_ID, $id )
      ->where( self::REL_TYPE, $type );

    $ids = [];
    foreach ( $relations as $relation ) {
      \Tracy\Debugger::barDump( $relation->taxonomy_id );
      $ids[] = $relation->taxonomy_id;
    }

    return $this->database->table( self::TABLE_BASE )
      ->where( "id", $ids );

  }

  public function getUserTaxonomiesOfTypeIds( int $id, int $type ): array
  {

    $subjects = $this->getUserTaxonomiesOfType( $id, $type );

    $ids = [];

    foreach ( $subjects as $subject ) {
      $ids[] = $subject->id;
    }

    return $ids;

  }

  public function getExperimentsByTaxonomy( int $id, bool $public = false ): array
  {
    $relations = $this->database->table( self::TABLE_REL_EXPERIMENT )
      ->where( self::REL_TAXONOMY_ID, $id );

    $output = [];

    foreach ( $relations as $relation ) {

      $experiment = $relation->ref("experiment","experiment_id");

      if ( $experiment->published ) {
        if ( $public == true ){
          if ( $experiment->public == true || $experiment->public == false ) {
            $output[] = $experiment;
          }
        } else {
          if ($experiment->public == true ) {
            $output[] = $experiment;
          }
        }
      }

    }

    return $output;

  }


  public function getFormArrayTypes(): array
  {
    return [
      self::TYPE_SUBJECT => $this->translator->translate("subject.res.sg.nom"),
      self::TYPE_TAG => $this->translator->translate("tag.res.sg.nom"),
      self::TYPE_FOLDER => $this->translator->translate("folder.res.sg.nom"),
      self::TYPE_LABEL => $this->translator->translate( "label.res.sg.nom" )
    ];
  }

  public function getTaxonomiesOfType( ?int $type = null ): ?Selection
  {
    $data = $this->database->table( self::TABLE_BASE );

    if ( $this->isValidType( $type ) ) {
      $data->where( self::TAX_TYPE, $type );
    }
    
    return $data; 
  }

  public function setSlug( int $id, string $slug ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::TAX_ID, $id )
      ->update([
        self::TAX_SLUG => $this->formatSlug( $slug )
      ]);
  }

  public function setType( int $id, int $type ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::TAX_ID, $id )
      ->update([
        self::TAX_TYPE => $type
      ]);
  }

  public function setDescription( int $id, string $description ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::TAX_ID, $id )
      ->update([
        self::TAX_DESCRIPTION => $description
      ]);
  }

  public function setName( int $id, string $name ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::TAX_ID, $id )
      ->update([
        self::TAX_NAME => $name
      ]);
  }

  public function setExperimentTaxonomyRelations( int $id, array $taxonomies, int $type ): void
  {

    $previous = [];
    foreach ($this->database->table( self::TABLE_REL_EXPERIMENT )->where( self::REL_EXPERIMENT_ID, $id )->where( self::REL_TYPE, $type ) as $relation ) {
      $previous[$relation->taxonomy_id] = $relation->experiment_id;
    }

    $previous = array_keys( $previous );

    $add = array_diff( $taxonomies, $previous );

    foreach ( $add as $relation ) {
      if ( ! $this->experimentTaxonomyRelationExists( $id, $relation ) ) {
        $this->database->table( self::TABLE_REL_EXPERIMENT )
          ->insert([
            self::REL_EXPERIMENT_ID => $id,
            self::REL_TAXONOMY_ID => $relation,
            self::REL_TYPE => $type
          ]);
      }
    }

    $remove = array_diff( $previous, $taxonomies );
    foreach ( $remove as $relation ) {
      $this->database->table( self::TABLE_REL_EXPERIMENT )
        ->where( self::REL_EXPERIMENT_ID, $id )
        ->where( self::REL_TAXONOMY_ID, $relation )
        ->where( self::REL_TYPE, $type )
        ->delete();
    }

  }

  public function setUserTaxonomyRelations( int $id, array $taxonomies, int $type ): void
  {

    $previous = [];
    foreach ($this->database->table( self::TABLE_REL_USER )->where( self::REL_USER_ID, $id )->where( self::REL_TYPE, $type ) as $relation ) {
      $previous[$relation->taxonomy_id] = $relation->user_id;
    }

    $previous = array_keys( $previous );

    $add = array_diff( $taxonomies, $previous );

    foreach ( $add as $relation ) {
      if ( ! $this->userTaxonomyRelationExists( $id, $relation ) ) {
        $this->database->table( self::TABLE_REL_USER )
          ->insert([
            self::REL_USER_ID => $id,
            self::REL_TAXONOMY_ID => $relation,
            self::REL_TYPE => $type
          ]);
      }
    }

    $remove = array_diff( $previous, $taxonomies );
    foreach ( $remove as $relation ) {
      $this->database->table( self::TABLE_REL_USER )
        ->where( self::REL_USER_ID, $id )
        ->where( self::REL_TAXONOMY_ID, $relation )
        ->where( self::REL_TYPE, $type )
        ->delete();
    }

  }

  public function deleteTaxonomy( int $id ): void
  {
    $this->database->table( self::TABLE_BASE )
      ->where( self::TAX_ID, $id )
      ->delete();
  }

  public function experimentTaxonomyRelationExists( int $experiment_id, int $taxonomy_id ): bool
  {
    return $this->database->table( self::TABLE_REL_EXPERIMENT )
      ->where( self::REL_EXPERIMENT_ID, $experiment_id )
      ->where( self::REL_TAXONOMY_ID, $taxonomy_id )
      ->count() > 0 ? true : false;
  }

  public function userTaxonomyRelationExists( int $user_id, int $taxonomy_id ): bool
  {
    return $this->database->table( self::TABLE_REL_USER )
      ->where( self::REL_USER_ID, $user_id )
      ->where( self::REL_TAXONOMY_ID, $taxonomy_id )
      ->count() > 0 ? true : false;
  }

  public function isUniqueSlug( string $slug, ?int $type = null ): bool
  {
    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::TAX_SLUG, $slug );

    if ( $type != null ) {
      if ( $this->isValidType( $type ) ) {
        $query->where( self::TAX_TYPE, $type );
      }
    }

    return $query->count() == 0;

  }

  public function isValidType( ?int $type = null ): bool
  {
    return $type === self::TYPE_SUBJECT
      || $type === self::TYPE_TAG
      || $type === self::TYPE_FOLDER
      || $type === self::TYPE_LABEL;
  }

  public function formatSlug( string $slug ): string
  {
    $baseSlug = $slug;
    if ( $this->isUniqueSlug( $slug ) ) {
      return $slug;
    } else {
      for ( $i = 1; $i < 100; $i++ ) {
        $slugTemp = implode( "-", [$baseSlug, $i] );
        if ( $this->isUniqueSlug( $slugTemp ) ) {
          return $slugTemp;
        }
      }
    }
  }

}
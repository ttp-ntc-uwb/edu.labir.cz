<?php

namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;

class LocalityManager extends \App\Model\BaseManager
{
  
  // Database constants
  const TABLE_BASE = "locality",
    LOC_ID = "id",
    LOC_NAME = "name",
    LOC_SLUG = "slug",
    LOC_DEPTH = "depth",
    LOC_PARENT_ID = "parent_id";

  
  use Nette\SmartObject;

  /** @var Nette\Database\Explorer */
  private $database;

  




  public function __construct(
    Nette\Database\Explorer $database
  ){
    $this->database = $database;
  }



  // SETTERS

  /**
   * Add a new locality, returning the new ID
   * @param string $name
   * @param int $parent_id
   * @param int $depth
   * @throws \App\Model\DupliciteItemException Locality name is taken.
   * @throws \App\Model\MissingRelatedItemException Parent locality is missing
   */
  public function addLocality( string $name, int $parent_id, int $depth, ?int $code = 0 ): int
  {

    // Check if the loclaity name is unique
    if ( ! $this->isUniqueLocality( $name ) ) {
      throw new \App\Model\DupliciteItemException( "A locality of the given name already exists!", self::TABLE_BASE, $code, [ "name"=>$name ] );
    }

    // Check if the parentExists
    if ( $depth !== 0 && !$this->localityExists( $parent_id ) ) {
      throw new \App\Model\MissingRelatedItemException( "Parent locality does not exist!", self::TABLE_BASE, 0, ["parent_id"=>$parent_id] );
    }

    // Check if the name is provided
    if ( empty( $name ) ) {
      throw new InputValidationException("Provide a name!", $code );
    }

    // Create the new entry
    $locality = $this->database->table( self::TABLE_BASE )
      ->insert([
        self::LOC_NAME => $name,
        self::LOC_PARENT_ID => $parent_id,
        self::LOC_DEPTH => $depth,
        self::LOC_SLUG => Nette\Utils\Strings::webalize( $name )
      ]);

    return $locality->id;

  }


  /**
   * @param int $id
   * @param string $name
   * @param int|null $code
   * @return bool
   * @throws \App\
   */
  public function setLocalityName( int $id, string $name, ?int $code = 0 ): bool
  {
    // If old and new names equals, do nothing
    if ( $this->getLocalityName($id) === $name ) {
      return true;
    }

    // Check if the new name is unique
    if ( ! $this->isUniqueLocality($name, null, true) ) {
      throw new InputValidationException("Locality name is taken already", $code );
    }

    // If everything passed, execute the change
    $this->database->table(self::TABLE_BASE)
      ->where(self::LOC_ID, $id)
      ->update( [
        self::LOC_NAME => $name,
        self::LOC_SLUG => Nette\Utils\Strings::webalize( $name )
      ] );

    return true;

  }


  /**
   * @param int $id
   * @param int $parent_id
   * @param int|null $code
   * @return bool
   * @deprecated This should send signal to modify structure of all related entities
   */
  public function setLocalityParent( int $id, int $parent_id, ?int $code = 0 ): bool
  {

    $itemDepth = $this->getLocalityDepth( $id );
    $parentDepth = $this->getLocalityDepth($parent_id);

    if ( $itemDepth != $parentDepth + 1 ) {

      throw new InputValidationException("The parent is not of the same level", $code );

    }

    $this->database->table(self::TABLE_BASE)
      ->where(self::LOC_ID, $id)
      ->update([
        self::LOC_PARENT_ID => $parent_id
      ]);

    return true;
  }


  /**
   * Delete a locality if it already has no children
   * @param int $id
   * @throws \App\Model\StructureException The locality has children and can not be deleted.
   */
  public function deleteLocality( int $id ): void
  {

    if ( $this->localityHasChildren( $id ) ) {
      throw new StructureException("A deleted locality can never have children", 0);
    }

    $this->database->table( self::TABLE_BASE )
      ->where( self::LOC_ID, $id )
      ->delete();
  }







  // GETTERS




  /**
   * Get one locality by ID
   * @param int $id
   * @return ActiveRow
   * @throws \App\Model\MissingItemException Locality is missing!
   */
  public function getLocality( int $id ): ActiveRow
  {
    
    $locality = $this->database->table( self::TABLE_BASE )
      ->get( $id );
    
    if ( $locality == null ) {
      throw new \App\Model\MissingItemException( "Locality does not exist", self::TABLE_BASE, 0, ["id" => $id, "table" => self::TABLE_BASE ] );
    }

    return $locality;

  }

  /**
   * Get a locality name by ID
   * @param int $id
   * @return string
   * @throws \App\Model\MissingItemException
   */
  public function getLocalityName( int $id ): string
  {

    // If the ID provided is 0, return "World"
    if ( $id == 0 ) {
      return "Svět";
    }

    // This may throw MissingItemException
    $locality = $this->getLocality( $id );

    // Return the name
    return $locality->name;
  }

  public function getLocalityIdByName( string $name ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::LOC_NAME, $name )
      ->fetch()->id;
  }

  /**
   * Get a locality depth by ID
   * @param int $id
   * @return int
   * @throws \App\Model\MissingItemException
   */
  public function getLocalityDepth( int $id ): int
  {

    // If the ID provided is 0, return "World"
    if ( $id == 0 ) {
      return -1;
    }
    
    // This may throw MissingItemException
    $locality = $this->getLocality( $id );

    // Return the name
    return $locality->depth;

  }


  /**
   * Get array of IDs of a locality parents
   * @param int $id
   * @return int[] Array of parents IDS
   * @throws \App\Model\MissingItemException Locality is missing (thwown by getLocality())
   */
  public function getLocalityParentsIds( int $id ): array
  {
    
    $output = [];

    // This can throw MissingItemException
    $locality = $this->getLocality( $id );

    // Continue upwards in the tree 
    // until there are no parents
    while ( $locality->parent_id != 0 ) {

      // Ad the parent to the output buffer
      $output[] = $locality->parent_id;
      
      // Load the parent
      $locality = $this->getLocality( $locality->parent_id );

    }

    return $output;


  }

  /**
   * Get Direct children of a locality
   * @param int $id
   * @return Nette\Database\Table\Selection
   * @throws \App\Model\MissingItemException
   */
  public function getLocalityDirectChildren( int $id ): Nette\Database\Table\Selection
  {

    // This may throw MissingItemException
    $this->getLocality( $id );

    return $this->database->table( self::TABLE_BASE )
      ->where( self::LOC_PARENT_ID, $id )
      ->order( self::LOC_NAME, "ASC" );

  }

  /**
   * Get array of IDS of a locality direct children
   * @param int $id
   * @return int[] Array of parents IDS
   * @throws \App\Model\MissingItemException
   * 
   */
  public function getLocalityDirectChildrenIds( int $id ): array
  {
    
    $output = [];

    // This may throw MissingItemException
    $localities = $this->getLocalityDirectChildren( $id );

    foreach ( $localities as $locality ) {
      $output[] = $locality->id;
    }

    return $output;

  }


  /**
   * Get localities of a given depth
   * @param int $depth
   * @throws Nette\InvalidArgumentException
   */
  public function getLocalitiesByDepth( int $depth ): Nette\Database\Table\Selection
  {
    // Check depth validity
    if ( !( $depth >= 0 && $depth <= 2 ) ) {
      throw new Nette\InvalidArgumentException( "Invalid depth" );
    } 

    // Return the selection
    return $this->database->table( self::TABLE_BASE )
      ->where( self::LOC_DEPTH, $depth )
      ->order( self::LOC_NAME, "ASC" );
  }

  /**
   * Get institutions related to a locality
   * @param int $id
   * @throws \App\Model\MissingItemException
   * @throws Nette\InvalidArgumentException (invalid depth)
   */
  public function getRelatedInstitutions( int $id ): Nette\Database\Table\Selection
  {
    // Locality existence control
    $locality = $this->getLocality( $id );

    // Get the related field from depth
    $field = false;
    switch ( $locality->depth ) {
      case 0: $field = "country_id"; break;
      case 1: $field = "region_id"; break;
      case 2: $field = "city_id"; break;
      default: 
        throw new Nette\InvalidArgumentException( "Invalid depth" );
        break;
    }

    // return the related entities
    return $locality->related( "institution.".$field )
      ->order("name","ASC");


  }


  /**
   * Build a tree of localities by parent
   * 
   * Return format: 
   * [ $id => [
   *  "name" => $name,
   *  "hasItems" => bool
   *  "items" => []
   *  "id" => $id
   * ] ]
   * 
   * @param int $parent_id (optional, can be 0 for countries)
   * @return array 
   * @todo The recursivness is really bad, sholuld be refactored
   */
  public function getLocalitiesTree( int $parent_id = 0 ): array
  {

    $output = [];

    // Iterate the starting level
    $selection = $this->database->table( self::TABLE_BASE )
      ->where( self::LOC_PARENT_ID, $parent_id );

    if ( $selection->count() > 0 ) {


      // Build branches for direct children
      foreach ( $selection as $row ) {
      
        // Assign the direct child branch
        $output[ $row->id ] = $this->getLocalityBranch( $row->id );


        // Iterate eventual subchildren
        if ( $output[ $row->id ]["hasItems"] ) {

          // Build branches for all subchildren
          $output[ $row->id ]["items"] = $this->getLocalityBranchChildren( $row->id );


          // Look for subsubchildren
          foreach ( $output[ $row->id ]["items"] as $item_id => $item ) {

            // Iterate eventual subsubchildren
            if ( $item["hasItems"] ) {

              // Buid subsubchildren tree
              $output[$row->id]["items"][ $item_id ]["items"] = $this->getLocalityBranchChildren( $item_id );

              foreach ( $output[$row->id]["items"][$item_id]["items"] as $subitem_id => $subitem ) {
                if ( $subitem["hasItems"] ) {
                  $output[$row->id]["items"][$item_id]["items"][$subitem_id]["items"] = $this->getLocalityBranchChildren($subitem_id);
                }

              }

            }

          }

        }

      }

    }

    return $output;

  }

  /**
   * Format one branch of a tree
   * 
   * Return format:
   * [ $id => [
   *  "name" => $name,
   *  "hasItems" => bool,
   *  "id" => $id,
   *  "items" => []
   * ] ]
   * 
   * @param int $parent_id
   * @return array
   */
  public function getLocalityBranch( int $parent_id = 0 ): array
  {

    $branch = [];

    if ( $parent_id == 0 || $this->localityExists( $parent_id ) ) {

      // Add depth
      if ( $parent_id == 0 ) {
        $depth = -1;
      } else {
        $depth = $this->getLocalityDepth( $parent_id );
      }

      // Depth
      $branch = [
        "name" => $this->getLocalityName( $parent_id ),
        'hasItems' => $this->localityHasChildren( $parent_id ),
        "id" => $parent_id,
        "items" => [],
        "depth" => $depth
      ];

      



    }

    return $branch;
  }

  /**
   * Get subbranches of a branch
   * @param int $id
   * @return array
   */
  private function getLocalityBranchChildren( int $parent_id ): array
  {

    $output = [];

    // Load direct children
    $direct_children = $this->getLocalityDirectChildrenIds( $parent_id );

    // Add a branch for each child
    if ( count( $direct_children ) > 0 ) {

      foreach ( $direct_children as $child ) {
        $output[ $child ] = $this->getLocalityBranch( $child );
      }

    }

    return $output;

  }



  /**
   * Get array of $key => $name depending on depth
   * For regions and cities, returns:
   * [ $parent_name => [
   *      $id => $locality_name
   *    ],
   *    ...
   * ]
   * @param int $depth
   * @param int|null $parent_id
   * @param string|null $select_one_label Optional Select one action label.
   * @param string|null $add_new_label Optional Add new action label.
   * @param string|null $actions_label Optional optgroup label for actions.
   */
  public function getFormOptionsLocalities( int $depth, int $parent_id = null, string $select_one_label = null, string $add_new_label = null, string $actions_label = "- Akce -" ): array
  {

    // Initialise the array with optional actions
    $output = $this->initOptionsContainer($select_one_label, $add_new_label, $actions_label);

    // For regions and cities, return nested array
    if ( $depth > 0 ) {
      
      $parents = $this->database->table( self::TABLE_BASE )
        ->where( self::LOC_DEPTH, $depth - 1 )
        ->order( self::LOC_NAME, "ASC" );

      if ( $parent_id != null ) {
        $parents->where( self::LOC_ID, $parent_id );
      }

      foreach ( $parents as $parent ) {

        $children = $this->getLocalityDirectChildren( $parent->id );

        if ( $children->count() > 0 ) {

          $output[ $parent->name ] = [];

          foreach ( $children as $child ) {

            $output[ $parent->name ][ $child->id ] = $child->name;
            
          }

        }

      }

    }

    // For countries, return flat structure
    else {
      $data = $this->database->table( self::TABLE_BASE )
        ->where( self::LOC_DEPTH, 0 )
        ->order( self::LOC_NAME, "ASC" );
      
      foreach ( $data as $country ) {
        $output[ $country->id ] = $country->name;
      }
    }
    

    return $output;

  }

  /**
   * Get indexed array of institutions based in depth
   * @param int $depth
   * @param int|null $parent_id
   * @param string|null $add_new_label Optional Add new action label.
   */
  public function getFormArrayInstitutions( int $depth = 0, int $parent_id = null, string $select_one_label = null, string $add_new_label = null ): array
  {
    $output = $this->initArrayContainer($select_one_label, $add_new_label);

    $data = $this->database->table( self::TABLE_BASE )
      ->order( self::LOC_NAME." ASC" )
      ->where( self::LOC_DEPTH, $depth );

    // Id a parent is provided, return 
    if ( $parent_id != null ) {
      
      // This may throw error
      $parent = $this->getLocality( $parent_id );

      $data->where( self::LOC_PARENT_ID, $parent_id );
    }

    foreach ( $data as $item ) {
      $output[ $item->id ] = $item->name;
    }

    return $output;
  }


  /**
   * Get data for admin
   */
  public function getAdminLocalities(): array
  {
    $data = $this->getLocalitiesTree();

    $output = [];

    foreach ( $data as $country_id => $country ) {

      $output[] = [
        "id" => $country_id,
        "name" => $country["name"],
        "depth" => "země",
      ];

      if ( count( $country["items"] ) > 0 ) {
        
        foreach ( $country["items"] as $region_id => $region ) {

          $output[] = [
            "id" => $region_id,
            "name" => "- " . $region["name"],
            "depth" => "region",
          ];

          // 
          if ( count( $region["items"] ) > 0 ) {
            
            foreach( $region["items"] as $city_id => $city ){

              $output[] = [
                "id" => $city_id,
                "name" => "- - " . $city["name"],
                "depth" => "město",
              ];

            }

          }

        }

      }

    }

    return $output;

  }




  // CONDITIONS & VALIDATORS

  /**
   * Is the locality name unique?
   * @param string $name
   * @param int $parent_id (optional)
   * @param bool $diacritics Should we control also webalised name?
   * @return bool
   */
  public function isUniqueLocality( string $name, int $parent_id = null, $diacritics = false ): bool
  {
    
    $unique = true;

    // Load all content within the region
    $data = $this->database->table( self::TABLE_BASE );

    // Optionally, add parent id
    if ( ! empty( $parent_id ) ) {
      $data->where( self::LOC_PARENT_ID, $parent_id );
    }



    // Fabricate a slug
    $slug = \Nette\Utils\Strings::webalize( $name );

    // Check found localities for name or slug conflict
    if ( $data->count() > 0 ) {

      foreach ( $data as $locality) {


        // Check the name
        if ( $locality->name === $name ) {

          $unique = false;

          // If we should ignore diacritics, check the slug
          if ( !$diacritics ) {
            if ( $locality->slug === $slug ) {

              $unique = false;

            }
          }

        }

      }

    }

    return $unique;

  }

  /**
   * Does the locality exist?
   * @param int $id
   * @return bool
   */
  public function localityExists( int $id ): bool
  {
    $data = $this->database->table( self::TABLE_BASE )->get( $id );
    return $data != null;
  }

  /**
   * Does the locality have children?
   * @param int $id
   * @return bool
   */
  public function localityHasChildren( int $id ): bool 
  {
    $data = $this->database->table( self::TABLE_BASE )
      ->where( self::LOC_PARENT_ID, $id );
    return $data->count() > 0;
  }






  // HELPERS & FORMATTERS








}
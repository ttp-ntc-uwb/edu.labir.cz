<?php
namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\Strings;
use Nette\Database\Explorer;
use Nette\Database\Table\Selection;

class ExperimentFileManager extends BaseManager {

  const TABLE_BASE = "experiment_file",
    FILE_ID = "id",
    FILE_EXPERIMENT_ID = "experiment_id",
    FILE_PATH = "path",
    FILE_NAME = "name",
    FILE_THUMB = "thumbnail",
    FILE_DESC = "description",
    FILE_SCOPE = "scope",
    FILE_SIZE = "size",
    FILE_PUBLIC = "public",
    FILE_FILENAME = "filename",
    FILE_EXTENSION = "extension";

  /** @var Connection */
  public $database;

  public function __construct(
    Explorer $database
  )
  {
    $this->database = $database;
  }

  public function addExperimentFile( int $experiment_id, string $name, string $description ): ActiveRow
  {
    return $this->database->table( self::TABLE_BASE )
      ->insert([
        self::FILE_EXPERIMENT_ID => $experiment_id,
        self::FILE_NAME => $name,
        self::FILE_DESC => $description
      ]);
  }

  public function createFile( int $experiment_id, string $name, string $path, string $thumb, string $scope, ?string $description = null, int $size, bool $public, string $filename ): ActiveRow
  {
    return $this->database->table( self::TABLE_BASE )->insert([
      self::FILE_EXPERIMENT_ID => $experiment_id,
      self::FILE_PATH => $path,
      self::FILE_NAME => $name,
      self::FILE_THUMB => $thumb,
      self::FILE_DESC => $description,
      self::FILE_SCOPE => $scope,
      self::FILE_SIZE => $size,
      self::FILE_PUBLIC => $public,
      self::FILE_FILENAME => $filename
    ]);
  }

  public function updateFile( int $id, string $name, string $path, string $thumb, string $description ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $id )
      ->update([
        self::FILE_PATH => $path,
        self::FILE_NAME => $name,
        self::FILE_THUMB => $thumb,
        self::FILE_DESC => $description
      ]);
  }

  public function deleteFile( int $id ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $id )
      ->delete();
  }

  public function getExperimentFiles( int $experiment_id, ?bool $public = null ): Selection
  {
    
    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_EXPERIMENT_ID, $experiment_id );

    if ( $public != null ) {
      $query->where( self::FILE_PUBLIC, $public );
    }

    return $query;
  }

  public function experimentHasPublicFiles( int $experiment_id ): bool
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_EXPERIMENT_ID, $experiment_id )
      ->where( self::FILE_PUBLIC, true )
      ->count() == 0 ? false : true;
  }

  public function experimentHasFiles( int $experiment_id, bool $public = null ): bool
  {
    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_EXPERIMENT_ID, $experiment_id );
    if ( $public != null ) {
      $query->where( self::FILE_PUBLIC, $public );
    }
    return $query->count() > 0 ? true : false;
  }

  public function getExperimentFile( int $file_id ): ?ActiveRow
  {
    return $this->database->table( self::TABLE_BASE )
      ->get( $file_id );
  }

  public function setPath( int $file_id, string $path ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_PATH => $path
      ]);
  }

  public function setThumb( int $file_id, ?string $thumb = null ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_THUMB => $thumb
      ]);
  }

  public function setScope( int $file_id, string $scope ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_SCOPE => $scope
      ]);
  }

  public function setName( int $file_id, string $name ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_NAME => $name
      ]);
  }

  public function setDescription( int $file_id, string $description ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_DESC => $description
      ]);
  }

  public function setSize( int $file_id, int $size ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_SIZE => $size
      ]);
  }

  public function setFilename( int $file_id, string $filename ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_FILENAME => $filename
      ]);
  }

  public function setExtension( int $file_id, string $extension ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_EXTENSION => $extension
      ]);
  }

  public function setPublic( int $file_id, bool $public ): int
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $file_id )
      ->update([
        self::FILE_PUBLIC => $public
      ]);
  }

  public function deleteExperimentFile( int $id ): void
  {
    $this->database->table( self::TABLE_BASE )
      ->where( self::FILE_ID, $id )
      ->delete();
  }

}
<?php
namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\Strings;
use Nette\Database\Explorer;
use Nette\Database\Table\Selection;

class ExperimentSubjectManager extends BaseManager {

  const TABLE_BASE = "experiment_subject",
    REL_SUBJECT_ID = "subject_id",
    REL_EXPERIMENT_ID = "experiment_id",
    TABLE_SUBJECTS = "subject",
    SUBJECT_ID = "id",
    SUBJECT_NAME = "name";

  /** @var Explorer */
  public $database;

  public function __construct(
    Explorer $database
  )
  {
    $this->database = $database;
  }


  /**
   * Get array of subjects related to an experiment
   * @return array $key => $name
   */
  public function getExperimentSubjects( int $experiment_id ): array
  {
    
    $related_subjects = $this->database->table( self::TABLE_BASE )
      ->where( self::REL_EXPERIMENT_ID, $experiment_id );

    $output = [];
    foreach ( $related_subjects as $subject ) {

      $loaded_subject = $this->database->table( self::TABLE_SUBJECTS )
        ->where( self::SUBJECT_ID, $subject->subject_id )
        ->fetch();
      $output[ $loaded_subject->id ] = $loaded_subject->name;
    }

    return $output;

  }






  public function setExperimentSubjects( int $experiment_id, array $subjects ): void
  {

    $previous = array_keys( $this->getExperimentSubjects( $experiment_id ) );

    $add = array_diff( $subjects, $previous );
    foreach ( $add as $subject ) {
      $this->addExperimentSubject( $experiment_id, $subject );
    }

    $remove = array_diff( $previous, $subjects );

    foreach ( $remove as $subject ) {
      $this->removeExperimentSubject( $experiment_id, $subject );
    }

  }

  public function addExperimentSubject( int $experiment_id, int $subject_id ): void
  {
    if ( ! $this->experimentHasSubject( $experiment_id, $subject_id ) ) {

      $this->database->table( self::TABLE_BASE )
        ->insert([
          self::REL_EXPERIMENT_ID => $experiment_id,
          self::REL_SUBJECT_ID => $subject_id
        ]);

    }
    
  }

  public function removeExperimentSubject( int $experiment_id, int $subject_id ): void
  {
    
    $this->database->table( self::TABLE_BASE )
      ->where( self::REL_EXPERIMENT_ID , $experiment_id )
      ->where( self::REL_SUBJECT_ID, $subject_id )
      ->delete();

  }

  public function experimentHasSubject( int $experiment_id, int $subject_id ): bool
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::REL_EXPERIMENT_ID, $experiment_id )
      ->where( self::REL_SUBJECT_ID, $subject_id )->count() > 0 ? true : false;
  }

}
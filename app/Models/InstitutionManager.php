<?php

namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Schema\Expect;

class InstitutionManager extends BaseManager
{

  const TABLE_BASE = "institution",
    TABLE_TYPES = "institution_type",
    TABLE_LOCALITY = "locality",
    INST_ID = "id",
    INST_NAME = "name",
    INST_COUNTRY = "country_id",
    INST_REGION = "region_id",
    INST_CITY = "city_id",
    INST_STREET = "street",
    INST_NUMBER = "orient",
    INST_NOTE = "note",
    INST_PSC = "psc",
    INST_ICO = "ico",
    TYPE_INST_ID = "institution_id",
    TYPE_VALUE = "value";

  /**
   * Type names should be defined in linsittution.cs.neon
   * @see institution.cs.neon
   */
  const TYPE = [
    0 => "ms",
    1 => "zs1",
    2 => "zs2",
    3 => "ss",
    4 => "vs",
    5 => "nfrm"
  ];

  use Nette\SmartObject;

  /** @var Nette\Database\Explorer @inject */
  private $database;


  public function __construct(
    Nette\Database\Explorer $database
  ){
    $this->database = $database;
  }


  // SETTERS

  /**
   * Add the form base entity
   * @param array $values
   * @param int|null $code
   * @return int ID of the new entity.
   * @throws \App\Model\MissingInputException 0 = no name provided
   */
  public function addInstitution( string $name, ?int $code = 0 ): int 
  {
    if ( empty( $name ) ) {
      throw new MissingInputException( "No name provided!", $code );
    }
    $entry = $this->database->table( self::TABLE_BASE )
      ->insert( [
        self::INST_NAME => $name,
      ] );
    return $entry[ self::INST_ID ];
  }

  /**
   * @param int $id
   * @param string $name
   * @return int Number of afected institutions.
   * @throws \App\Model\MissingItemException Institution is missing.
   */
  public function setInstitutionName( int $id, string $name ): int
  {
    // Throw error upon non-existence
    $this->institutionExistenceCheck( $id );

    return $this->database->table( self::TABLE_BASE )
      ->where( self::INST_ID, $id )
      ->update( [
        self::INST_NAME => $name
      ] );
  }

  /**
   * Set address to an institution
   * @param int $id
   * @param string $street
   * @param int $orient
   * @return int Number of afected institutions.
   * @throws \App\Model\MissingItemException Institution is missing.
   */
  public function setInstitutionAddress( int $id, string $street, int $orient, string $psc = null ): int
  {

    // Throw error upon non-existence
    $this->institutionExistenceCheck( $id );

    $data = [
      self::INST_STREET => $street,
      self::INST_NUMBER => $orient
    ];

    // Optionally assign $psc
    if ( !empty( $psc ) ) {
      $data[ self::INST_PSC ] = $psc;
    }

    return $this->database->table( self::TABLE_BASE )
      ->where( self::INST_ID, $id )
      ->update( $data );
  }

  /**
   * Set a PSC to an institution
   * @param int $id
   * @param int $psc
   * @return int Number of afected institutions.
   * @throws \App\Model\MissingItemException Institution is missing.
   */
  public function setInstitutionPsc( int $id, string $psc ): int
  {

    // Throw error upon non-existence
    $this->institutionExistenceCheck( $id );

    return $this->database->table( self::TABLE_BASE )
      ->where( self::INST_ID, $id )
      ->update( [
        self::INST_PSC => $psc
      ] );
  }

  /**
   * Set a note to an institution
   * @param int $id
   * @param string $note
   * @return int Number of afected institutions.
   * @throws \App\Model\MissingItemException Institution is missing.
   */
  public function setInstitutionNote( int $id, string $note ): int
  {
    // Throw error upon non-existence
    $this->institutionExistenceCheck( $id );

    return $this->database->table( self::TABLE_BASE )
      ->where( self::INST_ID, $id )
      ->update( [
        self::INST_NOTE => $note
      ] );

  }

  /**
   * Assign an institution to a country, region and city
   * @param int $id
   * @param int $country_id
   * @param int $region_id
   * @param int $city_id
   * @return int Number of afected institutions.
   * @throws \App\Model\MissingItemException Institution is missing.
   * @see LocalityManager::getParents( $city_id )
   */
  public function setInstitutionLocality( int $id, int $country_id, int $region_id, int $city_id ): int
  {
    // Check if institution exists
    $this->institutionExistenceCheck( $id );

    return $this->database->table( self::TABLE_BASE )
      ->where( self::INST_ID, $id )
      ->update( [
        self::INST_CITY => $city_id,
        self::INST_REGION => $region_id,
        self::INST_COUNTRY => $country_id
      ] );
  }

  /**
   * Assign types to an institution - add new, delete old ones
   * @param int $id
   * @param int[] $types Array of types ID
   * @return void
   * @throws \App\Model\MissingItemException Institution is missing.
   * @see self::TYPE
   */
  public function setInstitutionTypes( int $id, array $types ): void
  {

    // Throw error upon non-existence
    $this->institutionExistenceCheck( $id );

    // Load previously saved items
    $types_prev = $this->getInstitutionTypes( $id );

    // Add types which are not assigned already
    $types_to_add = array_diff( $types, $types_prev );
    foreach ( $types_to_add as $type ) {

      // Control type validity and proceed
      if ( $this->isValidInstitutionTypeId( $type ) ) {
        
        $this->database->table( self::TABLE_TYPES )
        ->insert( [
          self::TYPE_INST_ID => $id,
          self::TYPE_VALUE => $type
        ] );

      }
      
    }

    // Remove types that had been asigned, but are not among new ones
    $types_to_remove = array_diff( $types_prev, $types );
    foreach ( $types_to_remove as $type ) {
      $this->database->table( self::TABLE_TYPES )
        ->where( self::TYPE_INST_ID, $id )
        ->where( self::TYPE_VALUE, $type )
        ->delete();
    }

  }



  // GETTERS


  /**
   * @param int $id
   * @return Nette\Database\Table\ActiveRow
   * @throws \App\Model\MissingItemException Institution is missing.
   */
  public function getInstitution( int $id ): Nette\Database\Table\ActiveRow
  {
    // Check institution existence
    $this->institutionExistenceCheck( $id );

    return $this->database->table( self::TABLE_BASE )
      ->get( $id );

  }

  /**
   * Load the entity and its roles and return the array
   * @param int $id
   * @return array
   * @throws \App\Model\MissingItemException Institution is missing.
   */
  public function getFormDefaultsInstitution( int $id ): array
  {

    return array_merge(
      $this->getInstitution( $id )->toArray(),
      [ "types" => $this->getInstitutionTypes( $id ) ]
    );

  }

  /**
   * Retrieve index array of an institution localities
   * @param int $id
   * @return string[] ["country" => $name, $region => $name,"city"=>$name ]
   */
  public function getInstitutionLocalitiesNames( int $id ): array
  {
    return [
      "country" => $this->getLocalityName( $id, 0 ),
      "region" => $this->getLocalityName( $id, 1 ),
      "city" => $this->getLocalityName( $id, 2 )
    ];
  }


  /**
   * Get the referenced locality based on depth
   * @param int $locality_id
   * @param int $depth
   */
  public function getLocalityObject( int $locality_id, int $depth = 2 ): Nette\Database\Table\ActiveRow
  {
    
    // Select the field based on the depth
    $field = null;
    if ( $depth == 0 ) { $field = self::INST_COUNTRY; }
    else if ( $depth == 1 ) { $field = self::INST_REGION; }
    else if ( $depth == 2 ) { $field = self::INST_CITY; }

    // Load the institution
    $institution = $this->getInstitution( $locality_id );

    // Get the field
    return $institution->ref( self::TABLE_LOCALITY, $field );

  }

  /**
   * Retrieve the name of a licality
   * @param int $locality_id
   */
  public function getLocalityName( int $locality_id, int $depth ): string
  {
    return $this->getLocalityObject( $locality_id, $depth )->name;
  }

  /**
   * Return array of an intitution's types IDs
   * @param $id
   * @return int[]
   * @throws \App\Model\MissingItemException 0 = no institution
   */
  public function getInstitutionTypes( int $id ): array
  {
    $this->institutionExistenceCheck( $id );

    $types = $this->database->table( self::TABLE_TYPES )
      ->where( self::TYPE_INST_ID, $id );

    $output = [];

    foreach ( $types as $type ) {
      $output[] = $type[ self::TYPE_VALUE ];
    }

    return $output;

  }

  public function getInstitutionTypesAbbr( int $id ): array
  {
    return array_map(function($type){
      return self::TYPE[ $type ];
    },$this->getInstitutionTypes( $id ));
  }

  /**
   * Get array of types in the format [ $key => $abbr, ...]
   * Useful in forms selects and checkboxlists
   * @uses self::TYPE
   * @return string[]
   */
  public function getFormArrayAvailableTypes(): array
  {
    return self::TYPE;
  }

  /**
   * Get array of institutions in the format [ $id => $name, ... ]
   * @param int|null $country_id
   * @param int|null $region_id
   * @param int|null $city_id
   */
  public function getFormArrayInstitutions( int $country_id = null, int $region_id = null, int $city_id = null ): array
  {
    $data = $this->database->table( self::TABLE_BASE );

    if ( $country_id != null && $country_id > 0 ) {
      $data->where( self::INST_COUNTRY, $country_id );
    }

    if ( $region_id != null && $region_id > 0 ) {
      $data->where( self::INST_REGION, $region_id );
    }

    if ( $city_id != null && $city_id > 0 ) {
      $data->where( self::INST_CITY, $city_id );
    }

    $data->order( self::INST_NAME . " ASC" );

    $output = [];

    foreach ( $data as $row ){
      $output[ $row[self::INST_ID] ] = $row[self::INST_NAME];
    }

    return $output;

  }


  public function getFormOptionsInstitutionsByCity( string $select_one_label = null, $add_new_label = null, $actions_label = "Akce" ): array
  {

    // Initialise the container with optional actions
    $output = $this->initOptionsContainer($select_one_label, $add_new_label, $actions_label);

    // Load cities first
    $cities = $this->database->table( "locality" )
      ->where( "depth", 2 )
      ->order( "name", "ASC" );

    foreach ( $cities as $city ) {
      $institutions = $this->database->table( self::TABLE_BASE )
        ->where( self::INST_CITY, $city->id )
        ->order( self::INST_NAME, "ASC" );

      if ( $institutions->count() > 0 ) {
        $output[ $city->name ] = [];
        foreach ( $institutions as $institution ) {
          $output[ $city->name ][ $institution->id ] = $institution->name;
        }
      }
    }

    return $output;
  }

  /**
   * Get array of data to DataGrid
   * Number of related users are not loaded here! 
   * @see UserInstitutionRelationManager::getRelatedUsers( $institution_id, $role )
   */
  public function getAdminInstitutions(): array
  {
    
    $data = $this->database->table( self::TABLE_BASE )
      ->order( self::INST_NAME . " ASC" );

    $output = [];

    foreach ( $data as $row ) {
      $output[ $row[ self::INST_ID ] ] = array_merge( [
        "id" => $row[self::INST_ID],
        "name" => $row[self::INST_NAME],
        "types" => $this->getInstitutionTypes( $row[self::INST_ID] ),
      ],
      $this->getInstitutionLocalitiesNames( $row->id )
    );
    }

    return $output;

  }



  // CONDITIONS

  /**
   * Does an institution exist?
   * @param int $id
   * @return bool
   */
  public function institutionExists( int $id ): bool
  {
    return $this->database->table( self::TABLE_BASE )
      ->where( self::INST_ID, $id )
      ->count() > 0; 
  }

  public function institutionExistenceCheck( int $id ): void
  {
    if ( ! $this->institutionExists( $id ) ) {
      throw new \App\Model\MissingItemException("Missing institution", self::TABLE_BASE, 0, [ "id"=>$id ] );
    }
  }

  /**
   * Is the integer a valid institution type?
   * @param int $type_id
   * @return bool
   * @uses self::TYPE for definitions
   */
  public function isValidInstitutionTypeId( int $type_id ): bool
  {
    return array_key_exists( $type_id, self::TYPE );
  }

  /**
   * Is the string a valid institution slug?
   * @param int $type_abbr
   * @return bool
   * @uses self::TYPE for definitions
   */
  public function isValidInstitutionTypeSlug( string $type_abbr ): bool
  {
    return in_array( $type_abbr, self::TYPE );
  }





  // FORMATTERS & HELPERS

  /**
   * Form validators
   */


  // TYPE-related stuff






}
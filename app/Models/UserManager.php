<?php

/**
 * TODO:
 * - zvážit, zda neimplementovat všude OutOfRangeException
 */

namespace App\Model;

use Nette;
use Nette\Database\Table\Selection;

final class UserManager extends \App\Model\BaseManager
{ 

  /** @var Nette\Security\Passwords */
  private $passwords;
  
  /** @var Nette\Database\Explorer */
  private $database;

  /** @var Nette\Localisation\ITranslator */
  private $translator;

  const 
    TABLE_BASE = "user",
    TABLE_ROLES = "user_role",
    TABLE_ACTIONS = "user_action",
    TABLE_INST_REL = "user_institution",
    USER_ID = "id",
    USER_EMAIL = "email",
    USER_PASSWORD = "password",
    USER_NAME = "name",
    USER_SURNAME = "surname",
    USER_CITY_ID = "city_id",
    USER_ABOUT = "about",
    ROLE_USER_ID = "user_id",
    ROLE_ID = "role_id";

  const ROLES = [
    0 => "guest",
    1 => "member",
    2 => "confirmed",
    3 => "content_creator",
    4 => "public",
    5 => "teacher",
    6 => "student",

    30 => "admin"

  ];

  public function __construct(
    Nette\Database\Explorer $database,
    Nette\Security\Passwords $passwords,
    Nette\Localization\Translator $translator
  )
	{
    $this->passwords = $passwords;
    $this->database = $database;
    $this->translator = $translator;
  }








  /**
   * Get array of available roles
   * @param bool|null $names Return translated names, or keys?
   * @param bool|null $descriptions Include role rescriptions
   * @return array [ $id => $slug ]
   * @see self::ROLES
   */
  public function getFormArrayAvailableRoles( ?bool $names = false, ?bool $description = false, ?bool $guest = true ): array
  {

    if ( ! $names ) {
      return self::ROLES;
    } else {
      return array_map(function( $item ) use ($description) {
        
        $string = $this->translator->translate( "user.role.".$item.".name");

        if ( $description ) {
          $string = sprintf( '%1$s - %2$s', $string, $this->translator->translate( "user.role.".$item.".desc" ) );
        }

        return $string;

      }, array_filter( self::ROLES, function( $item ) use ($guest){
        if ( ! $guest ) {
          return $item != "guest";
        } else {
          return true;
        }
        
      }));
    }
  }









  // SETTERS

  /**
   * Create a user for a given email
   * @param string $email
   * @param int|null $code Number to identify errors.
   * @return int ID of the new entity
   * @throws \App\Model\InputValidationException Ivalid email format
   * @throws \App\Model\DupliciteItemException Email already exist.
   */
  public function addUser( string $email, ?int $code = 0 ): int
  {

    // Input must be units
    if ( ! Nette\Utils\Validators::isEmail($email) ) {
      throw new InputValidationException("The email is invalid", $code );
    }
    
    // Email must be unique!
    if ( $this->userEmailExists( $email ) ) {
      throw new DupliciteItemException("User already exists", $code );
    }

    $entity = $this->database->table( self::TABLE_BASE )
      ->insert( [
        "email" => $email
      ] );

    return $entity->id;

  }


  /**
   * Sets user email veryfiing if the email is available
   * @param int $user_id
   * @param string $value A valid email address
   * @param int|null $code default = 0
   * @throws MissingItemException
   * @throws InputValidationException
   * @throws DupliciteItemException
   */
  public function setUserEmail( $user_id, $value, $code = 0 ): void
  {
    
    // Check if user exists
    if ( ! $this->userExists( $user_id ) ) {
      throw new MissingItemException("user does not exist",$code);
    }
    
    // Check if a valid value had been provided
    if ( ! \Nette\Utils\Validators::isEmail( $value ) ) {
      throw new InputValidationException( "The email is not valid", $code );
    }

    // Duplicite email check
    $prev_data = $this->getUser( $user_id );
    if ( $prev_data->email != $value ) {  
      if ( $this->userEmailExists( $value ) ) {
        throw new DupliciteItemException("The email is already taken!", $code );
      }
    }

    // Perform the database operation
    $this->database->table( self::TABLE_BASE )
      ->where( self::USER_ID, $user_id )
      ->update( [ self::USER_EMAIL => $value ] );

  }

  public function setUserCity( int $user_id, int $city_id ): void
  {
    $this->database->table( self::TABLE_BASE )
      ->where( self::USER_ID, $user_id )
      ->update([
        self::USER_CITY_ID => $city_id
      ]);
  }

  /**
   * Set user name and surname
   * @param int $user_id
   * @param string $name
   * @param string $surname
   * @param int|null $code
   */
  public function setUserName( $user_id, $name, $surname, ?int $code = 0 ): void
  {

    if ( ! $this->userExists($user_id) ) {
      throw new MissingRelatedItemException( "Uživatel neexistuje", $code );
    }

    $this->database->table( self::TABLE_BASE )
      ->where( self::USER_ID, $user_id )
      ->update( [
        self::USER_NAME => $name,
        self::USER_SURNAME => $surname
      ] );
  }

  /**
   * Set user name and surname
   * @param int $user_id
   * @param string $name
   * @param string $surname
   * @param int|null $code
   */
  public function setAbout( int $user_id, string $about, ?int $code = 0 ): void
  {

    if ( ! $this->userExists($user_id) ) {
      throw new MissingRelatedItemException( "Uživatel neexistuje", $code );
    }

    $this->database->table( self::TABLE_BASE )
      ->where( self::USER_ID, $user_id )
      ->update( [
        self::USER_ABOUT => $about
      ] );
  }

  /**
   * Validates a new password and save it to the database
   * @param int $user_id
   * @param string $value
   * @param string|null $value_repetition
   * @param int|null $code
   * @throws \App\Model\MissingItemException
   * @throws \App\Model\InputValidationException $code
   */
  public function setUserPassword( int $user_id, string $value, string $value_repetition = null, ?int $code = 0 ): void
  {

    // Validate user existence
    if ( ! $this->userExists( $user_id ) ) {
      throw new MissingItemException( "Uživatel neexistuje!", $code );
    }

    // If the second password exist, validate it
    if ( $value_repetition != null || $value_repetition != "" || ! empty( $value_repetition ) ) {

      // Compare the second value against the first one
      if ( $value_repetition !== $value ) {
        throw new InputValidationException("Hesla se neshodují!", $code, ["type"=>"mismatch"]);
      }

    }

    // Validate the password strength
    $fails = [];

    // Password length
    if ( strlen( $value ) < 8 ) { 
      $fails[] = $this->translator->translate("fields.password.params.length"); 
    }

    // Password contains a upperscale letter
    if ( ! preg_match('/[A-Z]/', $value ) ) {
      $fails[] = $this->translator->translate("fields.password.params.uppercase"); 
    }

    // Password contains a lowersize letter
    if ( ! preg_match('/[a-z]/', $value ) ) {
      $fails[] = $this->translator->translate("fields.password.params.lowercase"); 
    }

    // Password contains a digit
    if ( ! preg_match('/[0-9]/', $value ) ) {
      $fails[] = $this->translator->translate("fields.password.params.digit"); 
    }

    if ( count( $fails ) > 0 ) {

      throw new InputValidationException("Wrong password format", $code, ["type"=>"strength","hints"=> $fails]);
    }

    // If everything went well, hash the pass & save it

    // Encode the password
    $pass = $this->passwords->hash( $value );
    
    // Save the value
    $this->database->table( self::TABLE_BASE )
      ->where( self::USER_ID, $user_id )
      ->update( ["password" => $pass ] );

  }

  /**
   * Performs validation of the two passwords and throws appropriate exceptions
   * @throws \App\Model\InputValidationException
   */
  public function validatePasswords( string $password_one, ?string $password_two = null, ?int $code = 0 ): void
  {

    // Compare the second value against the first one
    if ( $password_two !== $password_one ) {
 
      throw new InputValidationException("Hesla se neshodují!", $code, ["type"=>"mismatch"]);

    }

  }

  /**
   * Validate a single password and throw appropriate exceptions
   * @throws InputValidationException
   */
  public function validatePassword( string $password, ?int $code = 0 ): bool
  {

    // Validate the password strength
    $fails = [];

    // Password length
    if ( strlen( $password ) < 8 ) { 
      $fails[] = $this->translator->translate("fields.password.params.length"); 
    }

    // Password contains a upperscale letter
    if ( ! preg_match('/[A-Z]/', $password ) ) {
      $fails[] = $this->translator->translate("fields.password.params.uppercase"); 
    }

    // Password contains a lowersize letter
    if ( ! preg_match('/[a-z]/', $password ) ) {
      $fails[] = $this->translator->translate("fields.password.params.lowercase"); 
    }

    // Password contains a digit
    if ( ! preg_match('/[0-9]/', $password ) ) {
      $fails[] = $this->translator->translate("fields.password.params.digit"); 
    }

    // If the password is not valid, throw exceptions
    if ( count( $fails ) > 0 ) {

      throw new InputValidationException("Wrong password format", $code, ["type"=>"strength","hints"=> $fails]);

    }

    // If the password is valid, return true
    return true;

  }

  /**
   * Modify user<>role relationship for existing users
   * @param int $user_id
   * @param int[] $roles Array of IDs ot roles
   * @throws \App\Model\MissingItemException When user does not exist
   * @throws \App\Model\DictonaryValidationException One of roles is not valid
   */
  public function setUserRoles( int $user_id, array $roles = array(), ?int $code = 0 ): void 
  {
    if ( ! $this->userExists( $user_id ) ) {

      throw new MissingItemException("User does not exist.", $code );

    }

    // Load previous roles
    $prev = $this->getUserRoles( $user_id );

    // Add new roles (missing in $prev)
    foreach ( array_diff( $roles, $prev ) as $new ) {
      $this->addUserRole( $user_id, $new );
    }

    // Delete removed roles (missing in $roles)
    foreach ( array_diff( $prev, $roles ) as $old ) {
      $this->removeUserRole( $user_id, $old );
    }

  }

  /**
   * Assign a user to a role
   * @param int $user_id
   * @param string $role_id
   * @throws \App\Model\DictionaryValidationException 0 = The given role is not valid!
   * @see setUserRoles()
   * @todo Refactor user roles from strings to integers!
   */
  public function addUserRole( int $user_id, int $role_id ): void
  {

    // Check if the user has the proper role
    if ( ! in_array( $role_id, array_keys( self::ROLES ) ) ) {
      throw new DictionaryValidationException( "Role is not valid!", self::TABLE_ROLES, 0, ["role"=>$role_id] );
    }

    if ( ! $this->userHasRole( $user_id, $role_id ) && $this->userExists( $user_id ) ) {

      $this->database->table( self::TABLE_ROLES )
        ->insert([
          self::ROLE_USER_ID => $user_id,
          self::ROLE_ID => $role_id
        ]);

    }
  }

  /**
   * Remove a user from a role
   * @param int $user_id
   * @param string $role_id
   * @see setUserRoles()
   * @todo Refactor user roles from strings to integers!
   */
  public function removeUserRole( int $user_id, int $role_id ): void
  {
    $this->database->table( self::TABLE_ROLES )
      ->where( self::ROLE_USER_ID, $user_id )
      ->where( self::ROLE_ID, $role_id )
      ->delete();
  }

  /**
   * Delete a user and related content
   * @param int $user_id
   */
  public function deleteUser( int $user_id ): void 
  {
    // Delete the main entry
    $this->database->table( self::TABLE_BASE )
      ->where( self::USER_ID, $user_id )
      ->delete();

    // Delete related roles
    $this->database->table( self::TABLE_ROLES )
      ->where( self::ROLE_USER_ID, $user_id )
      ->delete();
      
  }



  /** 
   * Retrieve user data from the database
   * @param int|string ID user
  */
  public function getUser( int $user_id )
  {

    return $this->database->table( self::TABLE_BASE )->get( $user_id );

  }

  /** 
   * Retrieve the user name
   * @param int $user_id 
   * @param int|null $code
   * @return string A formatted user name
   * @throws MissingItemException
   */
  public function getUserName( int $user_id, ?int $code = 0 ): string
  {

    if ( ! $this->userExists( $user_id ) ) {
      throw new MissingItemException( "User does not exist!", $code );
    }

    // Load the user data
    $data = $this->getUser( $user_id );

    $pieces = [];

    // Look for real names first
    if ( $data->name ) { $pieces[] = $data->name; }
    if ( $data->surname ) { $pieces[] = $data->surname; }

    // Add the email if no name was given
    if ( count( $pieces ) == 0 ) { $pieces[] = $data->email; }

    return implode( " ", $pieces );

  }

  /**
   * Get user ID from email
   * @param string $email
   * @param int|null $code
   * @return int ID of the user
   * @throws MissingItemException
   * @throws InputValidationException
   */
  public function getUserId( string $email, ?int $code = 0 ): int 
  {

    // Validate the input
    if ( ! \Nette\Utils\Validators::isEmail( $email ) ) {
      throw new InputValidationException("Provide a valid email!", $code );
    }

    // Check if the email exist
    if ( ! $this->userEmailExists( $email ) ) {
      throw new MissingItemException("User with the given email does not exist", $code );
    }

    // Load the email
    $result = $this->database->table( self::TABLE_BASE )
      ->select( self::USER_ID )
      ->where( self::USER_EMAIL, $email )
      ->fetch();

    return $result->id;
  }

  /**
   * Get user ID from email
   * @param string $email
   * @param int|null $code
   * @return int ID of the user
   * @throws MissingItemException
   */
  public function getUserEmail( int $id, ?int $code = 0 ): string
  {
    if ( ! $this->userExists( $id ) ) {
      throw new MissingItemException( "Uživatel neexistuje", $code );
    }
    return $this->database->table( self::TABLE_BASE )
      ->get( $id )->email;
  }

  public function getUsersByRole( int $role ): Selection
  {
    $relations = $this->database->table( self::TABLE_ROLES )
      ->where( self::ROLE_ID . " = ? OR " . self::ROLE_ID . " = ?", $role, 30 );

    $ids = [];
    foreach ( $relations as $relation ) {
      $ids[] = $relation->user_id;
    }

    return $this->database->table( self::TABLE_BASE )
      ->where( self::USER_ID, $ids );
  }

  public function getPublicUsersByCity( int $city_id ): Selection
  {
    
    // Load all public users
    $query = $this->getUsersByRole( 4 );

    // Filter by the city
    $query->where( self::USER_CITY_ID, $city_id );

    // Order users
    $query->order( self::USER_SURNAME . " ASC" );

    return $query;

  }

  public function getPublicUsersCities(  ): Selection
  {
    
    // Load all public users
    $query = $this->getUsersByRole( 4 );

    $query->select( "COUNT(" . self::USER_ID . ")" );

    $query->select( self::USER_CITY_ID );
    

    // Filter by the city
    $query->group( self::USER_CITY_ID );

    // Order users
    // $query->where( self::USER_SURNAME . " ASC" );

    return $query;

  }

  public function getFormArrayUsesByRole( int $role ): array
  {

    $users = $this->getUsersByRole($role)->order( self::USER_EMAIL, 'DESC' );

    $output = [];

    foreach ( $users as $user ) {
      $output[ $user->id ] = $user->email;
    }

    return $output;
  }


  /**
   * Retrieve user role IDs, validating user existence
   * @param int ID of the user
   * @param int|null $code
   * @return int[] array of roles
   * @throws \App\Model\MissingItemException
   * @todo Must this check user access?
   */
  public function getUserRoles( int $user_id, ?int $code = 0 ): array
  {
    
    // Check if the user exists
    if ( ! $this->userExists( $user_id ) ) {

      throw new MissingItemException("User does not exist.", $code );

    } else {
    
      // Load roles from the DB
      $roles = $this->database->table( self::TABLE_ROLES )
        ->select( self::ROLE_ID )
        ->where( self::ROLE_USER_ID, $user_id );

      $output = [];

      foreach ( $roles as $role ) {
        $output[] = $role->role_id;
      }

      return count( $output ) > 0 ? $output : [0];

    }

  }

  /**
   * Retrieve user role slugs, validating user existence
   * @param int ID of the user
   * @return string[] array of roles
   * @throws \App\Model\MissingItemException
   * @todo Must this check user access?
   */
  public function getUserRolesSlugs( int $user_id ): array
  {
    return array_map( function($role_id){
      return self::ROLES[$role_id];
    }, $this->getUserRoles( $user_id ) );
  }

  /**
   * Get overview of users for admin overviews with configurable data
   * @param array $fields Array of fields to include.
   */
  public function getAdminUsers( bool $roles = false ){

    $users_data = $this->database->table( self::TABLE_BASE );

    $output = [];

    foreach ( $users_data as $row ) {

      $output[ $row->id ] = array(
        "id" => $row->id,
        "email" => $row->email,
        "name" => $row->name,
        "surname" => $row->surname
      );

    }

    if ( $roles ) {
      foreach ( $users_data as $row ) {

        $roles_all = ["member","admin","content_creator","public","confirmed"];
        $roles_user = $this->getUserRolesSlugs( $row->id );

        foreach ( $roles_all as $role ) {
          $output[ $row->id ][ $role ] = in_array($role,$roles_user) ? $role : "";
        }

      }

    }

    return $output;

  }

  public function getFormArrayUsers(): array
  {
    $users_rel = $this->database->table( "user_role" )
      ->where( "role_id", 3 );
  }

  

  



  
  
  




  /**
   * CHECKERS
   * 
   * Functions performing database checks of various fields
   */

  /**
   * Does an user with a given ID exist?
   * @param int $user_id
   * @return bool
   */
  public function userExists( int $user_id ): bool
  {
    return $this->database->table(self::TABLE_BASE)->get( $user_id ) != null;
  }

  /**
   * Does an user with a given email exist?
   * @param string $email
   * @return bool
   */
  public function userEmailExists( string $email ): bool
  {
    
    $count = $this->database->table(self::TABLE_BASE)
      ->where( self::USER_EMAIL, $email )->count();

    return $count == 0 ? false : true;

  }


  /**
   * Database control of user <> role relationship
   * @param int $user_id 
   * @param int $role_id
   * @return boolean
   */
  public function userHasRole( int $user_id, int $role_id ): bool
  {
    $data = $this->database->table( self::TABLE_ROLES )
      ->where( self::ROLE_USER_ID, $user_id )
      ->where( self::ROLE_ID, $role_id );
    return $data->count() > 0;
  }





}
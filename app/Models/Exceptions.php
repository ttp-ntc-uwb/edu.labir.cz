<?php

namespace App\Model;

class BaseException extends \Exception {


  /** @var array Additional data array */

  public function __construct( string $message, int $code = 0, $data = [] ){

    parent::__construct($message, $code);

    $this->data = $data;
  }

  public function getData()
  {
    return $this->data;
  }


}

/**
 * The primary entity is missing
 */
class MissingItemException extends BaseException {}

/**
 * Primary item already exists
 */
class DupliciteItemException extends BaseException {}

/**
 * A related entity does not exist
 */
class MissingRelatedItemException extends BaseException {}

/**
 * An input is not valid
 */
class InputValidationException extends BaseException {}

/**
 * An input is missing
 */
class MissingInputException extends BaseException {}

/**
 * A value is not valid against a predefined dictionary of values.
 * 
 * Intended for data that are defined in constants such as "roles", "types" and so...
 */
class DictionaryValidationException extends BaseException {}

/**
 * Data structure exception
 */
class StructureException extends BaseException {}


/**
 * A special exception intended for handling order of execution
 */
class NotYetException extends BaseException {}

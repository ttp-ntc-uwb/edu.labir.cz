<?php

namespace App\Model;

use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Utils\Random;

/**
 * Handles all operations against the request table
 */
final class RequestManager extends BaseManager
{

  /** @var Nette\Database\Explorer */
  public $database;

  const TABLE_BASE = "requests",
    REQUEST_ID = "id",
    REQUEST_TYPE = "type",
    REQUEST_USER_ID = "user_id",
    REQUEST_RELATED_ENTITY = "related_id",
    REQUEST_HASH = "hash",
    REQUEST_FROM = "time_from",
    REQUEST_TO = "time_to",
    REQUEST_DATA = "data";


  public function __construct(
    Nette\Database\Explorer $database
  )
	{
    $this->database = $database;
  }


  /**
   * A common method for creating actions
   * @param bool $isUnique Should we delete any existing actions of the same type created by this user?
   * @param int $type
   * @param int $user_id
   * @param string|null $to Optional time to which this request is valid.
   * @param int|null $related_entity_id
   * @param array $data
   * @return ActiveRow
   */
  public function addRequest( bool $isUnique, int $type, int $user_id, ?string $to, ?int $related_entity_id = null, ?array $data = null ): ActiveRow
  {

    // Delete conflicting requests if the new one is unique
    if ( $isUnique ) {
      $this->deleteUserRequests($user_id, $type);
    }

    $insert = [
      self::REQUEST_USER_ID => $user_id,
      self::REQUEST_TYPE => $type,
      self::REQUEST_FROM => new \DateTime,
      self::REQUEST_HASH => $this->generateHash( $type, $user_id ),
    ];

    if ( !empty( $related_entity_id ) ) {
      $insert[ self::REQUEST_RELATED_ENTITY ] = $related_entity_id;
    }

    if ( $to ) {
      $insert[ self::REQUEST_TO ] = new \DateTime( $to );
    }
    if ( !empty( $data ) ) {
      $insert[ self::REQUEST_DATA ] = json_Encode( $data );
    }

    return $this->database->table( self::TABLE_BASE )
      ->insert( $insert );

  }

  public function deleteRequest( int $id ): void
  {
    $this->database->table( self::TABLE_BASE )
      ->get( $id )
      ->delete();
  }

  /**
   * Cancel all requests related to an entity
   * @param int $related_entity_id
   * @param int|null $type
   * @return int Number of deleted requests
   */
  public function deleteRelatedEntityRequests( int $related_entity_id, ?int $type = null ): int
  {
    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::REQUEST_RELATED_ENTITY, $related_entity_id );
    
    if ( $type != null ) {
      $query->where( self::REQUEST_TYPE, $type );
    }

    return $query->delete();

  }

  /**
   * Delete all requests related to a user
   * @param int $user_id
   * @param int|null $type
   * @return int Number of deleted requests
   */
  public function deleteUserRequests( int $user_id, ?int $type = null ): int
  {
    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::REQUEST_USER_ID, $user_id );

    if ( $type != null ) {
      $query->where( self::REQUEST_TYPE, $type );
    }

    return $query->delete();
  }

  public function getRequest( int $id ): ActiveRow
  {
    return $this->database->table( self::TABLE_BASE )
      ->get( $id );
  }

  public function getRequestFromHash( string $hash ): ?ActiveRow
  {

    return $this->database->table( self::TABLE_BASE )
      ->where( self::REQUEST_HASH, $hash )
      ->fetch();
  }

  public function getUserRequests( int $user_id, int $type = null ): Selection
  {

    $query = $this->database->table( self::TABLE_BASE )
      ->where( self::REQUEST_USER_ID, $user_id );

    if ( $type !== null ) {
      $query->where( self::REQUEST_TYPE );
    } 

    return $query;

  }

  /**
   * RequestIsValid
   */
  public function requestIsValid( int $request_id ) {
    $request = $this->getRequest( $request_id );
    if ( empty( $request[self::REQUEST_TO] ) ) {
      return true;
    } else {
      // here should go the time validation
      return true;
    }
  }

  /**
  * Generate a relatively safe hash and check its uniqueness
   */
  private function generateHash( int $type, int $user_id ): string
  {
    return md5( $user_id . Random::generate(6) . $type . time() );
  }

}
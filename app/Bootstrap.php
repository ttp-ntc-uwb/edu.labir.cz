<?php

declare(strict_types=1);

namespace App;

use Nette\Bootstrap\Configurator;


class Bootstrap
{

	public static function boot(): Configurator
	{
		$configurator = new Configurator;
		$appDir = dirname(__DIR__);

		$configurator->setDebugMode(true); // enable for your remote IP
		$configurator->enableTracy($appDir . '/log');

		$configurator->setTimeZone('Europe/Prague');
		$configurator->setTempDirectory($appDir . '/temp');

		$configurator->createRobotLoader()
			->addDirectory(__DIR__)
			->register();

		// Basic configuration
		$configurator->addConfig($appDir . '/config/common.neon');

		// ACL
		$configurator->addConfig( $appDir . '/config/acl.neon' );

		// Image storage
		$configurator->addConfig( $appDir . '/config/images.neon' );
		
		// Local configuration (not in repository)
		$configurator->addConfig($appDir . '/config/local.neon');

		// Translation
		$configurator->addConfig($appDir . '/config/translations.neon');

		// Mailing
		// $configurator->addConfig( $appDir . '/config/mailing.neon' );

		// Menus
		$configurator->addConfig($appDir . '/app/BaseModule/config/menu.front.neon');
		$configurator->addConfig($appDir . '/app/BaseModule/config/menu.admin.neon');
		$configurator->addConfig($appDir . '/app/BaseModule/config/menu.partners.neon');
		$configurator->addConfig($appDir . '/app/BaseModule/config/menu.social.neon');
		$configurator->addConfig($appDir . '/app/BaseModule/config/menu.sponsors.neon');
		$configurator->addConfig($appDir . '/app/BaseModule/config/menu.project.neon');
		$configurator->addConfig($appDir . '/app/BaseModule/config/menu.user.neon');

		// FrontModule specifics
		$configurator->addConfig($appDir . '/app/FrontModule/config/menu.documentation.neon');
		$configurator->addConfig($appDir . '/app/FrontModule/config/menu.sign.neon');

		// Admin menu specifics
		$configurator->addConfig($appDir . '/app/AdminModule/config/menu.settings.neon');

		return $configurator;
		
	}

}

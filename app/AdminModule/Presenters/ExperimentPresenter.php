<?php

namespace App\AdminModule\Presenters;

use App\FrontModule\Presenters\BaseFrontPresenter;
use Nette\Application\UI\Control;
use Contributte\FormsBootstrap\BootstrapForm;
use App\AdminModule\Forms\ExperimentFormFactory;
use App\AdminModule\Forms\ExperimentFileFormFactory;
use App\AdminModule\Forms\TaxonomyFormFactory;
use App\Model\ExperimentManager;
use App\Model\ExperimentFileManager;
use App\Model\TaxonomyManager;
use Ublaboo\DataGrid\DataGrid;
use Contributte\ImageStorage\ImageStoragePresenterTrait;
use Nette\Application\UI\Form;
use Nette\Utils\Html;
use Nette\Utils\FileSystem;
use stdClass;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;

class ExperimentPresenter extends BaseFrontPresenter {

  use ImageStoragePresenterTrait;

  /** @var ExperimentFormFactory @inject */
  public $experimentFormFactory;

  /** @var ExperimentFileFormFactory @inject */
  public $experimentFileFormFactory;

  /** @var TaxonomyFormFactory @inject */
  public $taxonomyFormFactory;

  /** @var ExperimentManager @inject */
  public $experimentManager;

  /** @var TaxonomyManager @inject */
  public $taxonomyManager;

  /** @var ExperimentFileManager @inject */
  public $experimentFileManager;

  
  public function startup(): void
  {
    
    parent::startup();

    // Template
    $this->template->layoutContainer = "container";
    $this->template->layoutRow = "row";
    $this->template->layoutColumn = "col-12";

    $this->useLayout("@simple.latte");

  }


  

  /**
   * Public views
   */

  /**
   * Helper for list views
   * Loads and creates the filters
   */
  public function prepareListView( string $slug = null ): void
  {

    $filters = [
      [
        "scope" => "subject",
        "name" => $this->_t( "subject.res.pl.nom" ),
        "view" => "category",
        "type" => $this->taxonomyManager::TYPE_SUBJECT
      ],
      [
        "scope" => "tag",
        "name" => $this->_t( "tag.res.pl.nom" ),
        "view" => "category",
        "type" => $this->taxonomyManager::TYPE_TAG
      ],
      [
        "scope" => "folder",
        "name" => $this->_t( "folder.res.pl.nom" ),
        "view" => "category",
        "type" => $this->taxonomyManager::TYPE_FOLDER
      ]
    ];

    $this->template->filters = [];

    foreach ( $filters as $scope ) {

      $items = [];

      $data = $this->taxonomyManager->getTaxonomiesOfType( $scope["type"] );

      foreach ( $data as $item ) {

        $experiments = $this->experimentManager->getFrontendExperimentsByTaxonomy( $item->id, ! $this->user->isLoggedIn() );

        if ( $experiments->count() > 0 ) {
          $items[ $item->slug ] = [
            "slug" => $item->slug,
            "count" => $experiments->count(),
            "name" => $item->name,
            "view" => $scope["view"],
            "class" => $item->slug == $slug ? "active" : "inactive",
          ];
        }

      }

      if ( count( $items ) > 0 ) {
        $this->template->filters[ $scope["scope"] ] = [
          "title" => $scope["name"],
          "items" => $items
        ];
      }

    }

  }

  /**
   * List all ewperiments
   * @uses prepareListView
   */
  public function renderDefault(): void
  {

    $this->template->title = $this->_t("experiment.res.pl.nom");
    $this->template->titleSuffix = $this->_t("experiment.all");
    $this->prepareListView(null);

    $this->template->layoutColumn = false;
    $this->template->layoutContainer = "container-fluid";

    $this->template->experiments = $this->aggregateExperimentsAll();

    // Seo metadata

    $this->template->description = "Výukové materiály, experimenty, pokusy či termovizní inspirace.";

    $this->template->keywords = implode(", ", [
      $this->_t("experiment.res.pl.nom")
    ]);

  }

  /**
   * List experiments in a single category
   * @uses prepareListView
   */
  public function renderCategory( string $slug ): void
  {

    $this->prepareListView($slug);

    $taxonomy = $this->taxonomyManager->getTaxonomyBySlug( $slug );

    $this->template->titleSuffix = true;

    // Seo metadata
    $this->template->titleSeoPrefix = $this->_t("experiment.res.pl.nom") . " - ";

    $this->template->titleSeo = $taxonomy->name;

    $this->template->titleOg = $this->template->titleSeoPrefix . $this->template->titleSeo;

    $this->template->description = strip_tags( $taxonomy->description );

    $this->template->keywords = implode(", ", [
      $this->_t("experiment.res.pl.nom"),
      $taxonomy->name
    ]);

    // Layout properties

    $this->template->layoutColumn = false;

    $this->template->layoutContainer = "container-fluid";

    $this->template->title = $this->_t("experiment.res.pl.nom");

    $this->template->titleSuffix = $taxonomy->name;

    if ( $taxonomy != null ) {

      $this->template->experiments = $this->aggregateExperimentsByTaxonomy( $taxonomy->id, ! $this->getUser()->isLoggedIn() );

    } else {
      $this->template->experiments = [];
    }

    $this->template->taxonomy = $taxonomy;

  }

  /**
   * One experiment detail
   */
  public function renderDetail( string $slug ): void
  {

    // The experiment must exist
    $this->checkExperimentExistence( $slug );

    // Load the experiment
    $experiment = $this->experimentManager->getExperimentBySlug( $slug );

    // The user must have the right to see the current experiment
    $this->checkExperimentVisibilityToUser( $experiment->id );

    $this->useLayout("@layout.latte");



    // Seo metadata

    $this->template->description = strip_tags($experiment->teaser);

    $this->template->keywords = implode(", ", [
      $this->_t("experiment.res.sg.nom"),
      $experiment->name
    ]);



    // The base data
    $this->template->item = $experiment;

    // Thumbnail scaled down
    $this->template->thumb = $this->imageStorage->fromIdentifier([ $experiment->thumb, '150x150' ])->identifier;

    // Load related files

    // If the user can see anything...
    if ( 
      $this->getUser()->isInRole( "content_creator" )
      || $this->getUser()->isInRole( "admin" ) 
    ) { 
      
      $this->template->files = $this->experimentFileManager->getExperimentFiles( $experiment->id );
      $this->template->prompt = false;

    } 
    // else load only public files
    else {

      if ( $this->experimentFileManager->experimentHasFiles( $experiment->id, true ) ){

        $this->template->files = $this->experimentFileManager->getExperimentFiles( $experiment->id, true );

      } else {
        $this->template->files = null;
      }

      if ( $this->experimentFileManager->experimentHasFiles( $experiment->id, false ) ) {
        $this->template->prompt = true;
      } else {
        $this->template->prompt = false;
      }

    }

    // Related experiments
    $this->template->experiments = $this->aggregateExperimentsRandom(4, ! $this->getUser()->isLoggedIn(), [ $experiment->id ], [ $this->taxonomyManager::TYPE_SUBJECT ]);

    // Related subjects
    $this->template->subjects = $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_SUBJECT );

    // Related tags
    $this->template->tags = $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_TAG );

    // Related labels
    $this->template->labels = $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_LABEL );

    // Related folders
    $this->template->folders = $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_FOLDER );

    

  }

  /**
   * Administrative views
   */

  /**
   * Add an experiment
   */
  public function renderAdd(): void
  {

    // Only admins can access this view
    $this->checkIsAdmin();
    
    $form = $this->getComponent("experimentForm");

    $this->template->title = $this->formatOperationName("add","new","experiment",null,"sg","m");

  }

  /**
   * Edit one experiment
   */
  public function renderEdit( int $id ): void
  {

    // Accessible to admins only
    $this->checkIsAdmin();

    // The experiment must exist
    $this->checkExperimentExistence($id);

    $experiment = $this->experimentManager->getExperiment( $id );

    // Template variables

    $this->template->title = $this->formatOperationName("edit",null, "experiment");

    $this->template->titleSuffix = $experiment->name;

    $this->template->item = $experiment;

    // The main form

    $form = $this->getComponent("experimentForm");

    $form->setDefaults([
      "id" => $id,
      "name" => $experiment->name,
      "slug" => $experiment->slug,
      "teaser" => $experiment->teaser,
      "excerpt" => $experiment->excerpt,
      "text" => $experiment->text,
      "published" => $experiment->published,
      "public" => $experiment->public,
      "difficulty" => $experiment->difficulty,
      "subjects" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_SUBJECT ),
      "tags" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_TAG ),
      "folders" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_FOLDER ),
      "labels" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_LABEL ),
      "weight" => $experiment->weight,
      "users" => $this->experimentManager->getExperimentUserIds( $id )
    ]);

    // The files form

    $filesForm = $this->getComponent( "experimentFilesForm" );

    $filesForm->setDefaults([
      "experiment_id" => $id,
    ]);

    // The file overview
    $overview = $this->getComponent( "experimentFilesOverview" );
    $overview->setDataSource(
      $this->experimentFileManager->getExperimentFiles( $id )
    );

    $this->template->filesCount = $this->experimentFileManager->getExperimentFiles( $id )->count();

  }

  /**
   * Edit one experiment file
   */
  public function renderEditFile( $id ): void
  {

    $file = $this->experimentFileManager->getExperimentFile( $id );

    // Check if the user can edit the current experiment
    $this->checkExperimentEditabilityByUser( $file->experiment_id );

    $this->template->title = $this->formatOperationName("edit",null, "file");

    $this->template->titleSuffix = $file->name;

    $form = $this->getComponent( "experimentFilesForm" );

    $form->setDefaults([
      "id" => $id,
      "name" => $file->name,
      "description" => $file->description,
      "public" => $file->public,
      "experiment_id" => $file->experiment_id
    ]);

  }

  /**
   * List all experiments to admins
   */
  public function renderOverview(): void
  {

    // Only administrators can access
    $this->checkIsAdmin();

    $this->template->title = $this->formatOverviewName("overview","experiment");

    $grid = $this->getComponent( "experimentsAdmin" );
    $grid->setDataSource(
      $this->experimentManager->getAdminExperiments()
    );
  }

  /**
   * Creators views
   */

  /**
   * Creator adds an experiment
   */
  public function renderCreatorAdd(): void
  {

    // Accessible only to creators
    $this->checkIsCreator();

    $this->template->title = $this->formatOperationName("add","new","experiment");
  }

  /**
   * Creator edits his experiment
   */
  public function renderCreatorEdit( int $id ): void
  {

    $this->checkExperimentExistence($id);

    // Accessible only to creators
    $this->checkIsCreator();

    // Accessible only to owners
    $this->checkExperimentEditabilityByUser( $id );

    $experiment = $this->experimentManager->getExperiment( $id );

    // Template variables

    $this->template->title = $this->formatOperationName("edit",null,"experiment");

    $this->template->titleSuffix = $experiment->name;

    $this->template->item = $experiment;

    // The main form

    $form = $this->getComponent("experimentCreatorForm");

    $form->setDefaults([
      "id" => $id,
      "name" => $experiment->name,
      "slug" => $experiment->slug,
      "teaser" => $experiment->teaser,
      "excerpt" => $experiment->excerpt,
      "text" => $experiment->text,
      "published" => $experiment->published,
      "public" => $experiment->public,
      "difficulty" => $experiment->difficulty,
      "subjects" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_SUBJECT ),
      "tags" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_TAG ),
      "folders" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_FOLDER ),
      "labels" => $this->taxonomyManager->getExperimentTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_LABEL ),
      "weight" => $experiment->weight,
      "users" => $this->experimentManager->getExperimentUserIds( $id )
    ]);

    // The files form

    $filesForm = $this->getComponent( "experimentFilesForm" );

    $filesForm->setDefaults([
      "experiment_id" => $id,
    ]);

    // The file overview
    $overview = $this->getComponent( "experimentFilesOverview" );
    $overview->setDataSource(
      $this->experimentFileManager->getExperimentFiles( $id )
    );

    $this->template->filesCount = $this->experimentFileManager->getExperimentFiles( $id )->count();

  }

  /**
   * Creator's experiment overview
   */
  public function renderCreatorOverview(): void
  {

    // Accessible only by creators
    $this->checkIsCreator();

    $this->template->title = $this->formatOverviewName("overview","experiment",true);

    $grid = $this->getComponent("experimentsAdmin");

    $grid->setDataSource(
      $this->experimentManager->getUserExperiments( $this->user->getId() )
    );
  }


  
  /**
   * Signals
   */

  
  /**
   * Single file deletion
   */
  public function handleDeleteFile( int $id ): void
  {
    
    // Load the file from the DB

    $file = $this->experimentFileManager->getExperimentFile( $id );

    // The user can edit the current experiment
    $this->checkExperimentEditabilityByUser( $file->experiment_id );

    // Assamble the folder path

    $this->deleteFileFolder( $id );

    $experiment_id = $file->experiment_id;

    $this->experimentFileManager->deleteExperimentFile( $file->id );

    $this->flashResourceState("info","file","deleted",$file->name);

    $this->kickFurtner( $file->experiment_id );

  }

  public function handleDeleteExperiment( int $id ): void
  {
    
    // The experiment must exist
    $this->checkExperimentExistence( $id );

    // The user must have the access
    $this->checkExperimentEditabilityByUser( $id );

    // Load the experiment
    $experiment = $this->experimentManager->getExperiment($id);
    $name = $experiment->name;

    // Load the experiment files
    $files = $this->experimentFileManager->getExperimentFiles( $id );

    foreach ( $files as $file ) {
      $this->experimentFileManager->deleteExperimentFile($file->id);
    }

    // Delete the entire folder
    $basePath = "uploads/experiments/".$id;
    FileSystem::delete( $basePath );

    
    $this->experimentManager->deleteExperiment( $id );

    $this->flashResourceState("success","experiment","deleted",$name);

    $this->kickFurther();

  }

  /**
   * Helpers
   */
  public function deleteFileFolder( int $file_id ): void
  {

    $file = $this->experimentFileManager->getExperimentFile( $file_id );

    $pos = strpos( $file->path, "files/".$file->id);

    $basePath = substr( $file->path, 0, $pos ) . "files/".$file->id;

    if ( $file->scope == "image" ) {
      $basePath = "uploads/". $basePath;
    }

    FileSystem::delete( $basePath );

  }


  public function createComponentExperimentForm(): BootstrapForm
  {
    
    $form = $this->experimentFormFactory->create();

    if ( $this->getView() == "add" ) {

    }

    return $form;
  }

  public function createComponentExperimentCreatorForm(): BootstrapForm
  {
    $form = $this->experimentFormFactory->createUser();
    if ( ! $this->user->isInRole("admin") ) {
      $form->getComponent("slug")->setDisabled();
    }
    
    return $form;
  }

  public function createComponentExperimentFilesForm(): BootstrapForm
  {
    return $this->experimentFileFormFactory->create();
  }

  public function createComponentExperimentsAdmin( string $name ): DataGrid
  {

    $grid = new DataGrid( $this, $name );

    $grid->setDataSource( [] );

    $grid->addColumnText( "thumb", "Náhled" )
      ->setRenderer(function( $item ){

        $element = Html::el('img');

        if ( ! empty( $item->thumb ) ) {
          return $element
            ->addAttributes([
              'src'=>"/uploads/".$item->thumb,
              "style" => "max-width:100px;height:auto;"
            ]);
        }

      });

    // The main field
    $grid->addColumnText( "name", "Experiment" )
      ->setRenderer(function( $item ){

        $container = Html::el('div');

        $container->setAttribute("style","max-width:300px;");

        $container->create("div")->create("strong")->setHtml( $item->name );

        $container->create("div")->create("small")->setHtml( implode( " ", ["URL:",$item->slug] ))->setAttribute("class","color-secondary");


        $container->create("div")->setHtml( $item->excerpt );

        return $container;

      });

    $grid->addColumnText( "id", "Předměty" )
      ->setRenderer( function( $item ) {

        $subjects = $this->taxonomyManager->getExperimentTaxonomiesOfType( $item->id, $this->taxonomyManager::TYPE_SUBJECT );

        $container = Html::el("ul")->setAttribute("class","t-exp--tags");

        foreach ( $subjects as $subject ) {
          $container->create( "li" )->setAttribute("class","t-exp--tag__gray t-exp--tag__small")->setHtml($subject->name);
        }

        return $container;
      } );

    $grid->addColumnText( "public", "Veřejný" )
      ->setRenderer( function( $item ) {
        return Html::el('i')
          ->addAttributes([
            "class" => $item->public ? "fas fa-check-circle" : "far fa-times-circle"
          ]);
      } );

    $grid->addColumnText( "published", "Publikovaný" )
      ->setRenderer( function( $item ) {
        return Html::el('i')
          ->addAttributes([
            "class" => $item->published ? "fas fa-check-circle" : "far fa-times-circle"
          ]);
      } );

    $grid->addAction( 
      'detail', 
      "",
      null,
      [ "slug" ]
    )
      ->setIcon("eye")
      ->setClass('btn btn-xs btn-success');

    $grid->addAction( 
      $this->getUser()->isInRole("admin") ? 'edit' : 'creatorEdit', 
      ""
    )
      ->setIcon("edit")
      ->setClass('btn btn-xs btn-secondary');
    
    if ( $this->user->isInRole( "admin" ) ) {
    $grid->addAction(
      "deleteExperiment",
      "",
      "deleteExperiment!"
    )
      ->setConfirmation(
        new StringConfirmation( "Opravdu chcete smazat pokus %name%? Všechny jeho soubory i související záznamy budou otrvale dstraněny.", "name" )
      )
      ->setIcon( "trash" )
      ->setClass('btn btn-xs btn-danger');

    }
    return $grid;

  }

  public function createComponentExperimentFilesOverview( $name ): DataGrid
  {

    $grid = new DataGrid( $this, $name );

    $grid->setDataSource( [] );

    $grid->addColumnText( "thumbnail", "Náhled" )
      ->setRenderer(function( $item ){

        $element = Html::el('img');

        if ( ! empty( $item->thumbnail ) ) {
          return $element
            ->addAttributes([
              'src'=>"/uploads/".$item->thumbnail,
              "style" => "max-width:100px;height:auto;"
            ]);
        }

      });

    $grid->addColumnText( "name", "Soubor" )
    ->setRenderer( function( $item ) {
      $container = Html::el('div');

      $container->create("div")->create("strong")->setHtml( $item->name );

      $container->create("div")->setHtml( $item->filename );

      return $container;
    } );
    $grid->addColumnText( "public" , "Veřejný" )
    ->setRenderer( function( $item ) {
      return Html::el('i')
        ->addAttributes([
          "class" => $item->public ? "fas fa-check-circle" : "far fa-times-circle"
        ]);
    } );;

    $grid->addAction( 
      'editFile', 
      "",
    )
      ->setIcon( "edit" );

    $grid->addAction( 
      'deleteFile', 
      '',
      "deleteFile!"
    )
      ->setConfirmation(
        new StringConfirmation( "Opravdu chcete smazat soubor %name%?", "name" )
      )
      ->setIcon( "trash" );

    $grid->setPagination(false);

    $grid->addColumnCallback('thumbnail', function($column, $item) {
      return $item->scope;
      if ($item->id == 22) {
        $column->setRenderer(function() {
          return '';
        });
      }
    });

    return $grid;

  }

  public function createComponentExperimentsCreatorList( $name ): DataGrid
  {
    $grid = new DataGrid( $this, $name );

    $grid->setDataSource([]);
    $grid->addColumnText( "name", "Název" );

    $grid->addColumnText( "public", "Veřejný" )
      ->setRenderer( function( $item ) {
        return Html::el('i')
          ->addAttributes([
            "class" => $item->public ? "fas fa-check-circle" : "far fa-times-circle"
          ]);
      } );

    $grid->addColumnText( "published", "Publikovaný" )
      ->setRenderer( function( $item ) {
        return Html::el('i')
          ->addAttributes([
            "class" => $item->published ? "fas fa-check-circle" : "far fa-times-circle"
          ]);
      } );

    $grid->addAction( 
        'detail', 
        $this->_t( "common.op.detail" ),
        null,
        [ "slug" ]
      );
    $grid->addAction( 
        'creatorEdit', 
        $this->_t( "common.op.edit" )
      );

    return $grid;
  }


  /**
   * ACL Checkers
   */

  /**
   * Check is the current user is able to manage thhe current view
   */
  public function checkACL(): void
  {

    if ( ! $this->user->isAllowed( "experiment" ) ) {
      $this->kickOff();
    }

  }

  public function checkIsCreator(): void
  {
    if ( ! $this->userIsCreator() ) {
      $this->kickOff();
    }
  }

  public function checkIsAdmin(): void
  {
    if ( ! $this->userIsAdmin() ) {
      $this->kickOff();
    }
  }


  /**
   * The experiment must exist
   * @param string|int $experiment
   */
  public function checkExperimentExistence( $experiment ): void
  {

    if ( gettype( $experiment ) === "integer" ) {
      if ($this->experimentManager->getExperiment( $experiment ) == null ) {
        $this->flashResourceState("danger","experiment","does_not_exist", "ID:".$experiment );
        $this->redirect("Experiment:default");
      }
    }

    if ( gettype( $experiment ) === "string" ) {
      if ( $this->experimentManager->getExperimentBySlug( $experiment ) == null ) {
        $this->flashResourceState("danger","experiment","does_not_exist",$experiment);
        $this->redirect("Experiment:default");
      }
    }

  }

  public function userIsCreator(): bool
  {
    return $this->user->isInRole("content_creator");
  }

  public function userIsAdmin(): bool
  {
    return $this->user->isInRole( "admin" );
  }

  public function userCanManageExperiments(): bool
  {
    return $this->user->isAllowed( "experiment" );
  }

  public function userCanEditExperiment( int $experiment_id ): bool
  {
    
    $users = $this->experimentManager->getExperimentUserIds($experiment_id);
    
    if ( $this->user->isAllowed("experiment") || $this->user->isInRole("admin") || in_array( $this->user->getId(), $users )){
      return true;
    }
    
    return false;

  }

  public function checkExperimentEditabilityByUser( int $experiment_id ): void
  {
    
    // $experiment = $this->experimentManager->getExperiment( $experiment_id );

    if ( ! $this->userCanEditExperiment( $experiment_id ) ) {
      $this->kickOff();
    }

  }

  public function checkExperimentVisibilityToUser( int $experiment_id ): void
  {
    if ( ! $this->experimentIsVisibleToUser( $experiment_id ) ) {
      $this->kickOff();
    }
  }

  public function experimentIsVisibleToUser( int $experiment_id ): bool
  {
    
    // Load the experiment
    $experiment = $this->experimentManager->getExperiment($experiment_id);

    // Flash the eventual unpublished state

    // Published and public experiments are visible to all
    if ( $experiment->public == true && $experiment->published == true ) {
      return true;
    }
    

    // For logged in users
    if ( $this->user->isLoggedIn() ) {

      // Admins can edit everything
      if ( $this->userIsAdmin() ) {
        return true;
      }

      // Content creators can see:
      if ( $this->userIsCreator() ) {

        // Load the experiments authors
        $authors = $this->experimentManager->getUserExperimentsIds( $experiment_id );

        // 1. own content no matter if that is published or not
        if ( in_array( $this->user->getId(), $authors ) ) {
          return true;
        } 
        // 2. all other content unless it is published
        else if ( $experiment->published == true ) {
          return true;
        }


      }
      

      return false;


    } 
    
    // For anonymous users
    else {

      if ( $experiment->public == true && $experiment->published == true ) {
        return true;
      }

    }

    return false;
  }

  /**
   * Redirects
   */

  public function kickOff(): void
  {
    if ( $this->userIsCreator() ) {
      $this->flashACLError("edit","experiment");
      $this->redirect( ":Admin:Experiment:creatorOverview" );
    } else if ( $this->userIsAdmin() ) {
      $this->flashACLError("edit","experiment");
      $this->redirect(":Admin:Experiment:overview");
    } else {
      $this->flashACLError("edit","experiment");
      $this->redirect(":Front:Static:Default");
    }

    
  }

  public function kickFurther( int $experiment_id = null ): void
  {
    // If the experiment is provided, redirect to its edit page
    if ( $experiment_id !== null ) {
      if ( $this->userIsAdmin() ) {
        $this->redirect( ":Admin:Experiment:edit", ["id"=>$experiment_id] );
      } else if ( $this->userIsCreator()) {
        $this->redirect( ":Admin:Experiment:creatorEdit" ,["id"=>$experiment_id] );
      } else {
        $this->redirect( ":Admin:Experiment:default" );
      }
    } else {
      if ( $this->userIsAdmin() ) {
        $this->redirect( ":Admin:Experiment:overview" );
      } else if (  $this->userIsCreator()) {
        $this->redirect( ":Admin:Experiment:creatorOverview" );
      } else {
        $this->redirect( ":Admin:Experiment:default" );
      }
    }
  }

  


}
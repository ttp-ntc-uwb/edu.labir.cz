<?php

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use Contributte\FormsBootstrap\BootstrapContainer;
use Contributte\FormsBootstrap\BootstrapForm;
use Contributte\FormsBootstrap\Enums\RenderMode;
use Contributte\MenuControl\UI\MenuComponent;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Nette\Application\UI\Control;
use Nette\Forms\Validator;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Nette\Utils\Validators;

use App\Model\TaxonomyManager;

// Forms
use App\AdminModule\Forms\UserRolesFormFactory;
use App\AdminModule\Forms\UserSubjectsFormFactory;
use App\AdminModule\Forms\UserPasswordFormFactory;
use App\AdminModule\Forms\UserBasicInfoFormFactory;
use App\AdminModule\Forms\UserInstitutionFormFactory;

class UserPresenter extends BaseAdminPresenter
{

  /** @var \App\Model\UserManager @inject */
  public $userManager;

  /** @var \App\Model\InstitutionManager @inject */
  public $institutionManager;

  /** @var \App\Model\LocalityManager @inject */
  public $localityManager;

  /** @var TaxonomyManager @inject */
  public $taxonomyManager;

  /** @var \App\Model\UserInstitutionManager @inject */
  public $userInstitutionManager;

  /** @var \App\Model\RequestManager @inject  */
  public $requestManager;

  /** @var Nette\Security\User */
  public $user;

  /** @var \App\Services\Mailer $mailer @inject */
  public $mailer;

  /** @var UserRolesFormFactory @inject */
  public $userRolesFormFactory;

  /** @var UserSubjectsFormFactory @inject */
  public $userSubjectsFormFactory;

  /** @var UserPasswordFormFactory @inject */
  public $userPasswordFormFactory;

  /** @var UserBasicInfoFormFactory @inject */
  public $userBaseInfoFormFactory;

  /** @var UserInstitutionFormFactory @inject  */
  public $userInstitutionFormFactory;


  // Settings

  // Default redirectf for not allowed access
  const ACL_REDIRECTS = [
    "admin" => ":Admin:User:overview",
    "member" => ":Front:Static:default",
    "guest" => ":Front:Static:default"
  ];



  public function __construct(){

  }

  public function startup(): void
  {
    parent::startup();    

    $this->user = $this->getUser();

    $this->template->adminAssets = true;

  }

  /**
   * The administrative overview
   */
  public function renderOverview(): void
  {
    $this->canManageAllUsers(true);
  }

  /**
   * Add new user by administrator
   * @see :Front:Sign:register for non-administrative additions
   */
  public function renderAdd(): void
  {
    // Only managers can create users
    $this->canManageAllUsers(true);

    $this->template->title = $this->_t( "user.titles.add" );
    
    $form = $this->getComponent( "userFormFull" );

    $form->setDefaults([
        "mode" => 1,
      ]);

  }

  /**
   * Router for editation based on ACL
   */
  public function renderEdit( int $id ): void
  {

    // Redirect to a proper page

    // If a user is manager => Redirect to complete edit form
    if ( $this->canManageAllUsers( false ) ) {
      $this->redirect(":Admin:User:manage",["id" => $id]);
    }

    // If the user is self, redirect to profile settings page
    else if ( $this->canManageOneUser( $id, false ) ) {
      $this->redirect(":Admin:User:settings",[ "id" => $id ]);
    } 
    
    // In other cases, kick off completely
    else {
      $this->kickOff();
    }

  }


  public function renderDetail( int $id ): void
  {

    /** ACL can display self or admin */
    $this->canSeeOneUser( $id, false );

    try {

      // Load data to the template
      $this->template->userName = $this->userManager->getUserName( $id );

      // User details
      $this->template->details = $this->userManager->getUser( $id );

      // User subjects
      $this->template->subjects = $this->taxonomyManager->getUserTaxonomiesOfType( $id, $this->taxonomyManager::TYPE_SUBJECT );

      // User institutions
      


    } catch ( \Exception $e  ){

      $this->flashMessage( $e->getMessage() );
      $this->kickOff();

    }



  }



  /**
   * An administrative view to User:edit
   */
  public function renderManage( int $id ): void
  {

    // ACL check
    $this->canManageAllUsers( true );

    try {

      // Set template variables
      $this->template->titlePrefix = $this->_t("user.titles.edit");
      $this->template->title = $this->userManager->getUserName( $id );
      $this->template->titleSeo = $this->_t("user.titles.edit");

      // Load data

      $user = $this->userManager->getUser( $id );

      $roles = $this->userManager->getUserRoles( $id );

      $subjects = $this->taxonomyManager->getUserTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_SUBJECT );

      $institutions = $this->getUsersInstitutions( $id );

      $form = $this->getComponent( "userFormSelf" )
        ->setDefaults([
          "id" => $id,
          "name" => $user->name,
          "surname" => $user->surname,
          "email" => $user->email,
          "public" => $this->userManager->userHasRole( $id, 4 ),
          "city_id" => $user->city_id,
          "about" => $user->about
        ]);

        if ( $this->userManager->userHasRole( $id, 2 ) ) {

          $form["email"]->setOption(
            "description",
            Html::el( "div" )
              ->setAttribute( "class", "text-success" )
              ->setText( 
                $this->_t( "actions.user_email_verification.request.ok" )
              )
          );
        } else {
          
          $hint = false;
          
          if ( $this->requestManager->getUserRequests( $id, 2 )->count() == 0 ) {
            $hint = $this->_t( "actions.user_email_verification.request.renew" );
          } else {
            $hint = $this->_t( "actions.user_email_verification.request.timeout" );
          }
    
          $form["email"]->setOption(
            "description",
            Html::el("a")
              ->href( 
                "settings",
                [
                  "do" => "userEmailVerificationRequest",
                  "user_id" => $id,
                  "email" => $user->email
                ]
              )
              ->setText(
                $hint
              )
          );
    
        }

      $this->getComponent( "userFormRoles" )
        ->setDefaults([
          "id" => $id,
          "roles" => $roles
        ]);
      

      $this->getComponent( "userFormSubjects" )
        ->setDefaults([
          "id" => $id,
          "subjects" => $subjects
        ]);

      $this->getComponent( "userFormPassword" )
        ->setDefaults([
          "id" => $id
        ]);

      $this->getComponent( "userFormAddInstitutionRelationship" )
          ->setDefaults([
            "user_id" => $id,
            "valid" => $this->getUser()->isAllowed("user","edit")
          ]);

      $this->getComponent( "listUserInstitutions" )
        ->setDataSource( $institutions );


    } catch ( \App\Model\MissingItemException $e ) {

      $this->flashResourceState("warning","user","does_not_exist");
      $this->kickOff();

    }

  }

  public function renderContacts(): void
  {

    $this->template->title = "Kontakty";

    $localities = $this->localityManager->getLocalitiesTree();


    /**
     * Load public contacts by city
     */

    $user_cities_query = $this->userManager->getPublicUsersCities();

    $cities_with_users = [];

    foreach ( $user_cities_query as $city ) {
      $cities_with_users[] = $city->city_id;
    }

    $output = [];

    foreach ( $localities as $country ) {

      if ( $country["hasItems"] ) {

        $country_tmp = [
          "name" => $country["name"],
          "regions" => [],
          "has_contacts" => false
        ];

        foreach ( $country["items"] as $region ) {

          if ( $region["hasItems"] ) {

            $region_tmp = [
              "name" => $region["name"],
              "cities" => [],
              "has_contacts" => false
            ];

            foreach ( $region["items"] as $city ) {

              if ( in_array( $city["id"], $cities_with_users ) ) {

                $users = $this->userManager->getPublicUsersByCity( $city["id"] );

                $region_tmp["has_contacts"] = true;
                $region_tmp["cities"][ $city["id"] ] = [
                  "name" => $city["name"],
                  "people" => []
                ];

                foreach ( $users as $user ) {

                  $user_tmp = [
                    "user_name" => $this->userManager->getUserName( $user->id ),
                    "email" => $user->email,
                    "roles" => $this->userManager->getUserRoles( $user->id ),
                    "institutions" => $this->userInstitutionManager->getUserRelationsToInstitutions( $user->id ),
                    "subjects" => $this->taxonomyManager->getUserTaxonomiesOfType( $user->id, $this->taxonomyManager::TYPE_SUBJECT ),
                    "about" => $user->about
                  ];

                  $region_tmp["cities"][ $city["id"] ]["people"][ $user->id ] = $user_tmp;

                }

              }

            }

            if ( $region_tmp["has_contacts"] ) {

              $country_tmp["regions"][ $region["id"] ] = $region_tmp;

              $country_tmp["has_contacts"] = true;

            }

          }

        }

        if ( $country_tmp["has_contacts"] ) {
          $output[] = $country_tmp;
        }

      }

    }

    // Now, contacts are loaded

    $this->template->contacts = $output;
    

  }

  /**
   * Common functionality for all user setting views
   */
  private function prepareSettingView( ?int $id = null ): int
  {
    
    // Use the special layout
    $this->useLayout("@settings.latte");

    // Load ID if not provided
    if ( $id == null ) {
      $id = $this->getUser()->getId();
    }

    // Control if the user really exists
    if ( ! $this->userManager->userExists( $id ) ) {
      $this->flashResourceState("warning","user","does_not_exist");
      $this->kickOff();
    }

    // ACL check
    $this->canManageOneUser( $id, true );

    // Get the user name
    $this->template->userName = $this->userManager->getUserName( $id );

    // Return the ID
    return $id;
  }

  /**
   * A non administrative view to User:edit
   */
  public function renderSettings( int $id = null ): void
  {

    // Common SettingView preparations
    $id = $this->prepareSettingView( $id );

    // Set the template variables
    $this->template->title = $this->_t("user.forms.basic.title");
      
    // Set default values to the form
    $user = $this->userManager->getUser( $id );

    // Setup the main form
    $form = $this->getComponent("userFormSelf");

    $form->setDefaults( [
        "email" => $user->email,
        "id" => $id,
        "public" => $this->userManager->userHasRole( $id, 4 ),
        "name"=>$user->name,
        "surname" => $user->surname,
        "city_id" => $user->city_id
    ] );

    // If the user is not admin, hide the city_id field
    if ( ! $this->userManager->userHasRole( $id, 30 ) ) {
      $form->getComponent( "city_id" )
        ->setDisabled()
        ->setPrompt(
          $this->_t("city.will_be_assigned_automatically")
        );
    }


    if ( $this->userManager->userHasRole( $id, 2 ) ) {

      $form["email"]->setOption(
        "description",
        Html::el( "div" )
          ->setAttribute( "class", "text-success" )
          ->setText( 
            $this->_t( "actions.user_email_verification.request.ok" )
          )
      );
    } else {
      
      $hint = $this->_t( "actions.user_email_verification.request.renew" );
      
      if ( $this->requestManager->getUserRequests( $id, 2 )->count() == 0 ) {
        if ( ! $this->requestManager->requestIsValid( $this->requestManager->getUserRequests($id,2)->fetch()->id ) ) {
          $hint = $this->_t( "actions.user_email_verification.request.timeout" );
        }
        
      }

      $form["email"]->setOption(
        "description",
        Html::el("a")
          ->href( 
            "settings",
            [
              "do" => "userEmailVerificationRequest",
              "user_id" => $id,
              "email" => $user->email
            ]
          )
          ->setText(
            $hint
          )
      );

    }
    

  }

  /**
   * A non administrative view to User:setPassword
   */
  public function renderSettingsPassword( int $id = null ): void
  {
    // Common SettingView preparations
    $id = $this->prepareSettingView( $id );

    // Set the template variables
    $this->template->title = $this->_t("user.forms.password.title");

    // Setup the main form
    $form = $this->getComponent("userFormPassword");

    $form->setDefaults([
      "id" => $id
    ]);


  }

  /**
   * A non administrative view to User:setDomain
   */
  public function renderSettingsSubjects( int $id = null ): void
  {
    
    // Common SettingView preparations
    $id = $this->prepareSettingView( $id );

    // Set the template variables
    $this->template->title = $this->_t("user.forms.domain.title");

    // Setup the main form
    $form = $this->getComponent("userFormSubjects");

    $form->setDefaults([
        "id" => $id,
        "subjects" => $this->taxonomyManager->getUserTaxonomiesOfTypeIds( $id, $this->taxonomyManager::TYPE_SUBJECT ),
    ]);

  }

  /**
   * A non administrative vew to assign user relations to a user
   */
  public function renderSettingsUserInstitutions( int $id = null ): void
  {

    // Common SettingView preparations
    $id = $this->prepareSettingView( $id );

    // Set the template variables
    $this->template->title = $this->_t("user.forms.institutions.title");

    $this->template->userId = $id;

    // Load the related institutions
    $data = $this->getUsersInstitutions( $id );

    $this->template->dataCount = count( $data );

    if ( count( $data ) == 0 ) {
      $this->flash("info","user.forms.institutions.no_relation_prompt",true);
      if ( $this->userIsCurrentlyLoggedIn( $id ) ) {
        $this->template->addButtonText = $this->_t("user.forms.institutions.add_my_relation");
      } else {
        $this->template->addButtonText = $this->_t("user.forms.institutions.add_relation");
      }
      
    } else {
      $this->template->addButtonText = $this->_t("user.forms.institutions.add_another_relation");
    }

    // Set the data to the table control
    $table = $this->getComponent( "listUserInstitutions" );
    $table->setDataSource( $data );

  }

  public function renderSettingsUserInstitutionAdd( int $id = null ): void
  {

    // Common SettingView preparations
    $id = $this->prepareSettingView( $id );

    // Set the template variables
    $this->template->title = $this->_t("user.forms.institution.title");

    $form = $this->getComponent( "userFormAddInstitutionRelationship" );

    $form->setDefaults([
      "user_id" => $id,
      "valid" => $this->getUser()->isAllowed( "user", "edit" )
    ]);



  }

  public function renderSettingsUserInstitutionEdit( int $id = null ): void
  {

    // Common SettingView preparations

    $relation = $this->userInstitutionManager->getRelation( $id );

    $id = $this->prepareSettingView( $relation->user_id );


    // Set the template variables
    $this->template->title = "Upravit vazbu";// $this->_t("user.forms.institutions.title");

    $form = $this->getComponent( "userFormAddInstitutionRelationship" );

    $form->setDefaults([
      "id" => $relation->id,
      "user_id" => $id,
      "role" => $relation->role,
      "role_other" => $relation->role_other,
      "note" => $relation->note,
      "inst" => $relation->institution,
      "valid" => $relation->verified
    ]);

  }

  

  public function handleDelete( int $id ): void
  {

    // Can not delete self
    if ( $this->userIsCurrentlyLoggedIn( $id ) ) {

      $this->flash( "warning", "common.error.can_not_delete_self", true );

    } else {

      $user = $this->userManager->getUser( $id );

      $user_email = $user->email;

      $this->userManager->deleteUser( $id );

      $this->flashResourceState("warning","user","deleted",$user_email );
    }

    $this->redirect( "this" );

  }


  public function handleSetUserInstitutionVerify( int $id ): void
  {

    $relation = $this->userInstitutionManager->getRelation( $id );

  }

  public function handleSetUserInstitutionDelete( int $id ): void
  {
    $this->userInstitutionManager->deleteRelation( $id );

    $this->flashResourceState( "info", "relation", "deleted", null, "f" );
    $this->redirect( "User:settingsUserInstitutions" );

  }

  

  /**
   * Signal to create a new user email verification
   */
  public function handleUserEmailVerificationRequest( int $user_id, string $email ): void
  {

    // Load the user
    $user = $this->userManager->getUser( $user_id );

    // If the user does not exist, flash the error and redirect to homepage
    if ( empty( $user ) || empty($user_id ) ) {
      $this->flashResourceState( "danger", "user", "does_not_exist" );
    }

    // Validate the email
    if ( ! Validators::isEmail( $email ) ) {
      $this->flash( "danger", "common.error.not_valid_email", true );
    }

    // If everything succeeded, proceed to the request creation

    // Remove the verified role from the user
    $this->userManager->removeUserRole($user_id, 2 );

    
    // Generate the secret password for request
    $code = Random::generate( 4, '0-9' );

    // Create the request
    $request = $this->requestManager->addRequest( true, 2, $user_id, "+2 days", null, [ "code" => $code, "email" => $email ] );

  
    // Send the email
    try {

      $message = $this->mailer->createEmail(
          "userEmailVerification.latte",
          [
            "email" => $email,
            "code" => $code,
            "hash" => $request->hash,
          ]
      );
  
      $message->addTo( $email );
  
      $this->mailer->sendEmail( $message );

      // Flash successful request creation
      $this->flash( 
        "success", 
        $this->_t( "actions.user_email_verification.request.sent", [
          "email" => $email
        ] ), 
        true 
      );

    } catch ( Nette\Mail\FallbackMailerException $e ) {

        \Tracy\Debugger::$maxLength = 500;  

        \Tracy\Debugger::barDump( $message->body, "Email",  );

        $this->flash( "danger", "common.error.mailer_error", true );

    }

    // Finally, redirect to self
    $this->redirect( "settings", ["id"=>$user_id] );

  }



  









  // Components

  


  /**
   * Create the component for the basic user self-editing
   */
  public function createComponentUserFormSelf(): BootstrapForm
  {

    $form = $this->userBaseInfoFormFactory->create();

    // On success, watch for email changes to trigger verification requests
    $form->onSuccess[] = function( ) {

      $this->flashResourceState( "success", "user", "updated" );

    };

    return $form;

  }


  /**
   * Instantiate the user password form
   */
  public function createComponentUserFormPassword(): BootstrapForm
  {

    $form = $this->userPasswordFormFactory->create();
    $form->onSuccess[] = function(){
      $this->flash( "success", "user.fields.password.states.was_changed", true );
    };

    return $form;

  }


  /**
   * Instantiate the user subjects form
   */
  public function createComponentUserFormSubjects(): BootstrapForm
  {
    
    $form = $this->userSubjectsFormFactory->create();
    $form->onSuccess[] = function() {
      $this->flash("success","user.forms.domain.success",true);
    };

    return $form;

  }


  public function createComponentUserFormAddInstitutionRelationship(): BootstrapForm
  {

    $form = $this->userInstitutionFormFactory->create();
    $form->onSuccess[] = function( Form $form ){

      if ( $form->isValid() ) {
        $action = empty( $form->getValues()->id ) ? "created" : "updated";
        $this->flashResourceState( "success", "relation",$action, null, "f");
        $this->redirect( "this" );
        
      }

    };

    return $form;

  }

  /**
   * Instantiate the user roles form
   */
  public function createComponentUserFormRoles(): BootstrapForm
  {

    $form = $this->userRolesFormFactory->create();
    $form->onSuccess[] = function() {
      $this->flash( "success", "user.fields.role.success", true );
    };

    return $form;
    
  }




  /**
   * ADMIN
   */
  public function createComponentListUsers( string $name ): DataGrid
  {
    
    $grid = new DataGrid( $this, $name );

    // Load data from the manager
    $grid->setDataSource( $this->userManager->getAdminUsers( true ) );

    // Define columns based on ACL
    $user = $this->getUser();

    $grid->addColumnText('name', 'Jméno');
    $grid->addColumnText('surname', 'Příjmení');

    if ( $user->isAllowed( "user", "view_private" ) ) {
      $grid->addColumnText('email', 'Email');
    }

    if ( $user->isAllowed( "user", "edit" ) ) {

      $grid->addColumnText( 'member', 'Člen' )
      ->setRenderer( function( $item ) {
        return Html::el('i')
          ->addAttributes([
            "class" => $item["member"] ? "fas fa-check-circle" : "far fa-times-circle"
          ]);
      } );


      $grid->addColumnText( 'confirmed', 'Potvrzený' )
        ->setRenderer( function( $item ) {
          return Html::el('i')
            ->addAttributes([
              "class" => $item["confirmed"] ? "fas fa-check-circle" : "far fa-times-circle"
            ]);
        } );

      $grid->addColumnText( 'content_creator', 'Tvůrce obsahu' )
        ->setRenderer( function( $item ) {
          return Html::el('i')
            ->addAttributes([
              "class" => $item["content_creator"] ? "fas fa-check-circle" : "far fa-times-circle"
            ]);
        } );

      $grid->addColumnText( 'public', 'Veřejný' )
      ->setRenderer( function( $item ) {
        return Html::el('i')
          ->addAttributes([
            "class" => $item["public"] ? "fas fa-check-circle" : "far fa-times-circle"
          ]);
      } );

      $grid->addColumnText( 'admin', 'Admin' )
        ->setRenderer( function( $item ) {
          return Html::el('i')
            ->addAttributes([
              "class" => $item["admin"] ? "fas fa-check-circle" : "far fa-times-circle"
            ]);
        } );
    }

    if ( $user->isAllowed( "user", "edit" ) ) {
      $grid->addAction(
        'edit', 
        ''
      )
        ->setClass( "btn btn-secondary" )
        ->setIcon("edit")
        ->setTitle( $this->_t( "common.op.edit" ) );
    }

    if ( $user->isAllowed( "user", "delete" ) ) {
      $grid->addAction(
        'delete', 
        '',
        'delete!'
      )
        ->setClass( "btn btn-danger" )
        ->setIcon("trash")
        ->setTitle( $this->_t( "common.op.delete" ) )
        ->setConfirmation(
          new StringConfirmation( $this->_t( "user.forms.overview.prompt_delete" ), 'email') // Second parameter is optional
        );
    }

    if ( $user->isAllowed( "user", "edit" ) ) {
      $grid->addAction(
        'setPassword', 
        $this->_t( "fields.password.ops.change" )
      )
        ->setClass( "btn btn-outline-secondary" );
    }


    return $grid;

  }

  public function createComponentListUserInstitutions( ): Control
  {
    $grid = new DataGrid();

    $data = [];

    $grid->setDataSource( $data );

    $grid->addColumnText( 
      "city", 
      $this->_t( "locality.levels.city" ) 
    );

    $grid->addColumnText( 
      "institution", 
      $this->_t( "institution.res.sg.nom" )
    );

    $grid->addColumnText( 
      "role", 
      $this->_t( "user.fields.role.name" ) 
    );

    // Administrators can see the relation note
    if ( $this->canManageAllUsersSimple(false) ) {

      $grid->addColumnText(
        "note",
        "Note"
      );
    }


    // Add user fields for admins
    if ( $this->canManageAllUsersSimple(false) ) {

      $grid->addColumnText( 
        "verified_icon", 
        $this->_t( "Ověřeno") 
      )
        ->setTemplateEscaping(FALSE);
    }

      $grid->addAction(
        "settingsUserInstitutionEdit",
        ""
      )
        ->setClass('btn btn-xs btn-secondary')
        ->setIcon("edit")
        ->setTitle( $this->_t("common.op.edit") );

      

      $grid->addAction(
        "settingsUserInstitutionDelete",
        "",
        "SetUserInstitutionDelete!",
      )
        ->setClass('btn btn-xs btn-danger')
        ->setIcon("trash")
        ->setTitle( $this->_t("common.op.delete") )
        ->setConfirmation(
          new StringConfirmation(
            $this->_t( "user.forms.institutions.delete_confirmation" ), 
            'institution')
        );

    if ( $this->canManageAllUsersSimple(false) ) {

      $grid->addAction(
        "setUserInstitutionVerification",
        "Potvrdit",
        "SetUserInstitutionVerify!",
      )
        ->setClass('btn btn-xs btn-outline-success')
        ->setIcon("check")
        ->setTitle("Potvrdit vazbu");

    }

    return $grid;

  }





  /**
   * Documentation menu component
   */
	protected function createComponentMenuSettings(): MenuComponent
	{
		return $this->menuFactory->create('settings');
  }




  /**
   * Check if a user is currently logged in
   * @param int $user_id
   * @return bool
   */
  private function userIsCurrentlyLoggedIn( int $user_id ): bool
  {
    return $this->getUser()->getId() === $user_id;
  }





  // ACL functions


  /**
   * Can user display one particular user?
   * @param int $id
   * @param bool $redirect Souhld the redirects execute here?
   * @param string[] $targets Redirect targets [ $role => $target ]
   */
  private function canSeeOneUser( int $user_id, bool $redirect = false, array $targets = [] ): bool
  {

    $allowed = true;

    // Check logged in users
    if ( ! $this->user->isLoggedIn() ) {
      $allowed = false;
      $this->flash( "danger", "common.error.logged_in_mandatory", true );
    }

    // Target user must exist
    if ( ! $this->userManager->userExists( $user_id ) ) {
      $allowed = false;
      $this->flashResourceState("danger","user","does_not_exist",null,"m");
    }

    // Is self, can edit users or can see users
    if ( (
      $this->user->isInRole( "admin" )
      || $this->user->isAllowed( "user", "edit" )
      || $this->user->isAllowed( "user", "view" )
      || $this->user->getId() == $user_id
    ) ) {
      $allowed = false;
      $this->flashResourceState("danger","user","does_not_exist",null,"m");
    }

    if ( ! $allowed && $redirect ) { $this->kickOff( $targets ); }

    return $allowed;

  }

  /**
   * Can user manage one particular user?
   * User is either self, or admin => can.
   * 
   * @param int $id
   * @param bool $redirect Should this function perform redirects specified in $targets?
   * @param string[]|null $targets Redirect targets [ $role => $target ]
   */
  private function canManageOneUser( int $user_id, bool $redirect = false, array $targets = [] ): bool
  {
    
    $allowed = true;

    // The managed user must exist!
    if ( ! $this->userManager->userExists( $user_id ) ) {
      $allowed = false;
      $this->flashResourceState("danger","user","does_not_exist");
    }

    // Logged out users can not edit anything!
    if ( ! $this->user->isLoggedIn() ) {
      $allowed = false;
      $this->flash("danger","common.error.logged_in_mandatory",true);
    }

    // If not self, check if is admin
    if ( ! $this->userIsCurrentlyLoggedIn( $user_id ) ) {
      if ( ! $this->user->isAllowed( "user", "edit" ) ) {
        $allowed = false;
        $this->flashACLError("manage","user");
      }
    }

    // Kick off! (eventually)
    if ( ! $allowed && $redirect ) { $this->kickOff( $targets ); }

    // Return
    return $allowed; 

  }

  /**
   * Can user edit all users?
   * @param bool|null $redirect
   * @param string[]|null $targets
   */
  private function canManageAllUsers( ?bool $redirect = false, ?array $targets = [] ): bool
  {

    $allowed = true;

    // Check logged in users
    if ( ! $this->user->isLoggedIn() ) {
      $allowed = false;
      $this->flash("danger","common.error.logged_in_mandatory",true);
    }

    // Check self or admin
    if ( ! $this->user->isAllowed("user", "edit") ) {
      $allowed = false;
        $this->flashACLError("manage","user"); 
    }

    // Kick off! (eventually)
    if ( ! $allowed && $redirect ) { $this->kickOff( $targets ); }

    // Return
    return $allowed; 

  }

  private function canManageOneUserSimple( int $user_id ): bool
  {
    $allowed = true;

    if ( 
      ! $this->userManager->userExists( $user_id ) 
      || ! $this->userIsCurrentlyLoggedIn( $user_id )
      || ! $this->canManageAllUserSimple()
    ) {
      $allowed = false;
    }

    return $allowed;
  }

  private function canManageAllUsersSimple(): bool
  {
    $allowed = true;

    if ( 
      ! $this->user->isLoggedIn() 
      || ! $this->user->isAllowed("user", "edit")
    ) {
      $allowed = false;
    }

    return $allowed;
  }

  /**
   * Kick off unauthorised users
   * @param string[] $targets Override of self::ACL_REDIRECTS
   */
  private function kickOff( $targets = [] ): void
  {

    // Set default redirects, overriding with provided ones
    $redirects = array_merge( self::ACL_REDIRECTS, $targets );

    // Redirect! (by user role)
    foreach ( $redirects as $role => $target ) {
      if ( $this->user->isInRole($role) ) {
        $this->redirect( $target );
      }
    }

  }










  /**
   * A unified handler for a single user institution relationship
   */
  private function processUserInstitutionRelation( Form $form, Nette\Utils\ArrayHash $rel, int $user_id, int $field_key ): void
  {

    // If the relation already exists, process changes first
    
    try {

      // Handle localities first

      // Variables for further assignment
      $city_id = false;
      $institution_id = false;

      // Vytvořit nové město:
      // institution == "add" 
      // institution_new_city == "add"
      // institution_new_city_name != null
      // institution_new_city_region != 0
      if (
        $rel->institution == "add"
        && $rel->institution_new_city == "add"
      ) {

        // Check if the city name is provided
        if ( empty( $rel->institution_new_city_name ) ) {
          $form["institutions"][$field_key]["institution_new_city_name"]->addError("Vyplňte jméno!");
        }

        // Check if a region is selected
        if ( empty( $rel["institution_new_city_region"] ) ) {
          $form["institutions"][$field_key]["institution_new_city_region"]->addError("Zvolte region!");
        }

        // Check if the institution name is provided
        if ( empty( $rel["institution_new_name"] ) ) {
          $form["institutions"][$field_key]["institution_new_name"]->addError("Vyplňte jméno!");
        }

        // Create a new city
        $city_id = $this->localityManager->addLocality($rel["institution_new_city_name"],$rel["institution_new_city_region"],2, 9);

        // Create an new institution
        $institution_id = $this->institutionManager->addInstitution( $rel["institution_new_name"] );

        // Load the institution parents
        $parents = $this->localityManager->getLocalityParentsIds( $city_id );

        // Assign locality to an institution
        $this->institutionManager->setInstitutionLocality( $institution_id, $parents[1], $parents[0], $city_id );

      }


      // Město již existuje, vytvořit pouze instituci
      // institution == "add"
      // institution_new_city != 0
      // institution_new_city != "add"
      else if ( 
        $rel["institution"] == "add" 
        && $rel["institution_new_city"] != 0
        && $rel["institution_new_city"] != "add"
      ) {

        // Create an new institution
        $institution_id = $this->institutionManager->addInstitution( $rel["institution_new_name"] );

        // Load the institution parents
        $parents = $this->localityManager->getLocalityParentsIds( $rel["institution_new_city"]);

        // Assign locality to an institution
        $this->institutionManager->setInstitutionLocality( $institution_id, $parents[1], $parents[0], $rel["institution_new_city"] );

      } 
      
      // Instituce byla pouze zvolena a již existuje
      else if (
        $rel["institution"] != "add" 
        && $rel["institution"] != 0 
      ) {
        $institution_id = $rel["institution"];
      }

      // If the relation exists, edit it
      if ( ! empty( $rel->id ) ) {
      
        $this->userInstitutionManager->setRelationNote( $rel->id, $rel->note, 0 );

        $this->userInstitutionManager->setRelationRole( $rel->id, $rel->role, $rel->role_other, 1 );

        $this->userInstitutionManager->setRelationTargets( $rel["id"], $user_id, $rel["institution"], 3 );
  
      } 

      // If the relation does not exist, create it
      else {

        $this->userInstitutionManager->addRelationBetween($user_id,$institution_id, $rel["role"],$rel["role_other"],$rel["note"],true, 4);

      }

    } catch ( \App\Model\MissingItemException $e ) {

      switch ( $e->getCode() ) {
        
        case 0: // Note field
        case 1: // Roles fields
          $form["institutions"][$field_key]["institution"]->addError( "instituce neexistuje" );
          break;

      }

    } catch (\App\Model\DupliciteItemException $e ) {

      if ( $e->getCode() == 4 ) {
        $form["institutions"][$field_key]["institution"]->addError( "tato vazba již existuje!" );
      } else if ( $e->getCode() == 9 ) {
        $form["institutions"][$field_key]["institution_new_city_name"]->addError("Toto jěsto již existuje!");
      }

    } catch ( \App\Model\MissingInputException $e ) {

      $form["institutions"][$field_key]["institution_new_name"]->addError( "Vyplňte název nového města!" );

    }

  }


  /**
   * Aggregate institutions related to target user
   * @param int $id
   * @return array [ "id", "role", "role_id", "institution", "institution_id", "city", "region" ]
   */
  private function getUsersInstitutions( int $user_id ): array
  {

    $data = [];

    $relations = $this->userInstitutionManager->getUserRelations( $user_id );

    foreach ( $relations as $relation ) {

      $institution = $this->institutionManager->getInstitution( $relation->institution_id );

      $row = [];

      $row[ "id" ] = $relation->id;

      $row[ "role" ] = $this->userInstitutionManager->getUserRoleNameInInstitution( $user_id, $relation->institution_id );

      $row[ "role_id" ] = $relation->role;

      $row[ "institution" ] = $institution->name;

      $row[ "institution_id" ] = $institution->id;

      $row[ "city" ] = $this->localityManager->getLocality( $institution->city_id )->name;

      $row[ "region" ] = $this->localityManager->getLocality( $institution->region_id )->name;

      $row[ "note" ] = $relation->note;

      $row[ "verified" ] = $relation->verified;

      $row[ "verified_icon" ] = sprintf("<span class='fa fa-%s'></span>", $relation->verified ? "check" : "trash" );

      $data[ $relation->id ] = $row;

    }

    return $data;

  }





}
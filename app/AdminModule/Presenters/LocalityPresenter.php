<?php

namespace App\AdminModule\Presenters;

use Nette;

use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;
use Contributte\FormsBootstrap\BootstrapForm;
use Contributte\FormsBootstrap\Enums\RenderMode;

class LocalityPresenter extends BaseAdminPresenter
{

  /** @var \App\Model\LocalityManager @inject */
  public $localityManager;

  public function startup(): void 
  {
    
    parent::startup();

    if ( ! $this->user->isAllowed("locality") ){

      $this->flashACLError( "manage", "locality" );

      $this->redirect(":Front:Static:default");

    }

    $this->template->layoutColumn = "col-12 col-md-8 col-lg-6";

  }





  // Views



  /**
   * @todo předávání parametru ID pro tvorbu komponenty by se mělo přesunout do komponenty
   */
  public function renderEdit( int $id ): void 
  {
    
    try {

      // Load the locality
      $locality = $this->localityManager->getLocality( $id );

      // Title prefix: Action: depth
      $this->template->titlePrefix = $this->_t("common.op.edit") . " " . $this->getLocalityLevelName($locality->depth). ":" ;

      // Title: name of the locality
      $this->template->title = $locality->name;

      // Locate the form
      $form = $this["editLocalityForm"];

      // Initialise defaults
      $defaults = [
        "id" => $id,
        "name" => $locality->name,
        "depth" => $locality->depth
      ];

      // If the item is not country, add options to parent_id
      if ( $locality->depth != 0 ) {

        // Assign selectbox options bsed on item's depth

        $options = $this->localityManager->getFormOptionsLocalities( $locality->depth - 1, null );

        $form->getComponent( "parent_id" )
          ->setItems( $options )
          ->addOptionAttributes( $options );

        // Assign parents default value
        $defaults["parent_id"] = $locality->parent_id;

      } else {

        // If the item is a country, remove the parent_id field completely
        $form->removeComponent($form->getComponent( "parent_id" ));

      }

      // Set the form's defaults
      $form->setDefaults( $defaults );

    } catch (\App\Model\MissingItemException $e) {

      $this->flashResourceState("danger","locality","does_not_exist");

      $this->redirect( "Locality:default" );

    }

  }

  public function renderDelete( int $id ): void
  {

    // Redirect when locality does not exist
    if (!$this->localityManager->localityExists( $id ) ) {
      $this->flashResourceState("danger","locality","does_not_exist");
      $this->redirect( "Locality:default" );
    } 

    // If the locality exists, set variables
    // depending on whether it can be deleted
    try {

      // Set title
      $this->template->title = $this->localityManager->getLocalityName( $id );

      // Set title prefix
      $this->template->titlePrefix = $this->_t( "common.op.delete" ) . " " . $this->getLocalityLevelName( $id );

      // Initialise template data
      $this->template->children = [];

      // Load locality
      $this->template->locality = $this->localityManager->getLocality( $id );

      // If the locality can be deleted
      if ( !$this->localityManager->localityHasChildren( $id ) ) {

        $this->template->canDelete = true;

      } 
      // If the locality can not be deleted
      else {

        $this->template->canDelete = false;

        // Load children
        $this->template->children = $this->localityManager->getLocalityDirectChildren( $id );

        // Flash the warning
        $this->flashResourceState( "danger", "locality", "can_not_be_deleted", $this->localityManager->getLocalityName( $id ) );

      }

    } catch ( \App\Model\MissingItemException $e ) {

      $this->flashResourceState("danger","locality","does_not_exist");

      $this->redirect( "Locality:default" );

    } catch ( \App\Model\StructureException $e ) {

      $this->flashResourceState("danger","locality","not_valid",null,"f","sg"," - ".$this->_t("common.states.is_not_of_appropriate_depth"));

      $this->redirect( "Locality:default" );

    }

  }

  /**
   * Action deleting a locality
   */
  public function actionDeleteLocality( int $id ): void
  {

    $this->setView("default");
   
    try {

      $this->database->beginTransaction();

      $name = $this->localityManager->getLocalityName( $id );

      $this->localityManager->deleteLocality( $id );

      $this->flashResourceState( "success", "locality", "deleted", $name, "f" );

      $this->database->commit();

    } catch ( \App\Model\StructureException $e ) {

      $this->flashResourceState("danger","locality","can_not_be_deleted", $this->localityManager->getLocalityName( $id ) );

      $this->database->rollback();

    }
    
   
  }





  // Components





  /**
   * Add city form & handler
   */
  public function createComponentAddCityForm(): Form
  {
    $form = new BootstrapForm;
    // $form->renderMode = RenderMode::SIDE_BY_SIDE_MODE;

    $form->addText( 
      'name', 
      $this->_t("fields.name_city.name") 
    )
      ->setRequired();
    
    $form->addSelect( 
      'region', 
      $this->_t("locality.levels.region"), $this->localityManager->getFormOptionsLocalities(1)
    );

    $form->addSubmit( 
      'submit', 
      $this->_t("common.op.save") 
    );

    $form->addProtection();

    $form->onSuccess[] = [ $this, "addCityFormSuccess" ];

    return $form;

  }

  public function addCityFormSuccess( Form $form, \stdClass $values ): void 
  {

    try{

      $this->localityManager->addLocality( $values->name, $values->region, 2 );

      // Format massage
      $this->flashResourceState("success", "city", "created", $values->name,"n");

      $this->redirect( "Locality:" );

    } catch( \App\Model\DupliciteItemException $e ) { 
      $form["name"]->addError( 
        $this->formatStateMessage("city","already_exists",$values->name,"n")
      ); 
    } catch ( \App\Model\MissingRelatedItemException $e ) {
      $form["region"]->addError( 
        $this->formatStateMessage("locality","already_exists",$values->name,"f")
      );
    } catch ( \App\Model\InputValidationException $e ) {
      $form["name"]->addError(
        $this->_t("common.states.required_field")
      );
    }

  }

  /**
   * Add region form & handler
   */
  public function createComponentAddRegionForm(): Form
  {
    $form = new BootstrapForm;
    // $form->renderMode = RenderMode::SIDE_BY_SIDE_MODE;

    $form->addText(
      'name', 
      $this->_t("fields.name_region.name") 
    )
    ->setRequired();
    
    $form->addSelect( 
      'country', 
      $this->_t("locality.levels.country"),
      $this->localityManager->getFormOptionsLocalities(0) 
    );

    $form->addSubmit( 
      'submit', 
      $this->_t('common.op.save' )
    );

    $form->addProtection();

    $form->onSuccess[] = [ $this, "addRegionFormSuccess" ];

    return $form;

  }

  public function addRegionFormSuccess( Form $form, \stdClass $values ): void 
  {
    try{

      $this->localityManager->addLocality( $values->name, $values->country, 1 );

      $this->flashResourceState("success","region","created",$values->name,"m");

      $this->redirect( ":Admin:Locality:default" );

    } catch( \App\Model\DupliciteItemException $e ) {
      $form["name"]->addError( 
        $this->formatStateMessage("region","already_exists",$values->name,"m")
      );
    } catch ( \App\Model\MissingRelatedItemException $e ) {
      $form["country"]->addError( 
        $this->formatStateMessage("region","not_valid")
      );
    } catch ( \App\Model\InputValidationException $e ) {
      $form["name"]->addError(
        $this->_t("common.states.required_field")
      );
    }

  }


  /**
   * Add country form & handler
   */
  public function createComponentAddCountryForm(): Form
  {
    $form = new BootstrapForm;
    // $form->renderMode = RenderMode::SIDE_BY_SIDE_MODE;

    $form->addText( 'name', $this->_t("fields.name_country.name") )
      ->setRequired();

    $form->addSubmit( 'submit', $this->_t("common.op.save") );

    $form->addProtection();

    $form->onSuccess[] = [ $this, "addCountryFormSuccess" ];

    return $form;

  }

  public function addCountryFormSuccess( Form $form, \stdClass $values ): void 
  {
    try{

      $this->localityManager->addLocality( $values->name, 0, 0 );

      $this->flashResourceState("success","country","created",$values->name,"f");
      $this->redirect( "Locality:" );

    } catch( \App\Model\DupliciteItemException $e ) {
    
      $form["name"]->addError( 
        $this->formatStateMessage("country","already_exists",$values->name,"f")
      );

    } catch ( \App\Model\InputValidationException $e ) {
      $form["name"]->addError(
        $this->_t("common.states.required_field")
      );
    }

  }



  public function createComponentEditLocalityForm(): Form
  {
    $form = new BootstrapForm;

    $form->addHidden('id');

    $form->addText('name',$this->_t("fields.name.name"));

    $form->addSelect('parent_id',"Nadřazená položka",[])
      ->checkDefaultValue(false);

    $form->addSubmit("send",$this->_t("common.op.save"));

    $form->addProtection();

    $form->onSuccess[] = [$this,"editLocalityFormSuccess"];

    return $form;

  }

  public function editLocalityFormSuccess( Form $form, \stdClass $values ): void
  {

    try {

      $this->database->beginTransaction();

      // Load the previous state
      $locality = $this->localityManager->getLocality($values->id);

      // Proces the name
      $this->localityManager->setLocalityName($locality->id, $values->name);

      // If there is a parent, process it
      if ( $form->getComponent("parent_id")->getRawValue() != null ) {

        $this->localityManager->setLocalityParent($locality->id, $form->getComponent("parent_id")->getRawValue() );

      }

      $this->database->commit();

      $this->flashResourceState("success","locality","updated",$values->name,"f");

      $this->redirect( "Locality:default" );


    } catch( \App\Model\MissingItemException $e ) {

        $form->addError( 
          $this->formatStateMessage("locality","does_not_exist",null,"f")
        );

        $this->database->rollback();

    } catch(\App\Model\InputValidationException $e ) {
      
      // The name is already taken
      if ( $e->getCode() == 0 ) {

        $form["name"]->addError( 
          $this->formatStateMessage("locality","already_exists",$values->name,"f")
        );

      }

      // If the parent is not of valid depth
      if ( $e->getCode() == 1 ) {

        $form["parent_id"]->addError( 
          $this->formatStateMessage("locality","not_valid",null,"f","sg"," - ".$this->_t("common.states.is_not_of_appropriate_depth"))
        );

      }
      
      $this->database->rollback();

    }

  }




  /**
   * Create admin overview
   */
  public function createComponentLocalitiesTable( $name )
  {

    $grid = new DataGrid( $this, $name );

    $grid->setDataSource( $this->localityManager->getAdminLocalities() );

    $grid->addColumnText(
      'name', 
      $this->_t('fields.name.name')
    );
    $grid->addAction( 
      'edit', 
      $this->_t("common.op.edit") 
    );
    $grid->addAction( 
      'delete', 
      $this->_t("common.op.delete") 
    );
    $grid->setItemsPerPageList([20, 50, 100, 200]);

    return $grid;

  }






  // HELPERS

  /**
   * Return a translated name ot the locality depth
   * @param int $depth
   * @return string
   */
  private function getLocalityLevelName( int $depth ): string
  {
    switch ( $depth ) {
      case 0: return $this->_t("locality.levels.country");
      case 1: return $this->_t("locality.levels.region");
      case 2: return $this->_t("locality.levels.city");
      default: return $this->_t("locality.res.sg.nom");
    }
  }



}
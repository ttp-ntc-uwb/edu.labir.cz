<?php

namespace App\AdminModule\Presenters;

use App\BaseModule\Presenters\BasePresenter;

use Contributte\MenuControl\UI\IMenuComponentFactory;
use Contributte\MenuControl\UI\MenuComponent;

class BaseAdminPresenter extends BasePresenter
{

  public function startup(): void 
  {
    parent::startup();

    $this->template->layoutContainer = "container";
    $this->template->layoutRow = "row justify-content-center";
    $this->template->layoutColumn = "col-12";

    $this->template->titleSuffix = true;

    $this->template->adminAssets = true;


    
  }

  public function beforeRender(): void
  {
    
    parent::beforeRender();

    $this->useLayout("@simple.latte");

  }

}
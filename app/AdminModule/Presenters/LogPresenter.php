<?php

namespace App\AdminModule\Presenters;

use Nette;

use Ublaboo\DataGrid\DataGrid;
use App\AdminModule\Presenters\BaseAdminPresenter;


final class LogPresenter extends BaseAdminPresenter
{

  /** @var \App\Model\LogManager $logManager @inject */
  public $logManager;

  public function renderDefault(): void
  {



  }

  public function createComponentLogTable(): DataGrid
  {
    $grid = new DataGrid();

    $grid->setDataSource( $this->logManager->getAdminLogs() );

    $grid->addColumnDateTime("time","Čas");

    $grid->addColumnText("user_email","Ímejl");
    $grid->addColumnText( "message", "Zpráva" );

    return $grid;

  }

}
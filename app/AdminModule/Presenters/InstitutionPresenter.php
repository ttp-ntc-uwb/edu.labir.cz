<?php

namespace App\AdminModule\Presenters;


use Nette;

use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;
use Contributte\FormsBootstrap\BootstrapForm;
use Contributte\FormsBootstrap\Enums\RenderMode;
use App\AdminModule\Presenters\BaseAdminPresenter;

class InstitutionPresenter extends BaseAdminPresenter {

  /** @var \App\Model\InstitutionManager @inject */
  public $institutionManager;

  /** @var \App\Model\LocalityManager @inject */
  public $localityManager;


  public function startup(): void 
  {

    parent::startup();

    // Kick off all non logged in users
    if ( 
      ! $this->getUser()->isLoggedIn() 
      ||
      ! $this->getUser()->isAllowed("institution")
    ) {

      $this->flashACLError("manage","institution");

      $this->redirect( ":Front:Static:default" );
    }

    // Set default values for the template
    $this->template->layoutColumn = "col-12 col-lg-8";

  }
  
  
  


  
  // Views




  public function renderAdd(): void
  {
    $this->template->title = $this->formatOperationName("add","new","institution",null,"sg","f");
  }

  public function renderDefault(): void
  {
    $this->template->title = $this->formatOverviewName("overview","institution");
  }



  public function renderEdit( int $id ): void 
  {

    try {

      // Load existing data
      $defaults = $this->institutionManager->getFormDefaultsInstitution( $id );

      $this->template->title = $this->formatOperationName("edit",null,"institution",null,"sg","f");
      $this->template->titlePrefix = $defaults["name"];
      $this->template->titleSeo = $this->_t("institution.titles.edit");

      // Pass them to the form
      $form = $this["addInstitution"];
      $form->setDefaults( $defaults );

      // Pass additional variables to the form
      $this->template->name = $defaults["name"];

    } catch ( \App\Model\MissingItemException $e ) {
      $this->flashResourceState("danger","institution","does_not_exist");
      $this->redirect( "Institution:default" );

    }

  }




  // Forms

  public function createComponentAddInstitution(): Form
  {
    $form = new BootstrapForm();
    // $form->renderMode = RenderMode::SIDE_BY_SIDE_MODE;

    // Fields

    $form->addHidden( $this->institutionManager::INST_ID );

    $form->addText( 
      $this->institutionManager::INST_NAME, 
      $this->_t( "fields.name.name" ) 
    )
      ->setRequired();

    $form->addCheckboxList( 
      "types", 
      $this->_t( "fields.type.name" ),
      array_map(function($abbr){
          return $this->_t( "institution.type.".$abbr );
        },$this->institutionManager->getFormArrayAvailableTypes() )
    );

    $form->addSelect( 
      $this->institutionManager::INST_CITY, 
      $this->_t( "locality.levels.city" ),
      $this->localityManager->getFormOptionsLocalities(2, null, "Zvolte město", "Vytvořit nové město") )
      ->setRequired()
      ->setOption('id', 'city_controller')
      ->addCondition($form::EQUAL, "add")
        ->toggle("city_controller_name")
        ->toggle("city_controller_region");

    
    // Add conditionally rendered new city form
    $new_city_row = $form->addRow();

      $new_city_row->addCell(6)
        ->addText( "city_new" )
        ->setOption('id', 'city_controller_name')
        ->setPlaceholder( 
          $this->_t( "fields.new.city.name" )
        )
        ->addConditionOn( $form["city_id"], Form::EQUAL, "add" )
          ->setRequired(
            $this->_t( "common.msg.required_field" )
          );

      $new_city_row->addCell(6)
        ->addSelect( 
          "region_new", 
          null, 
          $this->localityManager->getFormOptionsLocalities(1,0,"Vyberte region") 
        )
        ->setOption('id', 'city_controller_region')
        ->addConditionOn( $form["city_id"], Form::EQUAL, "add" )
          ->setRequired($this->_t( "common.msg.required_field" ))
          ->addRule(Form::NOT_EQUAL,"Zvolte kraj!", 0);


    // Address in a single row

    $address_row = $form->addRow();

      $address_row->addCell( 6 )
        ->addText( 
          $this->institutionManager::INST_STREET, 
          $this->_t( "fields.street.name" ) 
        )
        ->setRequired( $this->_t( "common.msg.required_field" ) );

      $address_row->addCell( 2 )
        ->addInteger( 
          $this->institutionManager::INST_NUMBER, 
          $this->_t( "fields.orientnum.name" )
        )
        ->setRequired( $this->_t( "common.msg.required_field" ) );

      $address_row->addCell( 4 )
        ->addInteger( 
          $this->institutionManager::INST_PSC, $this->_t( "fields.psc.name" ) 
        );

    $form->addText( 
      $this->institutionManager::INST_ICO, 
      $this->_t( "fields.ico.name" ) 
    );

    $form->addTextarea( 
      $this->institutionManager::INST_NOTE, $this->_t( "institution.fields.note" ) 
    );

    // Submit

    $form->addSubmit( 
      "send", 
      $this->_t( "common.op.save" )
    );

    $form->onSuccess[] = [ $this, "formSuccessAddInstitution" ];

    return $form;

  }


  public function formSuccessAddInstitution( Form $form, \stdClass $values ): void
  {


    $this->database->beginTransaction();

    // 1. create the city first

    $city = false;

    // If a new city shall be created, do it first
    if ( $values->city_id == "add" ) {

      if ( $values->city_new != "" && $values->region_new != "" && $values->region_new != 0 ) {
        try {
          $city = $this->localityManager->addLocality( $values->city_new, $values->region_new, 2 );
        } catch ( \App\Model\DupliciteItemException $e){
          $form["city_new"]->addError(
            $this->formatMessage(
              "locality.levels.city",
              "common.states.already_exists"
            )
          );
        }
      }

    } 
    // Mark missing input and stop
    else if ( $values->city_id == 0 ) {
      $form["city_id"]->addError( $this->_targ("common.states.select_or_create","locality.levels.city") );
    } 
    // Assign the city from the form
    else {
      $city = $values->city_id;
    }


    // 2. SAve the institution itself
    // Proceed only when city was created or loaded successfully
    if ( $city != false ) {

      try {

        // First, retrieve the ID
        $id = false;
        
        // From the form (aka edit)
        if ( $values->id != "" ) {
          $id = $values->id;
        } 
        // From the new entity (aka add)
        else {
          $id = $this->institutionManager->addInstitution( $values->name );
        }


        // Then retrieve the locality parents...
        $parents = $this->localityManager->getLocalityParentsIds( $city );

        // ... and save the locality.
        $this->institutionManager->setInstitutionLocality( $id, $parents[1], $parents[0], $city );


        // Save the types
        $this->institutionManager->setInstitutionTypes($id, $values->types);

        // Save address
        if ( $values->orient != "" && $values->street != "" ) {
          $this->institutionManager->setInstitutionAddress( $id, $values->street, $values->orient );
        }

        // Save the PSČ
        if ( $values->psc != "" ) {
          $this->institutionManager->setInstitutionPsc( $id, $values->psc );
        }

        // Save the note
        if ( $values->note != "" ) {
          $this->institutionManager->setInstitutionNote( $id, $values->note );
        }

        // Commit when everything went well
        $this->database->commit();

        $this->flashResourceState("success", "institution","saved", $values->name, "f" );

        $this->redirect( "Institution:default" );

      } catch ( \App\Model\MissingItemException $e ) {

        $data = $e->getData();
        switch( $e["table"] ) {
          case $this->institutionManager::TABLE_BASE:
            $form->addError( $this->formatMessage(
              "institution.resource.sg",
              "common.states.does_not_exist"
            ) );
            break;
          case $this->localityManager::TABLE_BASE:
            $form["city_id"]->addError( $this->formatMessage(
              "locality.levels.city",
              "common.states.does_not_exist"
            ) );
            break;
        }

        $this->database->rollback();
      }

    } else {
      $this->database->rollback();
    }

  
    
  }



  /**
   * Create admin overview
   */
  public function createComponentInstitutionsTable( $name )
  {

    $grid = new DataGrid( $this, $name );

    $grid->setDataSource( $this->institutionManager->getAdminInstitutions() );

    $grid->addColumnText('name', 'Jméno')
      ->setSortable();

    $grid->addFilterText('name', 'Jméno');

    $grid->addColumnText('country', 'Země')
      ->setSortable();

    $grid->addColumnText('region', 'Region')
      ->setSortable();

    $grid->addColumnText('city', 'Město')
      ->setSortable();

    $grid->addAction( 'edit', "Upravit" );
    $grid->addAction( 'delete', "Smazat" );

    $grid->setItemsPerPageList([20, 50, 100, 200]);

    /*
    $grid->addExportCsv('Csv export (filtered)', 'examples.csv')
	    ->setTitle('Csv export (filtered)');
    */

    return $grid;

  }



  



}
<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Presenters\BaseAdminPresenter;
use App\AdminModule\Forms\TaxonomyFormFactory;
use App\Model\TaxonomyManager;
use Ublaboo\DataGrid\DataGrid;
use Contributte\FormsBootstrap\BootstrapForm;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;

class TaxonomyPresenter extends BaseAdminPresenter {

  /** @var TaxonomyFormFactory @inject */
  public $taxonomyFormFactory;

  /** @var TaxonomyManager @inject */
  public $taxonomyManager;

  public function startup(): void
  {
    parent::startup();

    // Any operation can be performed by administrative accounts only
    $this->checkACL();

    $this->template->titleSuffix = "Správa taxonomií";

    $this->useLayout("@simple.latte");

  }

  public function renderAdd(): void
  {
    $this->template->title = $this->formatOperationName("add","new","taxonomy",null, "sg","f");
  }

  public function renderEdit( int $id ): void
  {

    $this->template->title = $this->formatOperationName("edit",null,"taxonomy",null, "sg","f");

    try {
      
      // Load the taxonomy
      $taxonomy = $this->taxonomyManager->getTaxonomy( $id );
      
      // Set form defaults
      $this->getComponent( "taxonomyForm" )
        ->setDefaults([
          "id" => $taxonomy->id,
          "name" => $taxonomy->name,
          "slug" => $taxonomy->slug,
          "description" => $taxonomy->description,
          "type" => $taxonomy->type
        ]);

    } catch ( \App\Model\MissingItemException $e ){

      $this->flashResourceState( "danger", "taxonomy","does_not_exist", $id );
      $this->redirect( ":" );

    }
    
  }

  public function renderDefault(): void
  {

    $this->template->title = $this->formatOverviewName("overview","taxonomy");

    $grid = $this->getComponent( "taxonomyOverview" );
    $grid->setDataSource(
      $this->taxonomyManager->getTaxonomiesOfType()
    );
  }

  public function renderOverviewTags(): void
  {

    $this->template->title = $this->formatOverviewName("overview","tag");

    $grid = $this->getComponent( "taxonomyOverview" );
    $grid->setDataSource(
      $this->taxonomyManager->getTaxonomiesOfType(
        $this->taxonomyManager::TYPE_TAG
      )
    );
  }

  public function renderOverviewLabels(): void
  {

    $this->template->title = $this->formatOverviewName("overview","label");

    $grid = $this->getComponent( "taxonomyOverview" );
    $grid->setDataSource(
      $this->taxonomyManager->getTaxonomiesOfType(
        $this->taxonomyManager::TYPE_LABEL
      )
    );
  }

  public function renderOverviewSubjects(): void
  {

    $this->template->title = $this->formatOverviewName("overview","subject");

    $grid = $this->getComponent( "taxonomyOverview" );
    $grid->setDataSource(
      $this->taxonomyManager->getTaxonomiesOfType(
        $this->taxonomyManager::TYPE_SUBJECT
      )
    );
  }

  public function renderOverviewFolders(): void
  {

    $this->template->title = $this->formatOverviewName("overview","folder");

    $grid = $this->getComponent( "taxonomyOverview" );
    $grid->setDataSource(
      $this->taxonomyManager->getTaxonomiesOfType(
        $this->taxonomyManager::TYPE_FOLDER
      )
    );
  }

  public function createComponentTaxonomyForm(): BootstrapForm
  {
    return $this->taxonomyFormFactory->create();
  }

  public function createComponentTaxonomyOverview( string $name ): DataGrid
  {
    $grid = new DataGrid( $this, $name );

    $grid->setDataSource([]);

    $grid->addColumnText("name","Name");

    $grid->addColumnText("slug","Slug");

    $grid->addColumnText( "type", "Typ" );

    $grid->addAction(
      ":Admin:Experiment:category",
      "Detail",
      null,
      ["slug"]
    );

    $grid->addAction(
      "edit",
      "Upravit"
    );

    $grid->addAction(
      "delete",
      "Smazat",
      "delete!"
    )
      ->setConfirmation(
        new StringConfirmation( "opravdu chcete smazat tuto kategorii?" ) // Second parameter is optional
      );

    return $grid;
  }

  private function checkACL(): void
  {
    if ( ! $this->getUser()->isAllowed("taxonomy")) {
      $this->kickOff();
    }
  }

  private function kickOff(): void
  {
    if ( $this->user->isInRole("admin") ) {
      $this->flashACLError("manage","taxonomy");
      $this->redirect(":Admin:Taxonomy:default");
    } else if ( $this->user->isInRole("content_creator") ) {
      $this->flashACLError("manage","taxonomy");
      $this->redirect(":Admin:Experiment:creatorOverview");
    } else {
      $this->flashACLError("manage","taxonomy");
      $this->redirect(":Front:Static:default");
    }
  }

  private function kickFurther( int $taxonomy_id = null ): void
  {
    if ( $taxonomy_id != null ) {
      $taxonomy = $this->taxonomyManager->getTaxonomy($taxonomy_id);
      $this->redirect( ":Admin:Taxonomy:edit", ["id"=>$taxonomy_id] );
    } else {
      $taxonomy = $this->taxonomyManager->getTaxonomy($taxonomy_id);
      if ( $taxonomy->type == $this->taxonomyManager::TYPE_SUBJECT ) {
        $this->redirect( ":Admin:Taxonomy:overviewSubjects" );
      } else if ($taxonomy->type == $this->taxonomyManager::TYPE_FOLDER ) {
        $this->redirect( ":Admin:Taxonomy:overviewFolders" );
      } else if ($taxonomy->type == $this->taxonomyManager::TYPE_TAG ) {
        $this->redirect( ":Admin:Taxonomy:overviewTag" );
      } else if ($taxonomy->type == $this->taxonomyManager::TYPE_LABEL ) {
        $this->redirect( ":Admin:Taxonomy:overviewLabels" );
      }
    }
  }

  public function handleDelete( int $id ): void
  {
    $this->taxonomyManager->deleteTaxonomy($id);
  }

}
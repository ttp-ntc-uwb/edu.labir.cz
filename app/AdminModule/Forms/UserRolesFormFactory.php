<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use Nette;
use App\Model\UserManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;

class UserRolesFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  public function __construct(
    UserManager $userManager,
    Connection $connection,
    Translator $translator
  )
  {
    $this->userManager = $userManager;
    $this->connection = $connection;
    $this->translator = $translator;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden("id");

    $form->addCheckboxList(
      "roles",
      $this->_t("user.fields.role.name"),
      $this->userManager->getFormArrayAvailableRoles(true, null, false)
    );

    $form->addProtection();

    $form->onSuccess[] = [$this, "process"];

    $form->addSubmit( "send", $this->_t("user.fields.role.send") );

    return $form;

  }

  public function process( Form $form, \stdClass $values ): void
  {
    
    try {

      $this->userManager->setUserRoles( $values->id, $values->roles );

    } catch ( \App\Model\MissingItemException $e ) {

      $form->addError( $this->formatStateMessage("user","does_not_exist") );

    } catch ( \App\Model\DictionaryValidationException $e ) {

      $form["roles"]->addError( "Select a valid value" );

    }
  }



}
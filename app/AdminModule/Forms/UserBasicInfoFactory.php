<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use Nette;
use App\Model\UserManager;
use App\Model\RequestManager;
use App\Model\LocalityManager;
use App\Model\LogManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;
use Nette\Utils\Html;

class UserBasicInfoFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var RequestManager */
  public $requestManager;

  /** @var LocalityManager */
  public $localityManager;

  /** @var LogManager */
  public $logManager;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  public function __construct(
    UserManager $userManager,
    RequestManager $requestManager,
    LocalityManager $localityManager,
    LogManager $logManager,
    Connection $connection,
    Translator $translator
  )
  {
    $this->userManager = $userManager;
    $this->requestManager = $requestManager;
    $this->logManager = $logManager;
    $this->localityManager = $localityManager;
    $this->connection = $connection;
    $this->translator = $translator;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden("id",null);

    $form->addEmail(
      "email",
      $this->_t("fields.email.name")
    );

    $form->addText(
      "name",
      $this->_t("fields.name.name")
    );

    $form->addText(
      "surname",
      $this->_t("fields.surname.name")
    );

    $form->addTextarea(
      "about",
      $this->_t("fields.about.name"),
    );

    $form->addSelect(
      "city_id",
      $this->_t( "city.res.sg.nom" ),
      $this->localityManager->getFormOptionsLocalities(2)
    )
      ->setPrompt(
        $this->_t("city.select_city")
      );

    $form->addCheckbox(
      "public",
      $this->_t("fields.public.name")
    )
      ->setOption(
        "description",
        $this->_t( "fields.public.hint.user" )
      );

    $form->addProtection();

    $form->addSubmit("send",$this->_t("common.op.save"));

    // $form->onRender[] = [ $this, "prerender" ];

    $form->onSuccess[] = [ $this, "process" ];

    return $form;

  }

  public function prerender( Form $form ): Form
  {

    $presenter = $form->getPresenter();

    if ( ! $form->isValid() ) {
      return $form;
    }

    $values = $form->getValues();

    $isVerified = $presenter->userManager->userHasRole( $values->id, 2 );

    // Notify if the user email is verified
    if ( $isVerified ) {
      $form["email"]->setOption( 
        "description", 
        Html::el( "div" )
          ->setAttribute( "class", "text-success" )
          ->setText(
            $this->_t( "actions.user_email_verification.request.ok" )
          )
      );
    }

    // If the user is not verified, add link to create the request
    else {

      // Get the user's previous requests
      $requests = $this->requestManager->getUserRequests( $values->id, 2 );

      // If there are no requests, add link to create a new request
      $hint = $this->_t( "actions.user_email_verification.request.new" );
      
      // If the requests exist, select the link ba 
      if ( $requests->count() > 0  ) {
        if ( $this->requestManager->requestIsValid( $requests->fetch()->id ) ) {
          $hint = $this->_t( "actions.user_email_verification.request.renew" );
        } else {
          $hint = $this->_t( "actions.user_email_verification.request.timeout" );
        }
      }

      // Add the link to create a new verification request
      $form["email"]->setOption(
        "description",
        Html::el("a")
          ->href( 
            "settings",
            [
              "do" => "userEmailVerificationRequest",
              "user_id" => $values->id,
              "email" => $values->email
            ]
          )
          ->setText(
            $hint
          )
      );

    }

    return $form;

  }

  public function validate( Form $form, \stdClass $values  ): void
  {

    try {

      // Load the previous state of the
      $previous_email = $this->userManager->getUserEmail( $values->id, 0);

      // Validate if the email is unique
      if ( $previous_email != $values->email ) {

        if ( $this->userManager->userEmailExists( $values->email ) ) {

          throw new \App\Model\DupliciteItemException("Uživatel již existuje");

        }

      } 

    } catch ( \App\Model\MissingItemException $e ) {
      $form->addError( $this->resourceState( "user", "does_not_exist" ) );
    } catch ( \App\Model\DupliciteItemException $e ) {
      $form["email"]->addError(
        $this->resourceState( "user","already_exists", $values->email )
      );
    }
  }

  /**
   * Process the form and create the request if necessary
   */
  public function process( Form $form, \stdClass $values ): void
  {
    try {
     
      // Update user name
      $this->userManager->setUserName( $values->id, $values->name, $values->surname );


      // Handle the public role
      if ( $values->public == true ) {

        $this->userManager->addUserRole( $values->id, 4 );

      } else {

        $this->userManager->removeUserRole( $values->id, 4 );

      }

      // Save the locality if provided
      if ( $values->city_id != null && $values->city_id != 0) {
        $this->userManager->setUserCity( $values->id, $values->city_id );
      }

      // Save the about if provided
      if ( $values->about != "" || $values->about != null ) {
        $this->userManager->setAbout( $values->id, $values->about );
      }

      // Save the user email if necessary
      if ( $this->userManager->getUserEmail( $values->id, 0) != $values->email ) {

        // Update the email
        $this->userManager->setUserEmail( $values->id, $values->email, 0);

        // Create the new request
        if ( ! $form->getPresenter()->getUser()->isAllowed("user","edit") ) {
          $form->getPresenter()->handleUserEmailVerificationRequest( $values->id, $values->email );
        }
        

      } 

      // Set the user role to the identity

      if ( $values->public == true ) {
        $this->addCurrentUserRole($form, "public");
      } else {
        $this->removeCurrentUserRole($form, "public");
      }

      


    } catch ( \App\Model\DupliciteItemException $e ) {
      $form["email"]->addError(
        $this->resourceState( "user","already_exists", $values->email )
      );
    }

  }

}
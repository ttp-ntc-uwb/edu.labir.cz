<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use App\Model\DupliciteItemException;
use App\Model\InputValidationException;
use Nette;
use App\Model\UserManager;
use App\Model\LocalityManager;
use App\Model\InstitutionManager;
use App\Model\MissingInputException;
use App\Model\MissingItemException;
use App\Model\NotYetException;
use App\Model\UserInstitutionManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;

class UserInstitutionFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var LocalityManager */
  public $localityManager;

  /** @var InstitutionManager */
  public $isntitutionManager;

  /** @var UserInstitutionManager */
  public $userInstitutionManager;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  public function __construct(
    UserManager $userManager,
    LocalityManager $localityManager,
    InstitutionManager $institutionManager,
    UserInstitutionManager $userInstitutionManager,
    Connection $connection,
    Translator $translator
  )
  {
    $this->userManager = $userManager;
    $this->localityManager = $localityManager;
    $this->isntitutionManager = $institutionManager;
    $this->userInstitutionManager = $userInstitutionManager;
    $this->connection = $connection;
    $this->translator = $translator;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden( "id" );
    $form->addHidden( "user_id" );

    $form->addSelect(
      "inst",
      $this->_t( "institution.res.sg.nom" ),
      $this->isntitutionManager->getFormOptionsInstitutionsByCity("Zvolte jednu","Přidejte novou")
    )
    ->addCondition($form::EQUAL, "add")
		  ->toggle('new_inst_name')
      ->toggle('new_inst_city_id');

    $row_one = $form->addRow();

    $row_one->addCell( 3 )
      ->addText(
        "new_inst_name",
        "Jméno nové instituce"
      )
        ->setOption("id","new_inst_name");
    
    $row_one->addCell( 3 )
      ->addSelect(
        "new_inst_city_id",
        "Město, v němž je nová institce",
        $this->localityManager->getFormOptionsLocalities(2,null,"Zvolte jednu", "vytvořte nové město")
      )
        ->setOption("id","new_inst_city_id")
        ->addCondition($form::EQUAL, "add")
          ->toggle('new_city_region_id')
          ->toggle('new_city_name');

    $row_one->addCell( 3 )
      ->addText(
        "new_city_name",
        "Jméno nového města"
      )
        ->setOption("id","new_city_name");
    
    $row_one->addCell( 3 )
      ->addSelect(
        "new_city_region_id",
        "Region v němž je nové město!",
        $this->localityManager->getFormOptionsLocalities(1,null,"Zvolte město" )
      )
        ->setOption("id","new_city_region_id");

    $row_two = $form->addRow();
    
    $row_two->addCell( 6 )
      ->addSelect(
        "role",
        $this->_t( "user.fields.role.name" ),
        $this->userInstitutionManager->getFormArrayAvailableRoles()
      )
        ->setDefaultValue(2)
        ->addCondition($form::EQUAL, 0 )
          ->toggle('role_other');

    $row_two->addCell( 6 )
      ->addText(
        "role_other",
        "Jiná role"
      )
        ->setOption("id","role_other");

    $form->addTextarea(
      "note",
      "Poznámka"
    );

    $form->addCheckbox(
      "valid",
      "Ověřená vazba"
    );

    $form->addProtection();

    $form->addSubmit(
      "send",
      $this->_t( "common.op.save" )
    );

    $form->onRender[] = [ $this, "prerender" ];

    $form->onValidate[] = [ $this, "validate" ];

    $form->onSuccess[] = [ $this, "success" ];


    return $form;

  }

  public function prerender( Form $form ): void
  {
    $user = $form->getPresenter()->getUser();

    // Hide the fields for admin users
    if ( ! $user->isAllowed( "user", "edit" ) ) {
      $form->removeComponent( $form["note"] );
      $form->removeComponent( $form["valid"] );
    }
  }

  public function validate( Form $form )
  {

    // Load the form values
    $values = $form->getValues();

    // 1. validate the necessary values
    if ( $values->user_id == null || $values->user_id == "" ) {
      $form->addError( 
        $this->_t( "common.error.internal_error" )
       );
    }

    // 2. the relationship must be unique
    if ( empty( $values->id ) && $values->inst != "add" ) {
      if ( $this->userInstitutionManager->relationExists( $values->user_id, $values->inst ) ) {
        $form->addError( 
          $this->resourceState( "relation", "already_exists" )
        );
      }
    }

    // 2. Validate roles
    if ( $values->role == 0 && empty( $values->role_other ) ) {
      $form["role_other"]->addError(
        $this->_t( "common.states.required_field" )
      );
    }

    // 3. validate the main structure
    try {

      // Check if the relation can be added
      $this->canSaveRelationship( $values, true, 0 );

    }

    // The institution was not selected
    catch ( InputValidationException $e ) {
      $form["inst"]->addError(
        $this->_t( "common.error.select_valid_value" )
      );
    }

    // The related institution is missing
    catch ( MissingItemException $e ) {
      $form["inst"]->addError(
        $this->resourceState( "institution","not_found",null, "f" )
      );
    }
    // The relation already exists!
    catch ( DupliciteItemException $e ) {
      $form["inst"]->addError(
        $this->resourceState("relation","already_exists")
      );
    }

    // The institution needs to be created first
    catch ( NotYetException $e ) {

      // Check if the institution can be added
      try {

        // Try to save the institution
        $this->canAddInstitution( $values, true, 1 );

      } 
      // The institution field is in bad regime
      catch (InputValidationException $e ) {
        $form["inst"]->addError(
          $this->_t( "common.error.select_valid_value" )
        );
      } 
      // One of required inputs is missing
      catch ( MissingInputException $e ) {
        switch ( $e->getData()["type"] ) {
          case "city":
            $form["new_inst_city_id"]->addError(
              $this->_t( "common.states.required_field" )
            );
            break;
          case "name":
            $form["new_inst_name"]->addError(
              $this->_t( "common.states.required_field" )
            );
            break;
        }
        
      } 
      // The city needs to be added first
      catch (NotYetException $e) {

        try {

          $this->canAddCity( $values, true, 2 );

        } 
        // The inst field is in the bad regime
        catch ( InputValidationException $e ) {
          $form["inst"]->addError(
            $this->_t( "common.error.select_valid_value" )
          );
        }
        // The city name is empty
        catch ( MissingInputException $e ) {
          $form["new_city_name"]->addError(
            $this->_t( "common.states.required_field" )
          );
        }
        // The given city name already exists
        catch ( DupliciteItemException $e ) {
          $form["new_city_name"]->addError(
            $this->resourceState("city","already_exists",$values->new_city_name, "n")
          );
        }
        // The region was not provided!
        catch ( MissingItemException $e ) {
          $form["new_city_region_id"]->addError( 
            $this->_t( "common.error.select_valid_value" )
          );
        }

      }

    }

    
    
  }

  public function success( Form $form, \stdClass $values ): void
  {

    // If the relationship can ba saved, do so right now!
    if ( $this->canSaveRelationship( $values ) ){

      $this->saveRelationship( $values, $values->user_id, $values->inst, $values->role, $values->role_other, $values->note, $values->valid );

    }
    // If the relationship can not be saved, create the institution first
    else {

      // If the institution can be created, do it wight now and then the relationship
      if ( $this->canAddInstitution( $values ) ) {

        $inst_id = $this->isntitutionManager->addInstitution( $values->new_inst_name );

        $inst_parents = $this->localityManager->getLocalityParentsIds( $values->new_inst_city_id );

        $this->isntitutionManager->setInstitutionLocality( $inst_id, $inst_parents[1], $inst_parents[0], $values->new_inst_city_id );

        $this->saveRelationship( $values, $values->user_id, $inst_id, $values->role, $values->role_other, $values->note, $values->valid );

      } 
      // If the city needs to be added first, do it!
      else {

        $city_id = $this->localityManager->addLocality( $values->new_city_name, $values->new_city_region_id, 2 );

        $inst_id = $this->isntitutionManager->addInstitution( $values->new_inst_name );

        $inst_parents = $this->localityManager->getLocalityParentsIds( $city_id );

        $this->isntitutionManager->setInstitutionLocality( $inst_id, $inst_parents[1], $inst_parents[0], $city_id );

        $this->saveRelationship( $values, $values->user_id, $inst_id, $values->role, $values->role_other, $values->note, $values->valid );

      }

    }

  }

  /**
   * @throws InputValidationException - the institution field value is invalid
   * @throws NotYetException - the institution needs to be created first
   * @throws MissingItemException - the related institution does not exist
   * @throws DupliciteItemException - this relation already exists!
   */
  private function canSaveRelationship( \stdClass $values, ?bool $throw = false, ?int $code = 0 ): bool
  {

    // Check if the input is valid
    if ( $values->inst === 0 ) {
      if ( $throw ) {
        throw new InputValidationException("Invalid input!!", $code);
      }
      return false;
    }

    // Check if the institution needs to be added first
    if ( $values->inst === "add" ) {
      if ( $throw ) {
        throw new NotYetException("The new institutionMust be added first!",$code);
      }
      return false;
    }

    // Check if the referenced institution exists
    if ( ! $this->isntitutionManager->institutionExists( $values->inst ) ) {
      if ( $throw ) {
        throw new MissingItemException("Missing institution", $code);
      }
      return false;
    }

    if ( $this->userInstitutionManager->relationExists( $values->user_id, $values->inst ) && empty( $values->id ) ) {
      throw new DupliciteItemException("This relation already exists!", $code);
      return false;
    }

    // If everything passed, proceed
    return true;

  }

  /**
   * @throws InputValidationException - the institution should not be added!
   * @throws MissingInputException - no new city was selected
   * @throws NotYetException - the city needs to be created first
   */
  private function canAddInstitution( \stdClass $values, ?bool $throw = false, ?int $code = 0 ): bool
  {

    // Can the new institution be added?
    if ( $values->inst !== "add" ) {
      if ( $throw ) {
        throw new InputValidationException("The institution should not be added!", $code );
      }
      return false;
    }

    // The city name should be filled
    if ( empty( $values->new_inst_name ) ) {
      if ( $throw ) {
        throw new MissingInputException( "The new institution name needs to be specified", $code, ["type"=>"name"] );
      }
      return false;
    }

    // The new institution city field has a proper value
    if ( $values->new_inst_city_id === 0 ) {
      if ( $throw ) {
        throw new MissingInputException("The new city needs to be selected!", $code, ["type"=>"city"] );
      }
      return false;
    }

    // Should the new city be created?
    if ( $values->new_inst_city_id === "add" ) {
      if ( $throw ) {
        throw new NotYetException( "The city need to be added first!", $code );
      }
      return false;
    }

    return true;
  }

  /**
   * @throws InputValidationException The new institution should not be added
   * @throws MissingInputException The city name is empty
   * @throws DupliciteItemException The city of the given name already exists
   * @throws MissingItemException The region of the new city is not provided or is missing
   */
  private function canAddCity( \stdClass $values, ?bool $throw = false, ?int $code = 0  ): bool
  {

    // Should the city be added?
    if ( $values->inst !== "add" ) {
      if ( $throw ) {
        throw new InputValidationException("No institution should be added!", $code );
      }
      return false;
    }

    // Is the name provided?
    if ( 
      empty( $values->new_city_name ) 
    ) {
      if ( $throw ) {
        throw new MissingInputException( "No name provided!", $code );
      }
      return false;
    }

    // Is the city name unique?
    if (
      ! $this->localityManager->isUniqueLocality( $values->new_city_name )
    ) {
      if ( $throw ) {
        throw new DupliciteItemException("The city already exists",$code);
      }
      return false;
    }

    // Is does the region exist?
    if (
      $values->new_city_region_id === 0
      || ! $this->localityManager->localityExists( $values->new_city_region_id )
    ) {

      if ( $throw ) {
        throw new MissingItemException( "The region does not exist!", $code );
      }
      return false;

    }

    // If everything passed, return true
    return true;

  }

  private function saveRelationship( \stdClass $values, int $user_id, int $inst_id, int $role, ?string $role_other = null, ?string $note = null, ?bool $verified = false ): void
  {
    // If the relation should be created...
    if (empty( $values->id ) ) {
      $this->userInstitutionManager->addRelationBetween( $user_id, $inst_id, $role, $role_other, $note, $verified );
    }
    // ...or if the relation should be updated.
    else {

      $prev_rel = $this->userInstitutionManager->getRelation( $values->id );

      if ( $prev_rel->institution_id != $inst_id ) {
        $this->userInstitutionManager->setRelationTargets($values->id, $user_id, $inst_id );
      }
      $this->userInstitutionManager->setRelationNote($values->id, $note );
      $this->userInstitutionManager->setRelationRole($values->id, $role, $role_other );
      $this->userInstitutionManager->setRelationVerification( $values->id, $verified );
    }

    // Finally, set the user city_field

    // The city_field should be saved only when the user has no field already
    $user = $this->userManager->getUser( $user_id );

    if ( empty( $user->city_id ) ) {

      $institution = $this->isntitutionManager->getInstitution( $inst_id );

      $this->userManager->setUserCity( $user_id, strip_tags( $institution->city_id ) );
    }
  }



}
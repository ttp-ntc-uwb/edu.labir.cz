<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use Nette;
use App\Model\UserManager;
use App\Model\ExperimentManager;
use App\Model\TaxonomyManager;
use App\Model\LogManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;
use Contributte\ImageStorage\ImageStoragePresenterTrait;
use Contributte\ImageStorage\ImageStorage;
use Nette\Http\FileUpload;

class ExperimentFormFactory extends FormFactory
{

  use Nette\SmartObject;

	// use ImageStoragePresenterTrait;

  /** @var UserManager */
  public $userManager;

  /** @var LogManager */
  public $logManager;

  /** @var ExperimentManager */
  public $experimentManager;

  /** @var TaxonomyManager */
  public $taxonomyManager;

  /** @var ImageStorage */
  public $imageStorage;

  /** @var Connection */
  public $database;

  /** @var Translator */
  public $translator;

  public function __construct(
    UserManager $userManager,
    ExperimentManager $experimentManager,
    TaxonomyManager $taxonomyManager,
    LogManager $logManager,
    Connection $database,
    Translator $translator,
    ImageStorage $imageStorage
  )
  {
    $this->userManager = $userManager;
    $this->experimentManager = $experimentManager;
    $this->taxonomyManager = $taxonomyManager;
    $this->logManager = $logManager;
    $this->database = $database;
    $this->translator = $translator;
    $this->imageStorage = $imageStorage;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden( "id" );

    $form->addText(
      "name",
      $this->_t( "fields.name.name" )
    );

    $r = $form->addRow();

    $r->addCell(6)
      ->addText(
        "slug",
        $this->_t( "fields.slug.name" )
      )
        ->setOption(
          "description",
          $this->_t( "fields.slug.hint.experiment" )
        );

    $r->addCell(6)->addInteger(
          "weight",
          $this->_t( "fields.weight.name" )
        )
          ->setRequired()
          ->setDefaultValue( 0 )
          ->setOption(
            "description",
            $this->_t( "fields.weight.hint.experiment" )
          );

    $form->addUpload(
        'thumb', 
        $this->_t( "fields.thumbnail.name" )
    )
      ->setOption(
        "description",
        $this->_t( "fields.thumbnail.hint.experiment" )
      );

    $form->addTextarea(
      "excerpt",
      $this->_t( "fields.excerpt.name" )
    )
      ->setOption(
        "description",
        $this->_t( "fields.excerpt.hint.experiment" )
      );

    $form->addTextArea(
      "text",
      "Hlavní text experimentu"
    )
      ->setHtmlAttribute("class","tiny");

    $form->addTextarea(
      "teaser",
      $this->_t( "fields.downloads_teaser.name" )
    )
      ->setHtmlAttribute("class","tiny")
      ->setOption(
        "description",
        $this->_t( "fields.downloads_teaser.hint.experiment" )
      )
      ->setDefaultValue(
        $this->_t( "fields.downloads_teaser.default.experiment" )
      );

    $row = $form->addRow();

    $row->addCell(6)
      ->addCheckboxList(
        "tags",
        $this->_t( "tag.res.sg.nom" ),
        $this->taxonomyManager->getFormArrayTaxonomies( $this->taxonomyManager::TYPE_TAG )
      );

    $row->addCell(6)
      ->addCheckboxList(
        "folders",
        $this->_t( "folder.res.sg.nom" ),
        $this->taxonomyManager->getFormArrayTaxonomies( $this->taxonomyManager::TYPE_FOLDER )
      );

    $row->addCell(6)
      ->addCheckboxList(
        "subjects",
        $this->_t("subject.res.pl.nom"),
        $this->taxonomyManager->getFormArrayTaxonomies( $this->taxonomyManager::TYPE_SUBJECT )
      );

    $row->addCell(6)
      ->addCheckboxList(
        "labels",
        $this->_t("label.res.pl.nom"),
        $this->taxonomyManager->getFormArrayTaxonomies( $this->taxonomyManager::TYPE_LABEL )
      );

    $form->addCheckboxList(
      "users",
      $this->_t( "user.res.pl.nom" ),
      $this->userManager->getFormArrayUsesByRole(4)
    );


    $row2 = $form->addRow();
    

    $cell1 = $row2->addCell(6);
    $cell1->addCheckbox(
      "public",
      $this->_t("fields.public.name")
    )
      ->setOption(
        "description",
        $this->_t( "fields.public.hint.experiment" )
      );

    $cell1->addCheckbox(
      "published",
      $this->_t("fields.published.name")
    )
      ->setOption(
        "description",
        $this->_t( "fields.published.hint.experiment" )
      );

    $cell2 = $row2->addCell(6);

    

    $cell2->addRadioList(
      "difficulty",
      $this->_t( "fields.difficulty.name" ),
      array(
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5
      )
    )
      ->setDefaultValue( 1 );

    $form->addSubmit(
      "send",
      $this->_t( "common.op.save" )
    );

    $form->addProtection();

    $form->onSuccess[] = [ $this, "process" ];


    return $form;

  }


  public function createUser(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden( "id" );

    $r = $form->addRow();

    $r->addCell(8)
      ->addText(
        "name",
        $this->_t( "fields.name.name" )
      );

    $r->addCell(4)
      ->addText(
        "slug",
        $this->_t( "fields.slug.name" )
      )
        ->setOption(
          "description",
          $this->_t( "fields.slug.hint.experiment" )
        );

    $c = $r->addCell(8);

    $c->addUpload(
        'thumb', 
        $this->_t( "fields.thumbnail.name" )
    )
      ->setOption(
        "description",
        $this->_t( "fields.upload.format.image" )
      );

    $c->addTextarea(
      "excerpt",
      $this->_t( "fields.excerpt.name" )
    )
      ->setOption(
        "description",
        $this->_t( "fields.excerpt.hint.experiment" )
      );

    $r->addCell(4)
    ->addRadioList(
      "difficulty",
      $this->_t( "fields.difficulty.name" ),
      array(
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5
      )
    )
      ->setDefaultValue( 1 );
      

    $form->addTextArea(
      "text",
      "Hlavní text experimentu"
    )
      ->setHtmlAttribute("class","tiny");

    $form->addTextarea(
      "teaser",
      $this->_t( "fields.downloads_teaser.name" )
    )
        ->setHtmlAttribute("class","tiny")
        ->setOption(
          "description",
          $this->_t( "fields.downloads_teaser.hint.experiment" )
        )
        ->setDefaultValue(
          $this->_t( "fields.downloads_teaser.default.experiment" )
        );

    $row = $form->addRow();

    $row->addCell(6)
      ->addCheckboxList(
        "tags",
        $this->_t( "tag.res.sg.nom" ),
        $this->taxonomyManager->getFormArrayTaxonomies( $this->taxonomyManager::TYPE_TAG )
      );

    $row->addCell(6)
      ->addCheckboxList(
        "subjects",
        $this->_t("subject.res.pl.nom"),
        $this->taxonomyManager->getFormArrayTaxonomies( $this->taxonomyManager::TYPE_SUBJECT )
      );

  

    $form->addSubmit(
      "send",
      $this->_t( "common.op.save" )
    );

    $form->addProtection();

    $form->onSuccess[] = [ $this, "process" ];


    return $form;

  }

  public function process( Form $form, \stdClass $values ): void
  {

    // $this->database->beginTransaction();

    if ( $values->id == "" || $values->id == false || $values->id == null || $values->id == 0 ) {

      // Create the experiment
      $experiment = $this->experimentManager->addExperiment( $values );

      // Save its thumbnail
      $this->saveImage( $experiment->id, $values->thumb );

      // Store the taxonomies

      // Subjects
      $this->taxonomyManager->setExperimentTaxonomyRelations( $experiment->id, $values->subjects, $this->taxonomyManager::TYPE_SUBJECT );

      // Tags
      $this->taxonomyManager->setExperimentTaxonomyRelations( $experiment->id, $values->tags, $this->taxonomyManager::TYPE_TAG );

      // Folders (optionally)
      if ( isset( $values->folders ) ) {
        $this->taxonomyManager->setExperimentTaxonomyRelations( $experiment->id, $values->folders, $this->taxonomyManager::TYPE_FOLDER );
      }

      // Labels (optionally)
      if ( isset( $values->labels ) ) {
        $this->taxonomyManager->setExperimentTaxonomyRelations( $experiment->id, $values->labels, $this->taxonomyManager::TYPE_LABEL );
      }

      // If the users field is present, save it, else save the default value
      if ( isset( $values->users ) ) {
        $this->experimentManager->setExperimentUsers( $experiment->id, $values->users );
      } else {
        $this->experimentManager->setExperimentUsers($experiment->id, [ $form->getPresenter()->getUser()->getId() ]);
      }

      // Create the log entry
      $this->logManager->addLog(
        4,
        $form->getPresenter()->getUser()->getId(),
        $experiment->id,
        [
          "experiment" => $experiment->name,
          "user" => $form->getPresenter()->getUser()->getIdentity()->getData()["name"]
        ]
        );

      

    } else {

      $this->experimentManager->setName( $values->id, $values->name );

      if ( isset( $values->public ) ) {
        $this->experimentManager->setSlug( $values->id, $values->slug );
      }

      $this->experimentManager->setText( $values->id, $values->text );

      $this->experimentManager->setExcerpt( $values->id, $values->excerpt );

      $this->experimentManager->setDifficulty( $values->id, $values->difficulty );

      // Optionally se the public value
      if ( isset( $values->public ) ) {
        $this->experimentManager->setPublic( $values->id, $values->public );
      }

      // Optionally set the published value
      if ( isset( $values->published ) ) {
        $this->experimentManager->setPublished( $values->id, $values->published );
      }

      // Optionally set the teaser
      if ( isset( $values->teaser ) )  {
        $this->experimentManager->setTeaser( $values->id, $values->teaser );
      }
      

      

      // Optionally set the weight
      if ( isset( $values->weight ) )  {
        $this->experimentManager->setWeight( $values->id, intval( $values->weight ) );
      }

      

      // Optionally set the thumbnail
      if ( $values->thumb->hasFile() ) {
        $this->saveImage( $values->id, $values->thumb );
      }


      if ( isset( $values->subjects ) ) {
        $this->taxonomyManager->setExperimentTaxonomyRelations( $values->id, $values->subjects, $this->taxonomyManager::TYPE_SUBJECT );
      }

      if ( isset( $values->labels ) ) {
        $this->taxonomyManager->setExperimentTaxonomyRelations( $values->id, $values->labels, $this->taxonomyManager::TYPE_LABEL );
      }

      if ( isset( $values->tags ) ) {
        $this->taxonomyManager->setExperimentTaxonomyRelations( $values->id, $values->tags, $this->taxonomyManager::TYPE_TAG );
      }

      if ( isset( $values->folders ) ) {
        $this->taxonomyManager->setExperimentTaxonomyRelations( $values->id, $values->folders, $this->taxonomyManager::TYPE_FOLDER );
      }

      if ( isset( $values->users ) ) {
        $this->experimentManager->setExperimentUsers( $values->id, $values->users );
      }



      


    }

    // $this->database->commit();

    // If was created, assign
    if ( empty( $values->id ) ) {
      
      $form->getPresenter()
        ->flashResourceState( 
          "success",
          "experiment", 
          "saved",
          $experiment->name
        );

      $form->getPresenter()
        ->kickFurther( $experiment->id );

    }

  }

  public function saveImage( int $id, FileUpload $image ): void
  {

    if ( $image->hasFile() ) {

      $experiment = $this->experimentManager->getExperiment( $id );

      // Delete the previous file
      if ( !empty( $experiment->thumb ) ) {

        $oldImage = $this->imageStorage->fromIdentifier( $experiment->thumb );

        $this->imageStorage->delete( $oldImage );

      }

      // Save the new image

      $newImage = $this->imageStorage->saveUpload( $image, 'experiments/'.$id."/thumb");

      $this->experimentManager->setThumb( $experiment->id, $newImage->identifier );

    }

  }



}
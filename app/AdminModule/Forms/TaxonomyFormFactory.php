<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use App\Model\DictionaryValidationException;
use Nette;
use App\Model\TaxonomyManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;
use NAttreid\Utils\Strings;

class TaxonomyFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var TaxonomyManager */
  public $taxonomyManager;

  /** @var Connection */
  public $database;

  /** @var Tranlator */
  public $translator;

  public function __construct(
    TaxonomyManager $taxonomyManager,
    Connection $database,
    Translator $translator
  )
  {
    $this->database = $database;
    $this->taxonomyManager = $taxonomyManager;
    $this->translator = $translator;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden( "id" );

    $form->addText(
      "name",
      $this->_t( "fields.name.name" )
    );

    $form->addText(
      "slug",
      $this->_t( "fields.slug.name" )
    )
      ->setOption(
        "description",
        $this->_t( "fields.slug.desc.optional" )
      );

    $form->addTextarea(
      "description",
      $this->_t( "fields.description.name" )
    );

    $form->addRadioList(
      "type",
      $this->_t( "common.fields.type" ),
      $this->taxonomyManager->getFormArrayTypes()
    );

    $form->addProtection();

    $form->onSuccess[] = [ $this, "process" ];
    $form->onValidate[] = [ $this, "validate" ];

    $form->addSubmit(
      "send",
      $this->_t( "common.op.save" )
    );

    return $form;

  }

  public function validate( Form $form ): void
  {

    $values = $form->getValues();


    // Validate and format the type
    if ( ! $this->taxonomyManager->isValidType( $values->type ) ) {
      $form->getComponent( "type" )->addError(
        $this->_t( "common.error.select_valid_value" )
      );
    }

  }

  public function process( Form $form, \stdClass $values ): void
  {
    if ( $form->isValid() ) {
      
      $slug = $values->slug != "" ? Strings::webalize( $values->slug ) : Strings::webalize( $values->name );

      \Tracy\Debugger::barDump( $values );

      if ( $values->id == "" || $values->id == null || $values->id == 0 ) {

        $this->taxonomyManager->addTaxonomy(
          $values->type,
          $values->name,
          $slug,
          $values->description
        );

      } else {

        $taxonomy = $this->taxonomyManager->getTaxonomy( $values->id );

        $this->taxonomyManager->setName( $values->id, $values->name );

        $this->taxonomyManager->setDescription( $values->id, $values->description );

        $this->taxonomyManager->setType( $values->id, $values->type );

        if ( $taxonomy->slug !== $slug ) {
          $this->taxonomyManager->setSlug( $taxonomy->id, $slug );
        }

      }

      $form->getPresenter()->flashResourceState("success","taxonomy","saved",$values->name,"f");

      $target = "default";

      if ( $values->type == 0 ) {
        $target = "overviewSubjects";
      } else if ( $values->type == 1 ) {
        $target = "overviewTags";
      } else if ( $values->type == 2 ) {
        $target = "overviewTags";
      } else if ( $values->type == 3 ) {
        $target = "overviewLabels";
      }

      $form->getPresenter()->redirect( ":Admin:Taxonomy:".$target );

    }
  }

}

<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use Nette;
use App\Model\UserManager;
use App\Model\TaxonomyManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;

class UserSubjectsFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var TaxonomyManager */
  public $taxonomyManager;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  public function __construct(
    UserManager $userManager,
    TaxonomyManager $taxonomyManager,
    Connection $connection,
    Translator $translator
  )
  {
    $this->userManager = $userManager;
    $this->taxonomyManager = $taxonomyManager;
    $this->connection = $connection;
    $this->translator = $translator;
  }

  public function create(): BootstrapForm
  {
    $form = new BootstrapForm();

    $form->addHidden( "id", null );

    $form->addCheckboxList(
      "subjects",
      $this->_t("subject.res.pl.nom"),
      $this->taxonomyManager->getFormArrayTaxonomies( $this->taxonomyManager::TYPE_SUBJECT )
    );

    $form->addProtection();

    $form->addSubmit(
      "send",
      $this->_t( "user.forms.domain.send" )
    );

    $form->onSuccess[] = [ $this, "process" ];

    return $form;

  }

  public function process( Form $form, \stdClass $values ): void
  {

    $this->taxonomyManager->setUserTaxonomyRelations( $values->id, $values->subjects, $this->taxonomyManager::TYPE_SUBJECT );

    // If there are more than one subject, add the creator role
    if ( count( $values->subjects ) > 0 ) {
      $this->userManager->addUserRole( $values->id, 3 );
      $this->addCurrentUserRole($form,"content_creator");
    } else {
      $this->userManager->removeUserRole( $values->id, 3 );
      $this->removeCurrentUserRole($form,"content_creator");
    }
  }

}
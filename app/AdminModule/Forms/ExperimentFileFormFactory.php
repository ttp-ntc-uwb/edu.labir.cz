<?php

namespace App\AdminModule\Forms;


use Nette;
use App\AdminModule\Forms\FormFactory;
use App\Model\UserManager;
use App\Model\ExperimentFileManager;
use App\Model\ExperimentManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;
use Contributte\ImageStorage\ImageStorage;
use Nette\Utils\FileSystem;
use NAttreid\Utils\Strings;
use Nette\Utils\Finder;


class ExperimentFileFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var ExperimentManager */
  public $experimentManager;

  /** @var ExperimentFileManager */
  public $experimentFileManager;

  /** @var Connection */
  public $database;

  /** @var Translator */
  public $translator;

  /** @var ImageStorage */
  public $imageStorage;

  /** @var array */
  public $mimeTypes;

  /** @var array */
  public $mimeScopes;

  const THUMB_W = 400;
  const THUMB_H = 350;
  const THUMB_DIMENSIONS = "400x300.fill";

  public function __construct(
    UserManager $userManager,
    Connection $database,
    Translator $translator,
    ExperimentManager $experimentManager,
    ExperimentFileManager $experimentFileManager,
    ImageStorage $imageStorage
  )
  {

    $this->userManager = $userManager;
    $this->experimentManager = $experimentManager;
    $this->experimentFileManager = $experimentFileManager;
    $this->database = $database;
    $this->translator = $translator;
    $this->imageStorage = $imageStorage;

    $this->mimeTypes();
  }

  public function mimeTypes(): void
  {
    $this->mimeTypes = [
      
      // Text files
      "doc" => [
        "mime" => "application/msword",
        "prompt" => $this->_t( "file.mime.labels.doc" ),
        "scope" => "text"
      ],
      "docx" => [
        "mime" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "prompt" => $this->_t( "file.mime.labels.doc" ),
        "scope" => "text"
      ],
      "rtf" => [
        "mime" => "application/rtf",
        "prompt" => $this->_t( "file.mime.labels.doc" ),
        "scope" => "archive"
      ],
      "txt" => [
        "mime" => "text/plain",
        "prompt" => $this->_t( "file.mime.labels.doc" ),
        "scope" => "text"
      ],
      "odt" => [
        "mime" => "application/vnd.oasis.opendocument.text",
        "prompt" => $this->_t( "file.mime.labels.doc" ),
        "scope" => "text"
      ],
      // Presentation files
      "ppt" => [
        "mime" => "application/vnd.ms-powerpoint",
        "prompt" => $this->_t( "file.mime.labels.ppt" ),
        "scope" => "presentation"
      ],
      "pptx" => [
        "mime" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "prompt" => $this->_t( "file.mime.labels.ppt" ),
        "scope" => "presentation"
      ],
      "odp" => [
        "mime" => "application/vnd.oasis.opendocument.presentation",
        "prompt" => $this->_t( "file.mime.labels.ppt" ),
        "scope" => "presentation"
      ],
      // PDF
      "pdf" => [
        "mime" => "application/pdf",
        "prompt" => $this->_t( "file.mime.labels.pdf" ),
        "scope" => "pdf"
      ],
      // Images
      "jpg" => [
        "mime" => "image/jpeg",
        "prompt" => $this->_t( "file.mime.labels.image" ),
        "scope" => "image"
      ],
      "png" => [
        "mime" => "image/png",
        "prompt" => $this->_t( "file.mime.labels.image" ),
        "scope" => "image"
      ],
      "gif" => [
        "mime" => "image/gif",
        "prompt" => $this->_t( "file.mime.labels.image" ),
        "scope" => "image"
      ],
      // Video
      "mp4" => [
        "mime" => "video/mp4",
        "prompt" => $this->_t( "file.mime.labels.video" ),
        "scope" => "video"
      ],
      // Archives
      "rar" => [
        "mime" => "application/vnd.rar",
        "prompt" => $this->_t( "file.mime.labels.archive" ),
        "scope" => "archive"
      ],
      "zip" => [
        "mime" => "application/zip",
        "prompt" => $this->_t( "file.mime.labels.archive" ),
        "scope" => "archive"
      ],
      // Binary file
      "lrc" => [
        "mime" => "application/lrc",
        "prompt" => $this->_t( "file.mime.labels.archive" ),
        "scope" => "binary"
      ],
    ];

  }

  public function getMimeType( string $extension ): string
  {
    return $this->mimeTypes[ $extension ][ "mime" ];
  }

  public function getMimeTypeArray(): array
  {
    return array_map(function($item){
      return $item["mime"];
    },$this->mimeTypes);
  }

  public function routeScope( string $mime )
  {

    foreach ( $this->mimeTypes as $item ) {

      if ( $item[ "mime" ] == $mime ) {
        return $item[ "scope" ];
      }

    }

  }
  

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden( "experiment_id" );
    $form->addHidden( "id" );

    $form->addProtection();

    $form->addUpload(
      "upload",
      $this->_t( "fields.upload.main" )
    )
      ->setOption(
        "description",
        $this->_t( "field.upload.format.experiment.other" )
      )
      ->addCondition( Form::IMAGE, true)
        ->toggle("thumb_uploader");

    $form->addUpload(
      "thumb",
      $this->_t( "fields.thumbnail.name" )
    )
      ->setOption( "id", "thumb_uploader" )
      ->setOption( 
        "description", 
        $this->_t( "fields.upload.format.image" ) 
      );

    $form->addText(
      "name",
      $this->_t( "experiment.fields.files.name" )
    )
      ->setRequired("Název souboru je povinný v každém případě.");

    $form->addTextarea(
      "description",
      $this->_t( "fields.description.name" )
    )
      ->setHtmlAttribute("class","tiny-minimal");

    $form->addCheckbox(
      "public",
      $this->_t( "fields.public.name" )
    )
      ->setOption( 
        "description", 
        $this->_t( "fields.public.hint.file" ) 
      );

    $form->onSuccess[] = [ $this, "process" ];

    $form->onValidate[] = [ $this, "validate" ];

    $form->addSubmit(
      "send",
      $this->_t( "fields.upload.ops.upload_file" )
    );

    return $form;

  }

  public function validate( Form $form ): void
  {

    $values = $form->getValues();

    \Tracy\Debugger::barDump( $values );

    if ( $values->id == ""  || $values->id == 0 || $values->id == null ) {

      if ( ! $values->upload->hasFile() ) {

        $form->addError( "Bylo by hezké nahrát sem soubor" );
        $form["upload"]->addError("");

      } else if ( ! in_array( $values->upload->getContentType(), $this->getMimeTypeArray() ) ) {
        
        $form->addError( "Špatný typ souboru" );
        $form["upload"]->addError("");

      }

    } else {

      if ( $values->upload->hasFile() && ! in_array( $values->upload->getContentType(), $this->getMimeTypeArray() ) ) {
        
        $form->addError( "Špatný typ souboru" );
        $form["upload"]->addError("");

      }

    }

  }

  public function process( Form $form, \stdClass $values ): void
  {

    if ( ! $form->isValid() ) {
      return;
    }

    $file = null;

    if ( empty( $values->id ) || $values->id == "" || $values->id == false || $values->id == 0 ) {

      // Create the base experiment
      $file = $this->experimentFileManager->addExperimentFile( $values->experiment_id, $values->name, $values->description );

    } else {

      // Load the base experiment
      $file = $this->experimentFileManager->getExperimentFile( $values->id );

      $this->experimentFileManager->setName( $file->id, $values->name );

      $this->experimentFileManager->setDescription( $file->id, $values->description );

    }

    $this->experimentFileManager->setPublic( $file->id, $values->public );

    

    // Create the folder
    $basePath = "experiments/".$values->experiment_id . "/files/" . $file->id;

    $folderPath = "experiments/".$values->experiment_id . "/files/" . $file->id;

    FileSystem::createDir( $folderPath );


    if ( 
      $values->upload->hasFile()
    ) {

      // Load the new file scope
      if ( $values->upload->hasFile() ) {
        $mime = $values->upload->getContentType();
      } else {
        $f = Finder::find( $file->path );
        $mime = $f;
      }
      

      $scope = $this->routeScope( 
        $values->upload->getContentType()
      );
      
      switch ( $scope ) {

        case "image":
          $this->saveImage( $file->id, $basePath, $values );
          break;

        default:
          $this->saveOther( $file->id, $basePath, $values );
          break;

      }

      // Once verything is done, store remaining values

      // Store the filename
      $this->experimentFileManager->setFilename( $file->id, Strings::webalize( $values->upload->getName(), ".-_") );

      // Store the scope
      $this->experimentFileManager->setScope( $file->id, $scope );

      // Store the file size
      $this->experimentFileManager->setSize( $file->id, $values->upload->getSize() );

      // Store the extension
      $this->experimentFileManager->setExtension( $file->id, pathinfo( $values->upload->getTemporaryFile() )["extension"] );

    }

    if ( $values->thumb->hasFile() ) {
      
      // Delete the previous thumbnail
      $this->deletePreviousThumb( $file->id );

      // Move the thumbnail
      $t = $this->imageStorage->saveUpload( $values->thumb, $basePath . "/thumb" );

      // Store the thumb in the DB
      $this->experimentFileManager->setThumb( $file->id, $t->identifier );

    }

    // Flash message and redirects are based on the presenter kickFurtner
    $form->getPresenter()->flashResourceState( "success", "file", "updated" );
    $form->getPresenter()->kickFurther( $values->experiment_id );

    return;

  }

  public function deletePreviousFiles( int $file_id ): void
  {
    $this->deletePreviousFile( $file_id );
    $this->deletePreviousThumb( $file_id );
  }

  public function deletePreviousFile( int $file_id ): void
  {
    $file = $this->experimentFileManager->getExperimentFile( $file_id );
    if ( $file != null ) {
      if ( $file->path ) {
        switch ( $file->scope ) {
          case "image":
            $this->deleteImage( $file->path, $file->filename );
            break;
          default:
            FileSystem::delete( $file->path );
            break;
        }
      }
    }
  }

  public function deletePreviousThumb( int $file_id ): void
  {
    $file = $this->experimentFileManager->getExperimentFile( $file_id );
    if ( $file != null ) {
      if ( $file->thumbnail ) {
        switch ( $file->scope ) {
          case "image":
            $this->deleteImage( $file->path, $file->filename );
            break;
          default:
            FileSystem::delete( $file->thumbnail );
            break;
        }
      }
    }
  }

  private function deleteImage( string $path, string $filename ): void
  {
    $length = strlen( $path ) - strlen( $filename );
    $folder = substr($path, 0, $length );
    FileSystem::delete( $folder );
  }

  public function saveImage( int $file_id, string $path, \stdClass $values ): void
  {

    // $path = "uploads/" . $path;

    // Delete the previous file
    $this->deletePreviousFile( $file_id );

    // Delete previous thumbnail
    $this->deletePreviousThumb( $file_id );

    // Move the upload to the destination folder

    $f = $this->imageStorage->saveUpload( $values->upload, $path );

    // Save the image to the DB
    $this->experimentFileManager->setPath( $file_id, $f->identifier );

    // Clear all eventual thumbnails from the DB
    $this->experimentFileManager->setThumb( $file_id, $f->identifier );

    

  }

  public function saveOther( int $file_id, string $path, \stdClass $values ): void
  {

    $path = "uploads/" . $path;

    // Delete the previous file
    $this->deletePreviousFile( $file_id );

    // Assamble the target path
    $filePath = $path . "/" .$values->upload->getSanitizedName() ;

    if ( $values->upload->hasFile() ) {

      // Move the file into target path
      $values->upload->move( $filePath );

      // Store the file to the DB
      $this->experimentFileManager->setPath( $file_id, $filePath );

    }

    

  }

}
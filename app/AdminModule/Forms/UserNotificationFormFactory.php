<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use Nette;
use App\Model\UserManager;
use App\Model\LogManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;

class UserNotificationFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var LogManager */
  public $logManager;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  public function __construct(
    UserManager $userManager,
    LogManager $logManager,
    Connection $connection,
    Translator $translator
  )
  {
    $this->userManager = $userManager;
    $this->logManager = $logManager;
    $this->connection = $connection;
    $this->translator = $translator;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;


    return $form;

  }

  public function process( Form $form, \stdClass $values ): void
  {
    
  }



}
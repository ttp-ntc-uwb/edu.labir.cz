<?php

namespace App\AdminModule\Forms;


use Nette;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Nette\Forms\Form;
use Nette\Security\SimpleIdentity;

/**
 * The purpose of this form factory is to present the basic connections and translations
 */
class FormFactory
{
  
  use Nette\SmartObject;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  public function __construct(
    Connection $connection,
    Translator $translator
  )
  {
    $this->connection = $connection;
    $this->translator = $translator;
  }

  /**
   * Assign a new role to the current user's SimpleIdentity
   */
  public function addCurrentUserRole( Form $form, string $name ): void
  {
    $identity = $this->getCurrentUserIdentity($form);
    if ( !empty( $identity ) ) {
      \Tracy\Debugger::barDump( $identity );
      if ( $identity->id == intval( $form->getValues()->id ) ) {
        $roles_old = $identity->getRoles();

        $roles_new = $roles_old;

        if ( !in_array( $name, $roles_old ) ) {
          $roles_new[] = $name;
        }

        $identity->setRoles( $roles_new );
      }
    }
  }

  /**
   * Remove a role from the current user's SimpleIdentity
   */
  public function removeCurrentUserRole( Form $form, string $name ): void
  {
    $identity = $this->getCurrentUserIdentity($form);
    if ( !empty( $identity ) ) {
      \Tracy\Debugger::barDump( $identity );
      if ( $identity->id == intval( $form->getValues()->id ) ) {
        $roles_old = $identity->getRoles();

        $roles_new = $roles_old;

        if ( in_array( $name, $roles_old ) ) {
          $roles_new = array_diff( $roles_old, [$name] );
        }

        $identity->setRoles( $roles_new );
      }
    }
  }

  private function getCurrentUserIdentity( Form $form ): ?SimpleIdentity
  {
    return $form->getPresenter()->getUser()->getIdentity();
  }

  /**
   * Translate a simple string
   */
  public function _t( string $translation_key, ?array $args = [] ): string
  {
    return $this->translator->translate( $translation_key, $args );
  }

  public function resourceState( string $resource, string $state, ?string $item = null, ?string $genre = "m", ?string $number = "sg", ?string $message = null ): string
  {

    $buffer = [];

    // Check if number is valid
    if ( !in_array( $number, ["sg","pl"] ) ) {
      $number = "sg";
    }

    // Check if the genre is valid
    if ( !in_array( $genre, ["m","f","n"] ) ) {
      $genre = "m";
    }

    // Load the article based on number and genre
    $article = $this->_t( implode(".",[ "common.art.def", $number , $genre ] ) );
    
    // Store article if not null
    if ($article !== "" ) {
      $buffer[] = $article;
    }

    // Store resource
    $buffer[] = $this->_t( $resource . ".res." . $number .".nom" );

    // Eventually, store the resource name
    if ( ! empty( $item ) ) {
      $buffer[] = $item;
    }

    // Store the state
    $buffer[] = $this->_t( implode(".",["common.was", $state, $number, $genre ] ) );

    

    $text = implode( " ", $buffer ) . "!";

    // Store the eventual message
    if ( ! empty( $message ) ) {
      $text .= " " . $message;
    }

    // If the item is present, return the present
    return $text;

  }

  /**
   * Format a standardised ACL error:
   * - "You do not have sufficent permissions."
   * - "You can not [$operation] [this] [$resource] [$item]."
   * - "You can not edit Pilsen."
   * - "You can not edit this city."
   * - "You can not edit the city Pilsen."
   * 
   * @param string|null $operation
   * @param string|null $resource
   * @param string|null $item "m" / "f" / "entry name"
   * @return string|null
   * @see common.cs.neon
   */
  public function accessError( ?string $operation=null, ?string $resource=null, ?string $item=null, ?bool $plural = false ): ?string
  {

    // not_allowed
    // !$operation && !$resource
    if ( empty( $operation ) && empty( $resource ) ) {
      return $this->_( "common.error.not_allowed" );
    }

    // init $args
    $args = [
      "op" => strtolower( $this->_( "common.op." . $operation ) )
    ];

    // Format the number
    $number = $plural ? "pl" : "sg";


    // not_allowed_op_item
    // $operation && $item && $item != "m" && $item != "f"
    if (
      !empty( $operation ) 
      && empty( $resource )
      && !empty( $item )
      && $item !== "m"
      && $item !== "f"
    ) {
      return $this->translator->translate("common.error.not_allowed_op_item",array_merge($args,["item"=>$item]));
    }

    if ( !empty( $resource ) ) {
      // Add resource to translation
      $args["res"] = strtolower( $this->_( implode( ".",[$resource,"res",$number,"gen"] ) ) );
    }

    // not_allowed_op_res
    // $operation && $resource && !empty
    if (
      !empty( $operation )
      && !empty( $resource )
      && empty( $item )
    ) {

      $args["res"] = strtolower( $this->_( implode( ".",[$resource,"res.pl.gen"] ) ) );

      return $this->translator->translate( "common.error.not_allowed_op_res", $args );

    }

    // not_allowed_op_res_*
    if (
      !empty( $operation )
      && !empty( $resource )
      && !empty( $item )
    ) {

      // not_allowed_op_res_m
      if ( $item == "m" ) {
        return $this->translator->translate( "common.error.not_allowed_op_res_m", $args );
      }
      // not_allowed_op_res_f
      else if ( $item == "f" ) {
        return $this->translator->translate( "common.error.not_allowed_op_res_f", $args );
      } else {
        return $this->translator->translate( "common.error.not_allowed_op_item_res", array_merge($args,["item"=>$item]) );
      }

    }

    return null;

  }

}
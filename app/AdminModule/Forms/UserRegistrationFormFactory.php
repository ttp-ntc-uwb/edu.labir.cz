<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use Nette;
use App\Model\UserManager;
use App\Model\LogManager;
use App\Model\RequestManager;
use App\Services\Mailer;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;
use Nette\Utils\Html;

class UserRegistrationFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var LogManager */
  public $logManager;

  /** @var RequestManager */
  public $requestManager;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  /** @var Mailer  */
  public $mailer;

  public function __construct(
    UserManager $userManager,
    Connection $connection,
    Translator $translator,
    LogManager $logManager,
    RequestManager $requestManager,
    Mailer $mailer
  )
  {
    $this->userManager = $userManager;
    $this->connection = $connection;
    $this->translator = $translator;
    $this->requestManager = $requestManager;
    $this->logManager = $logManager;
    $this->mailer = $mailer;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addText(
      "name",
      $this->_t( "fields.name.name" )
    );

    $form->addText(
      "surname",
      $this->_t( "fields.surname.name" )
    );

    $form->addEmail(
      "email",
      $this->_t( "fields.email.name" )
    );

    $form->addCheckbox(
      "consent",
      $this->_t( "sign.register.consent" )
    )
      ->setOption( 
        "description", 
        Html::el("a")
          ->setText(
            $this->_t( "sign.register.consent_link" )
          )
      );

    $form->addSubmit(
      "send",
      $this->_t( "common.op.register" )
    );

    $form->addProtection();

    $form->onValidate[] = [ $this, "validate" ];
    $form->onSuccess[] = [ $this, "process" ];


    return $form;

  }

  public function validate( BootstrapForm $form ): void
  {

    $values = $form->getValues();

    // The user must be unique
    if ( $this->userManager->userEmailExists( $values->email ) ) {

      $form->getComponent( "email" )->addError( $this->resourceState("user","already_exists", $values->email ) );

    }

    // The consent is required
    if ( empty( $values->consent ) ) {

      $form->getComponent( "consent" )->addError( $this->_t( "sign.register.consent_error" ) );

    }

  }

  public function process( BootstrapForm $form ): void
  {

    \Tracy\Debugger::barDump( "komponenta" );

    if ( $form->isValid() ) {

      $values = $form->getValues();

      try {

        $user_id = $this->userManager->addUser( $values->email, 0 );

        $this->userManager->setUserName( $user_id, $values->name, $values->surname );

        $this->userManager->addUserRole( $user_id, 1 );

        // Add the new request
        $request = $this->requestManager->addRequest( true, 0, $user_id, "+2 days", null, ["email" => $values->email] );

        // Add the log input
        $log = $this->logManager->addLog( 0, $user_id, null, ["email"=>$values->email] );

        // Flash the success
        $form->getPresenter()->flashResourceState("success", "user","created",$values->email );

        // Send the email
        $data = [
          "email" => $values->email,
          "time" => $request->time_from,
          "hash" => $request->hash
        ];
  
        $message = $this->mailer->createEmail( "userRegisteredSelf.latte", $data );

        $message->addTo($values->email);
  
        $this->mailer->sendEmail( $message );

        $form->getPresenter()->flash( 
          "success", 
          $this->_t( "sign.register.success" )
        );
        
      }

      catch ( \Nette\Mail\FallbackMailerException $e ) {

          $form->getPresenter()->flash( 
            "danger", 
            $this->_t( "sign.register.mail_error" )
          );

          \Tracy\Debugger::$maxLength = 5000;

          \Tracy\Debugger::barDump( $message->body );

          \Tracy\Debugger::barDump( $e );

      } catch ( \App\Model\InputValidationException $e ) {

        $form->getComponent( "email" )->addError( "nesprávný formát emailu" );

      } catch ( \App\Model\DupliciteItemException $e ) {

        $form->getComponent( "consent" )->addError( "S tímhle holt musíte souhlasit, přes to vlak nejede." );

      }


    }
  
  }



}
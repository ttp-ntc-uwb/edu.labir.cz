<?php

namespace App\AdminModule\Forms;

use App\AdminModule\Forms\FormFactory;
use Nette;
use App\Model\UserManager;
use Nette\Application\UI\Form;
use Nette\Database\Connection;
use Contributte\Translation\Translator;
use Contributte\FormsBootstrap\BootstrapForm;

class UserPasswordFormFactory extends FormFactory
{

  use Nette\SmartObject;

  /** @var UserManager */
  public $userManager;

  /** @var Connection */
  public $connection;

  /** @var Translator */
  public $translator;

  public function __construct(
    UserManager $userManager,
    Connection $connection,
    Translator $translator
  )
  {
    $this->userManager = $userManager;
    $this->connection = $connection;
    $this->translator = $translator;
  }

  public function create(): BootstrapForm
  {

    $form = new BootstrapForm;

    $form->addHidden( "id", null );

    $form->addPassword(
      "password",
      $this->_t("user.fields.password.new")
    );

    $form->addPassword(
      "password_repeat",
      $this->_t("fields.password.ops.repeat")
    );

    $form->addProtection();

    $form->addSubmit(
      "send",
      $this->_t("user.forms.password.send")
    );

    $form->onValidate[] = [ $this, "validate" ];

    $form->onSuccess[] = [ $this, "process" ];

    return $form;

  }

  public function validate( Form $form, \stdClass $values ): void
  {

    try {

      // Control if the user exists
      $user = $this->userManager->getUser( $values->id );

      // Control if the first password is of the proper format
      $this->userManager->validatePassword( $values->password, 0 );

      // Control if the passwords matches
      $this->userManager->validatePasswords( $values->password, $values->password_repeat, 1 );

    } catch ( \App\Model\InputValidationException $e ) {

      switch ( $e->getCode() ) {

        case 0:

          $err = $this->_t("fields.password.states.errors") . " ". implode(", ", $e->getData()["hints"]) . ".";

          $form["password"]->addError( $err );
          break;

        case 1:

          $form["password_repeat"]->addError(
            $this->_t("user.fields.password.states.mismatch") 
          );
          break;

      }

      // $form->addError( "Nepodařilo se uložit heslo" );

    } catch ( \App\Model\MissingItemException $e  ) {

      $form->addError( $this->formatStateMessage("user","does_not_exist") );

    }

  }

  public function process( Form $form, \stdClass $values ): void
  {

    if ( $form->isValid() ) {

      $this->userManager->setUserPassword( $values->id, $values->password, $values->password_repeat );

      $user = $form->getPresenter()->getUser();

    }

    

  }



}
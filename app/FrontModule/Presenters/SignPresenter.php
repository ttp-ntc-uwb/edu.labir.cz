<?php

/**
 * User sign in & register & out presenter
 * @author Jan Jáchim
 */

namespace App\FrontModule\Presenters;

use Nette;

use Contributte\FormsBootstrap\BootstrapForm;
use Contributte\FormsBootstrap\Enums\RenderMode;

use Contributte\MenuControl\UI\MenuComponent;
use Nette\Application\UI\Form;
use App\Model\RequestManager;
use App\Model\LogManager;
use App\Services\Mailer;
use App\Model\UserManager;

use App\AdminModule\Forms\UserRegistrationFormFactory;

class SignPresenter extends BaseFrontPresenter
{

  /** @var UserManager @inject */
  public $userManager;

  /** @var RequestManager @inject */
  public $requestManager;

  /** @var LogManager @inject */
  public $logManager;

  /** @var Mailer @inject */
  public $mailer;

  /** @var Nette\Security\User */
  public $user;

  /** @var UserRegistrationFormFactory @inject */
  public $userRegistrationFormFactory;

  /** @var \Contributte\MenuControl\UI\IMenuComponentFactory @inject */
  public $menuFactory;

  public function startup(): void
  {
    parent::startup();

    // Set base variables for template
    
    $this->template->titleSuffix = true;

    $this->template->layoutContainer = "container";
    $this->template->layoutRow = "row justify-content-center";
    $this->template->layoutColumn = "col-12 col-md-8 col-md-6 col-lg-5";

    // Set the layout
    $this->useLayout("@simple.latte");
    
  }

  /**
   * Sign menu component
   */
	protected function createComponentMenuSign(): MenuComponent
	{
		return $this->menuFactory->create('sign');
  }




  // VIEWS




  /**
   * Login
   */ 
  public function renderLogin(): void
  {

    // User must be logged out
    $this->userMustBeLoggedOutCheck();

    $this->template->title = $this->_t( "sign.login.title" );

  }

  /**
   * Logout
   */
  public function renderLogout(): void
  {

    // User must be logged in
    $this->userMustBeLoggedInCheck();

    $this->template->title = $this->_t( "sign.logout.title" );

  }

  /**
   * Request a new password
   */
  public function renderRequest(): void
  {

    // User must be logged out
    $this->userMustBeLoggedOutCheck();

    $this->template->title = $this->_t( "sign.request.title" );

  }

  /**
   * Set a new password
   */
  public function renderRestore( string $hash = null ): void
  {

    // User must be logged out
    $this->userMustBeLoggedOutCheck();

    // Hash must be not null
    if ( $hash == null ) {
      $this->redirect( ":Front:Static:default" );
    }

    $request = $this->requestManager->getRequestFromHash( $hash );

    // If the request is null, redirect
    if ( $request == null ) {
      $this->flashResourceState( "danger", "request", "expired_or_missing" );
      $this->redirect( ":Front:Static:default" );
    }

    $user_id = $request->user_id;

    // If a user does not exist, redirect out
    if ( ! $this->userManager->userExists( $user_id ) ) {

      $this->flashResourceState("error","user","does_not_exist");
      $this->redirect( ":Front:Static:default" );
      
    }

    // Get the user name
    $user_name = $this->userManager->getUserName( $user_id );

    $title_prefix = $this->_t( "sign.restore.titlePrefix" );


    // Set the template variables

    $this->template->titlePrefix = $title_prefix;
    $this->template->title = $user_name;
    $this->template->titleSeo = $title_prefix . " " . $user_name;

    // Set default values for the form

    $form = $this->getComponent("restoreForm");
    $form->setDefaults([
        "id" => $user_id,
        "request_id" => $request->id,
      ]);

  }

  /**
   * Register a new user
   */
  public function renderRegister(): void
  {

    // User must be logged out
    $this->userMustBeLoggedOutCheck();

    $this->template->title = $this->_t( "sign.register.title" );

  }

  public function renderRegisterConfirm( string $hash ): void
  {

    try {

      // Load the request
      $request = $this->requestManager->getRequestFromHash( $hash );

      // Load the user
      $user = $this->userManager->getUser( $request->user_id );

      $user_name = $this->userManager->getUserName( $user->id );

      $this->template->title = $this->translator->translate( "sign.restore.title", [ "name" => $user_name ] );

      // Fill in the set password form
      $form = $this->getComponent("restoreForm");
      $form->setDefaults([
          "id" => $user->id,
          "request_id" => $request->id,
        ]);

    } catch ( \Exception $e ) {



    }
    
  }

  public function renderVerifyEmail( string $hash ): void
  {
    try {

      // Load the request
      $request = $this->requestManager->getRequestFromHash( $hash );

      // Load the user
      $user = $this->userManager->getUser( $request->user_id );

      $this->template->title = $this->_t( "actions.user_email_verification.response.title", [ "email" => $user->email ] );

      // If the request is not valid, flash, delete and redirect
      if ( ! $this->requestManager->requestIsValid( $request->id ) ) {

        $this->flash( "danger", "actions.user_email_verification.response.err.timeout" );

        $this->requestManager->deleteRequest( $request->id );

        $this->redirect( "Admin:User:settings" );

      }

      // Add default data to the form
      $this->getComponent( "emailVerificationForm" )
        ->setDefaults( [
          "request_id" => $request->id
        ] );

      

    } catch (\App\Model\MissingItemException $e) {
      throw $e;
      $this->flashResourceState( "danger", "request", "expired_or_missing" );
    }
  }

  public function createComponentEmailVerificationForm(): Form
  {
    $form = new BootstrapForm;

    $form->addHidden( "request_id" );

    $form->addText(
      "code",
      $this->_t( "actions.user_email_verification.response.insert_code" )
    );

    $form->addProtection();

    $form->addSubmit( 
      "send",  
      $this->_t( "common.op.send" )
    );

    $form->onSuccess[] = [ $this, "emailVerificationFormSuccess" ];

    return $form;
  }

  public function emailVerificationFormSuccess( Form $form, \stdClass $values ): void
  {
    
    

    // If the request is OK, proceed
    if ( $this->requestManager->requestIsValid( $values->request_id ) ) {

      // Load the request
      $request = $this->requestManager->getRequest( $values->request_id );

      // Get the request data
      $request_data = json_decode( $request->data );

      // If the code is OK...
      if ( $values->code === $request_data->code ) {

        // Delete the request
        $this->requestManager->deleteRequest( $values->request_id );

        // Add user the cerified role
        $this->userManager->addUserRole( $request->user_id, 2 );

        // Log the success
        $this->logManager->addLog( 2, $request->user_id, null, ["email" => $request_data->email]);

        // Flash success
        $this->flash("success","actions.user_email_verification.response.success", true);

        $this->redirect( ":Front:Static:default" );

        

      } 
      // ... if the code is not OK
      else {

        $form[ "code" ]->addError( $this->_t( "actions.user_email_verification.response.err.wrong" ) );

      }

    } 
    // If the request is not valid, delete it and redirect
    else {

      $this->flash( "danger", "actions.user_email_verification.response.err.timeout" );

      $this->redirect( "Admin:User:settings" );
    }

  }






  // LOGIN FORM

  /**
   * Sign in form component
   */
  protected function createComponentLoginForm(): BootstrapForm
  {

    $form = new BootstrapForm;
    // $form->renderMode = RenderMode::SIDE_BY_SIDE_MODE;	
    
    $form->addText( 'email' )
      ->setPlaceholder( $this->_t( "sign.login.form.email.name" ) )
      ->setRequired( $this->_t( "sign.login.form.email.required" ) );

    $form->addPassword( 'password' )
      ->setPlaceholder( $this->_t( "sign.login.form.password.name" ) )
      ->setRequired( $this->_t( "sign.login.form.password.required" ) );

    $form->addSubmit( 'send', $this->_t( "sign.login.form.submit" ) );

    $form->onSuccess[] = [ $this, 'signInFormSucceeded' ];

    return $form;

  }

  /**
   * Sign in form succeeded
   */
  public function signInFormSucceeded( Form $form, \stdClass $values ): void
  {

    try {

      $this->getUser()->login( $values->email, $values->password );
      $this->redirect( ":Front:Static:default" );

    } catch ( Nette\Security\AuthenticationException $e ) {
      $form['email']->addError( $this->_t( "sign.login.success.error" ) );
      $form['password']->addError( $this->_t( "sign.login.success.error" ) );
    }

  }

  public function createComponentUserRegistrationForm(): BootstrapForm
  {
    $form = $this->userRegistrationFormFactory->create();

    return $form;
  }




  // LOGOUT

  /**
   * Logout a user action
   * Triggered by logout.latte
   */
  public function actionLogoutUser(): void 
  {
    $this->getUser()->logout( true );
    $this->flashMessage("Uživatel byl odhlášen");
    $this->redirect(":Front:Static:default");
  }



  // REQUEST

  /**
   * Password request form
   */
  public function createComponentPasswordRequestForm(): Form
  {

    $form = new BootstrapForm;
    // $form->renderMode = RenderMode::SIDE_BY_SIDE_MODE;

    $form->addText( 'email' )
      ->setPlaceholder( $this->_t( "sign.request.form.email.name" ) )
      ->setRequired( $this->_t( "sign.request.form.email." ) )
      ->addRule( $form::EMAIL );

    $form->addProtection();

    $form->addSubmit( 'send', $this->_t( "sign.request.form.submit" ) );

    $form->onSuccess[] = [ $this, 'userPasswordRequestSuccess' ];

    return $form;

  }

  public function userPasswordRequestSuccess( Form $form, \stdClass $values ): void 
  {

    // Check user existence
    try {

      // retrieve the ID
      $id = $this->userManager->getUserId( $values->email );
      
      // Create a unique restoration request 
      $request = $this->requestManager->addRequest(
        true,
        3,
        $id, 
        "+2 days"
      );

      $date_format = $this->_t( "date.datetime.reversed" );
      $time_to = $request->time_to->format( $date_format );

      $time_from = $request->time_from->format( $date_format );

      // Send the email
      $mail = $this->mailer->createEmail( "userPasswordRestore.latte", [
        "name" => $this->userManager->getUserName( $id ),
        "hash" => $request->hash,
        "from" => $time_from,
        "to" => $time_to
      ] );

      $mail->addTo( $values->email );

      $this->mailer->sendEmail( $mail );

      $this->flash( 
        "success",  
        $this->_t( "actions.user_password_recovery.request.sent", [
          "email" => $values->email
        ] )
      );

    } 
    // The user was not found
    catch (\App\Model\MissingItemException $e) {
      
      $form["email"]->addError( 
        $this->formatStateMessage( "user", "does_not_exist", $values->email ) 
      );
    
    }
    // The email was not sent 
    catch ( \Nette\Mail\FallbackMailerException $e ) {

      $this->flash( "danger", "Email se nepodařilo odeslat" );

      \Tracy\Debugger::barDump( $mail->body );

    } 
    // Invalid email format
    catch ( \App\Model\InputValidationException $e ) {
    
      $form["email"]->addError( 
        $this->_t( "common.error.not_valid_email" )
       );
    
    }

  }


  // RESTORE PASSWORD
  public function createComponentRestoreForm( ): Form
  {

    $form = new BootstrapForm;
    // $form->renderMode = RenderMode::SIDE_BY_SIDE_MODE;

    $form->addHidden( 'id' );

    $form->addHidden( 'request_id' );

    $form->addPassword( 'password' )
      ->setPlaceholder( $this->_t( "sign.restore.form.password.name" ) )
      ->setRequired( $this->_t( "sign.restore.form.password.required" ) )
      ->addRule( $form::PATTERN, $this->_t( "sign.restore.form.password.digit" ), '.*[0-9].*' )
      ->addRule( $form::PATTERN, $this->_t( "sign.restore.form.password.minor" ), '.*[a-z].*' )
      ->addRule( $form::PATTERN, $this->_t( "sign.restore.form.password.major" ), '.*[A-Z].*' );

    
    $form->addPassword( 'password_repeat' )
      ->setPlaceholder( $this->_t( "sign.restore.form.password_repeat.name" ) )
      ->setRequired($this->_t( "sign.restore.form.password.required" ));
    
    $form->addProtection();

    $form->addSubmit( 'send', $this->_t( "sign.restore.form.submit" ) );

    $form->onSuccess[] = [ $this, 'userPasswordChangeFormSuccess' ];

    return $form;

  }

  public function userPasswordChangeFormSuccess( Form $form, \stdClass $values ): void 
  {

    try {

      // Change the password
      $this->userManager->setUserPassword( $values->id, $values->password, $values->password_repeat );

      // If the change succeeded ... 

      // Delete the existing user request
      $this->requestManager->deleteRequest( $values->request_id );

      // Add the user the verified role
      $this->userManager->addUserRole( $values->id, 2 );

      $flash = $this->translator->translate(
        "sign.restore.success",
        [
        "name" => $this->userManager->getUserName( $values->id )
      ]);

      // Show the success message
      $this->flash( "success", $flash );

      // Redirect to homepage
      $this->redirect( ":Front:Sign:login" );

    } catch ( \App\Model\MissingItemException $e ) {

      // If user does not exist, redirect to homepage
      $this->flashResourceState( "danger","user","does_not_exist" );

      $this->redirect( ":Front:Static:default" );
    

    } catch ( \App\Model\InputValidationException $e ) {

      // If there was something with the form values ... 
      $form["password_repeat"]->addError( $this->_t( "sign.restore.form.password_repeat.notMatch" ) );

    }    

  }



  // REGISTER
  public function createComponentRegisterForm(): Form
  {

    $form = $this->userRegistrationFormFactory->create();

    return $form;
  }

  public function registerFormSuccess( Form $form, \stdClass $values ): void
  {

    \Tracy\Debugger::barDump( "presenter" );

    // If the user did not consent, force a refresh
    if (!$values->consent) {
      $this->flash( "danger", "sign.register.consent_error", true );
      $this->redirect( "this" );
    }

    if ( ! $form->isValid() ) {
      return;
    }

    try {

      $this->database->beginTransaction();

      // Create the user
      $user_id = $this->userManager->addUser( $values->email, 0 );

      // Set his name
      $this->userManager->setUserName( $user_id, $values->name, $values->surname );

      // Give the user the member role
      $this->userManager->addUserRole( $user_id, 1 );

      // Add the new request
      $request = $this->requestManager->addRequest( true, 0, $user_id, "+2 days", null, ["email" => $values->email] );

      // Add the log input
      $log = $this->logManager->addLog( 0, $user_id, null, ["email"=>$values->email] );



      // PErform the main DB operation
      $this->database->commit();

      // Flash the success
      $this->flashResourceState("success", "user","created",$values->email );

            // Create a confirmation request
            $request = $this->requestManager->addRequest( false, 0, $user_id, "+2 days", null, [ "email" => $values->email ] );

            // Send the email
      
            $data = [
              "email" => $values->email,
              "time" => $request->time_from,
              "hash" => $request->hash
            ];
      
            $message = $this->mailer->createEmail( "userRegisteredSelf.latte", $data );
      
            $this->mailer->sendEmail( $message );

            \Tracy\Debugger::barDump( $this->mailer );
      



      // Emaile here
            /*
      try {

        // Expiration date
        $date = new DateTime("+2 days");

        // Create the email
        $email = $this->mailer->createEmail(
          "userRegisteredSelf.latte",
          [
            "email" => $values->email,
            "name" => $values->name . " " . $values->surname,
            "time" => $date->format( $this->_t( "date.datetime.standard" ) ),
            "hash" => $request->hash
          ]
        )
            ->setFrom( "LabIR Edu <test@labir.cz>" )
            ->addTo( 
              $values->email, 
              implode( " ", [ $values->name, $values->surname ] ) 
            )
            ->addBcc( "info@labir.cz" );

        // Send the email
        $this->mailer->sendEmail( $email );

        // Confirm sending success
        $this->flash( "info", "sign.register.success", true );

      } catch (Nette\Mail\FallbackMailerException $e) {

        $this->flash( "danger", $request->hash );

      }

      */


      // Redirect
      $this->redirect( ":Front:Static:default" );

    } catch (Nette\Mail\FallbackMailerException $e) {

      $this->flash( "danger", $request->hash );

    } catch ( \App\Model\InputValidationException $e ) {

      $this->database->rollback();
      $form["email"]->addError( $this->_t( "common.error.not_valid_email" ) );

    } catch ( \App\Model\DupliciteItemException $e ) {

      $this->database->rollback();
      $form["email"]->addError( $this->formatStateMessage( "user", "already_exists", $values->email ) );

    }
  }




  // CONTROLS





  /**
   * Control if a user is logged out, 
   * else redirect him to login.
   * 
   * @param string $redirect
   */
  private function userMustBeLoggedOutCheck( string $redirect = ":Front:Sign:logout" ): void
  {

    if ( $this->getUser()->isLoggedIn() ) {

      $this->flashMessage( $this->_t("sign.check.errorIsLoggedIn") );

      $this->redirect( $redirect );

    }

  }

  /**
   * Control if a user is logged in, 
   * else kick him off.
   * 
   * @param string $redirect
   */
  private function userMustBeLoggedInCheck( string $redirect = ":Front:Sign:login" ): void
  {

    if ( ! $this->getUser()->isLoggedIn() ) {

      $this->flashMessage( $this->_t("sign.check.errorIsLoggedOut") );

      $this->redirect( $redirect );

    }

  }

}
<?php

namespace App\FrontModule\Presenters;

use Nette;
use App\FrontModule\Presenters\BaseFrontPresenter;
use Contributte\MenuControl\UI\MenuComponent;
use Contributte\MenuControl\UI\IMenuComponentFactory;

final class DocumentationPresenter extends BaseFrontPresenter
{

  /** @var \Contributte\MenuControl\UI\IMenuComponentFactory @inject */
  public $menuFactory;

  public function beforeRender(): void
  {

    parent::beforeRender();

    $this->useLayout( "@documentation.latte" );
  }

  /**
   * Documentation menu component
   */
	protected function createComponentMenuDocumentation(): MenuComponent
	{
		return $this->menuFactory->create('documentation');
  }

}
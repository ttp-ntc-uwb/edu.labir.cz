<?php

namespace App\FrontModule\Presenters;

use Nette;
use Nette\Mail\SendmailMailer;
use App\Model\TaxonomyManager;
use Nette\Mail\Message;
use Nette\Application\UI\Form;

use App\BaseModule\Components\Karel;
use Contributte\FormsBootstrap\BootstrapForm;

class StaticPresenter extends BaseFrontPresenter {



	/** @var TaxonomyManager @inject */
	public $taxonomyManager;

  public function startup(): void
  {
    parent::startup();

		$this->useLayout( "@simple.latte" );

		$this->template->titleSuffix = true;

  }

  public function renderDefault(): void
  {

		$this->template->experiments = $this->aggregateExperimentsRandom( 4,! $this->getUser()->isLoggedIn(), [], [ 0 ] );



  }

  public function renderSoftware(): void
  {

    // Add Analyzy
    $analyzy = $this->createContentBlock( new Karel, "analyzy" )
      ->setTitle($this->translator->translate("front.software.analysis.title"))
      ->addParagraph($this->translator->translate("front.software.analysis.paragraph"),"font-style__large")
      ->setImage("/static-content/software/analyzy.jpg");
    
    if ( $this->getUser()->isLoggedIn() ) {
      $analyzy->addButton($this->translator->translate("front.software.analysis.buttons.true"),"btn-text","/static-content/software/setup_labir_1-2.exe");
    } else {
		$this->flash("info","front.software.register_prompt",true);
    	$analyzy->addButton($this->translator->translate("front.software.analysis.buttons.false"),"btn-outline-text",$this->link(":Front:Sign:login"));
    }

    $this->addContent( $analyzy );

    // Add Mereni
    $this->template->content[] = $this->createContentBlock( new Karel, "display" )
      ->enableAos()
      ->setImage("/static-content/software/trend.jpg")
      ->setTitle($this->translator->translate("front.software.line.title"), "font-style__page-name","h1")
      ->addParagraph($this->translator->translate("front.software.line.paragraph"),"font-style__large")
      ->setRtl();

    // Add Export
    $this->template->content[] = $this->createContentBlock( new Karel, "export" )
        ->setTitle($this->translator->translate("front.software.export.title"))
        ->addParagraph($this->translator->translate("front.software.export.paragraph"),"font-style__large")
        ->setImage("/static-content/software/export.jpg");


  }

  public function renderSoutez(): void
  {
	  $this->template->experiments = $this->aggregateExperimentsByTaxonomy(18,$this->getUser()->isLoggedIn(), [ $this->taxonomyManager::TYPE_LABEL, $this->taxonomyManager::TYPE_SUBJECT ]);
  }

  public function renderAbout(): void
  {
    $this->createContentBlock( new Karel, "about" )
      ->setTitle("O nás","font-style__page-name","h1")
      ->addParagraph("Naším cílem je dostat do škol a vzdělávacích center netradiční pomůcku, se kterou mohou studenti kreativně pracovat a zábavně objevovat okem neviditelné rozměry školních předmětů.","font-style__large")
      ->addButton("Podpořte nás!","btn-outline-text",$this->link("Static:zapojit"))
      ->setVideo("/static-content/video-sauron/clenem-tymu.mp4")
      ->setClip(4);
  }

  public function renderReference(): void
	{
		$this->template->testimonials = array(
			array(
				"name" => "Mgr. Kateřina Lipertová",
				"function" => array(
					"Učitelka fyziky a matematiky na Církevním gymnáziu Plzeň",
					"Vedoucí Plzeňského regionálního centra Elixíru do škol."),
				"statement" => array(
					"Termokamery postavené přesně tak, jak je pro žáky třeba, jednoduché a přehledné ovládání, nezničitelné. A samozřejmě zajímavé. Hned je to vtáhne do akce. Využití především ve fyzice (ale možná to tak vidím, protože jsem fyzik). Ale i ve fyzikální hodině bych se nebála chemické nebo biologické odbočky. A nepochybuji, že děti kreativně vymyslí něco, co vás (nás) ještě nenapadlo."),
				"image" => "Katka_Lipertova.jpg"
			),
			array(
				"name" => "Mgr. David Bílek",
				"function" => array(
					"Odborný konzultant v centru celoživotního vzdělávání Eduteam.",
					"Pracoval jako odborný konzultant Nakladatelství Fraus. Od roku 2015 je certifikovaným Google lektorem."
				),
				"statement" => array(
					"V naší pojízdné polytechnické laboratoři EDUbus se pokoušíme školám ukazovat nejmodernější technologie využitelné ve výuce. Vedle digitálních mikroskopů a třeba oblíbené robotiky máme ve výbavě i termokamery. Zejména v posledním době je o prezentace tohoto zařízení a programů s ním spojených velký zájem. Mimo jiné máme ve vybavení termokamery, které vznikly na plzeňské univerzitě a jsou určené přímo pro výuku. Kamery LabIR využíváme jak na ukázku standardní práce (hledání teplých a studených míst, ukázka tepelných mostů či prostupnost materiálů pro tepelné záření), tak se snažíme o didakticko-motivační aktivity (ukázka zjištění tepelné vodivosti materiálů, potvrzení zákonu odrazu pro tepelné záření apod.)."),
				"image" => "David_Bilek.jpg"
			),
			array(
				"name" => "Mgr. Ing. Andrea Tláskalová",
				"function" => array( 
					"Učí na na prvním stupni ZŠ J. V. Sládka Zbiroh.",
					"Získala 2. místo ceny pro inspirativní učitele - Global Teacher Prize Czech Republic 2017.",
					"Koordinuje program GLOBE, Ekoškola a Les ve škole",
					"Spolupracuje se vzdělávacím centrem TEREZA."
				),
				"statement" => array(
					"Využití termokamer ve školách je skvělá příležitost obohatit výuku o nový úhel pohledu na dosud bádané jevy. Otevíráme tím žákům a studentům spoustu příležitostí pro bádání s technikou v ruce a to má na žáky, studenty i učitele vždy ohromný motivační účinek. Termokamery jsem vyzkoušela při lektorování na učitelích a ohlas byl ohromný!"
				),
				"image" => "Andrea_Tlaskalova.jpg"
			),
			array(
				"name" => "PhDr. Jitka Soukupová",
				"function" => array(
					"Působí na gymnáziu ve Stříbře.",
					"Vede kluby malých debrujárů, připravuje metodické materiály.",
					"Je jedním z organizátorů, zadavatelů a hodnotitelů mezinárodní soutěže Pohár vědy – Science Cup."
				),
				"statement" => array(
					"Termokamera má proti jiným kamerám výrazně větší displej, její ovládání je opravdu jednoduché a intuitivní a termosnímky je možné ukládat jako jpg. obrázky na flesh disk. Všechny tyto její parametry zvyšují její hodnotu pro zatraktivnění výuky přírodovědných a technických předmětů.",
					"Již nyní s kolegyní vymýšlíme, jaké aktivity pro naše žáky s kamerou připravíme, a to jak během výuky, tak během badatelských klubů a musím říct, že se samy těšíme jako malé děti. Termokamera nám dává obrovské možnosti propojení teorie s praxí, a to jak u starších žáků v biologii a chemii, tak i u mladších žáků v přírodovědě."
				),
				"image" => "Jitka_Soukupova.jpg"
			),
			array(
				"name" => "Mgr. Vladimír Vochozka, Ph.D.",
				"function" => array(
					"Získal ocenění ČEZ Ámos - nejoblíbenější fyzikář roku 2018.",
					"Pracuje jako odborný asistent na Katedře aplikované fyziky a techniky Jihočeské univerzity v Českých Budějovicích.",
					"Učí také na Základní škole v Plané nad Lužnicí předmět fyzika, výpočetní technika, tisk 3D a fyzikální experimenty."
				),
				"statement" => array(
					"Po absolvování studia učitelství a nástupu na základní školu v Plané nad Lužnicí jsem využil možnosti zapůjčit si termokameru z katedry aplikované fyziky a techniky pedagogické fakulty JU. Cena zařízení byla v té době pro školní rozpočet neakceptovatelná, ale přínos v možnosti sledovat změnu rozložení povrchové teploty tělesa téměř v reálném čase či vyvarování se problému relaxační doby teploměru mě velice nadchla.",
					"Postupem času ceny techniky klesaly, a tak jsem při výuce termiky v praktiku školních pokusů na vysoké škole volil termokameru jako samozřejmost, její použití je přínosné i v mechanice či elektromagnetismu, kde lze pozorovat „ztráty energie“ nebo její přeměny. Další z výhod bylo dynamické pozorování umožňující sledovat jev soustavně, bez přerušení po celou dobu experimentu. Také neovlivňování termodynamické rovnováhy vkládáním tělesa s vlastní tepelnou kapacitou jsem vnímal jako jednoznačný posun v přesnosti měření.",
					"Poslední školní rok jsem při výuce na střední průmyslové škole automobilní a technické se studenty učebních oborů využil možnosti termokamery LabIR v analýze změny teploty určité oblasti tělesa. Žáky bylo vyzdvihováno vyvarování se očekávání stejné hodnoty teploty pro celý povrch na základě jedné hodnoty z teploměru.",
					"Možnost nového pohledu na známé i neznámé situace byl na všech typech škol motivačním prvkem. Použití termokamery vedlo k hlubšímu porozumění probíraného učiva a také propojení souvislostí mezi jednotlivými přírodními jevy."
				),
				"image" => "Vladimir_Vochozka.jpg"
			),
		);
	}

	/**
	 * Contact form component
	 */
	protected function createComponentContactForm()
	{

		$form = new BootstrapForm();

		$form->addText("name")
			->setPlaceholder($this->translator->translate("front.contact.form.name"))
			->addRule(Form::FILLED, $this->translator->translate("front.contact.form.errors.noName") );
		
		$form->addText('email')
			->setPlaceholder($this->translator->translate("front.contact.form.email"))
			->addRule(Form::FILLED,$this->translator->translate("front.contact.form.errors.noEmail") )
			->addRule(Form::EMAIL,$this->translator->translate("front.contact.form.errors.wrongEmail") );
		
		$form->addTextarea("message")
			->addRule(Form::FILLED,$this->translator->translate("front.contact.form.errors.noMessage") );
		
		$form->addSubmit('send',$this->translator->translate("front.contact.form.send"));

		$form->addProtection();
		$form->onSuccess[] = [$this,'processContactForm'];

		return $form;

	}

	/**
	 * Contact form handler
	 */
	public function processContactForm( Form $form ){
		
		$values = $form->getValues( true );

		try {

			$message = new Message;

			$message->addTo( 'info@labir.cz' )
				->setFrom( $values['email'] )
				->setSubject( "Zpráva z kontaktního formuláře" )
				->setBody( $values["message"] );
			
			$mailer = new SendmailMailer;
			$mailer->send($message);

			$this->flashMessage( $this->translator->translate("front.contact.form.success") );
			

		} catch ( \Nette\Mail\SendException $e ) {

			$form->addError( $this->translator->translate( "front.contact.form.failed" ) . " info@labir.cz." );

		}

		
	}

}
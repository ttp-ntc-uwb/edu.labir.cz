<?php

namespace App\Services;

use Contributte\MenuControl\IMenuItem;
use Contributte\MenuControl\LinkGenerator\ILinkGenerator;

use Nette\Application\LinkGenerator;

final class MenuLinkGenerator implements ILinkGenerator
{

  /**
	 * @var LinkGenerator
	 */
  private $linkGenerator;
  
  public function __construct(
    LinkGenerator $linkGenerator
  ){
    $this->linkGenerator = $linkGenerator;
  }


	public function link(IMenuItem $item): string
	{
		return generateLink($item);
	}
	
}
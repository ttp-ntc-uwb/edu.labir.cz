<?php

namespace App\Services;

use Contributte\Translation\Translator;

final class Formatter {

  /** @var Translator @inject */
  public $translator;

  public function __constructor( Translator $translator ) 
  {
    $this->translator = $translator;
  }

  /**
   * Basic translator
   */
  public function _( string $translation_key, ?array $data = null ): string
  {
    return $this->translator->translate( $translation_key, $data );
  }


  /**
   * Format a message about a resource state
   * @param string $resource Translation from [scope].resource.[sg/pl]
   * @param string $state Translation from common.was.[action]
   * @param string|null $item Optional item name
   * @param string|null $genre m / f / n (default == m)
   * @param string|null $number sg / pl (default = pl)
   * @param string|null $message Optional, must be translated already.
   * @see `common.cs.neon`
   * @see `common.was...`
   */
  public function resourceState( string $resource, string $state, ?string $item = null, ?string $genre = "m", ?string $number = "sg", ?string $message = null ): string
  {

    $buffer = [];

    // Check if number is valid
    if ( !in_array( $number, ["sg","pl"] ) ) {
      $number = "sg";
    }

    // Check if the genre is valid
    if ( !in_array( $genre, ["m","f","n"] ) ) {
      $genre = "m";
    }

    // Load the article based on number and genre
    $article = $this->_( implode(".",[ "common.art.def", $number , $genre ] ) );
    
    // Store article if not null
    if ($article !== "" ) {
      $buffer[] = $article;
    }

    // Store resource
    $buffer[] = $this->_( $resource . ".res." . $number .".nom" );

    // Eventually, store the resource name
    if ( ! empty( $item ) ) {
      $buffer[] = $item;
    }

    // Store the state
    $buffer[] = $this->_( implode(".",["common.was", $state, $number, $genre ] ) );

    

    $text = implode( " ", $buffer ) . "!";

    // Store the eventual message
    if ( ! empty( $message ) ) {
      $text .= " " . $message;
    }

    // If the item is present, return the present
    return $text;

  }

  /**
   * Format a standardised ACL error:
   * - "You do not have sufficent permissions."
   * - "You can not [$operation] [this] [$resource] [$item]."
   * - "You can not edit Pilsen."
   * - "You can not edit this city."
   * - "You can not edit the city Pilsen."
   * 
   * @param string|null $operation
   * @param string|null $resource
   * @param string|null $item "m" / "f" / "entry name"
   * @return string|null
   * @see common.cs.neon
   */
  public function accessError( ?string $operation=null, ?string $resource=null, ?string $item=null, ?bool $plural = false ): ?string
  {

    // not_allowed
    // !$operation && !$resource
    if ( empty( $operation ) && empty( $resource ) ) {
      return $this->_( "common.error.not_allowed" );
    }

    // init $args
    $args = [
      "op" => strtolower( $this->_( "common.op." . $operation ) )
    ];

    // Format the number
    $number = $plural ? "pl" : "sg";


    // not_allowed_op_item
    // $operation && $item && $item != "m" && $item != "f"
    if (
      !empty( $operation ) 
      && empty( $resource )
      && !empty( $item )
      && $item !== "m"
      && $item !== "f"
    ) {
      return $this->translator->translate("common.error.not_allowed_op_item",array_merge($args,["item"=>$item]));
    }

    if ( !empty( $resource ) ) {
      // Add resource to translation
      $args["res"] = strtolower( $this->_( implode( ".",[$resource,"res",$number,"gen"] ) ) );
    }

    // not_allowed_op_res
    // $operation && $resource && !empty
    if (
      !empty( $operation )
      && !empty( $resource )
      && empty( $item )
    ) {

      $args["res"] = strtolower( $this->_( implode( ".",[$resource,"res.pl.gen"] ) ) );

      return $this->translator->translate( "common.error.not_allowed_op_res", $args );

    }

    // not_allowed_op_res_*
    if (
      !empty( $operation )
      && !empty( $resource )
      && !empty( $item )
    ) {

      // not_allowed_op_res_m
      if ( $item == "m" ) {
        return $this->translator->translate( "common.error.not_allowed_op_res_m", $args );
      }
      // not_allowed_op_res_f
      else if ( $item == "f" ) {
        return $this->translator->translate( "common.error.not_allowed_op_res_f", $args );
      } else {
        return $this->translator->translate( "common.error.not_allowed_op_item_res", array_merge($args,["item"=>$item]) );
      }

    }

    return null;

  }

}
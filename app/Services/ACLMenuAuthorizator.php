<?php

namespace App\Services;

use Nette;

use Contributte\MenuControl\ImenuItem;
use Contributte\MenuControl\Security\IAuthorizator;

final class ACLMenuAuthorizator implements IAuthorizator
{

  /** @var Nette\Security\Permission */
  private $acl;

  /** @var Nette\Security\User */
  private $user;

  public function __construct(
    Nette\Security\Permission $acl,
    Nette\Security\User $user
  ) 
  {
    
    $this->acl = $acl;
    $this->user = $user;

  }

  /**
   * Validation of access based on ACL in ACLSiteAuthorizator
   * @param IMenuItem $item The menu item object
   * @return bool
   */
  private function isItemAllowed( $item ): bool 
  {

    // Default state is true which can be overriden in template
    $can = true;

    // Links to internal actions depend on ACL definition
    if ( $item->getAction() != null ) {

      // Data to be retrieved
      $resource = false;
      $action = false;

      // Retrieve action parts
      $pieces = array_map(function($item){
        return strtolower( $item );
      }, explode( ":", $item->getAction() ) );      

      // Three pieces == links to a module page
      if ( count( $pieces ) == 3 ) {
        $resource = $pieces[1];
        $action = $pieces[2];
      }

      // Two pieces == links to a standard page with an action
      if ( count( $pieces ) == 2 ) {
        $resource = $pieces[0];
        $action = $pieces[1];
      }

      // One piece = links to a standard presenter default action
      if ( count( $pieces ) == 1 ) {
        $resource = $pieces[0];
        $action = "default";
      }


      $can = $this->user->isAllowed( $resource, $action );

    }    
    
    return $can;

  }

  /**
   * Hook called automatically during menu rendering
   */
  public function isMenuItemAllowed( IMenuItem $item ): bool
  {
    return $this->isItemAllowed( $item );
  }

}
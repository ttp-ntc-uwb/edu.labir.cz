<?php

namespace App\Services;

use Nette;
use Nette\Security\SimpleIdentity;

class SiteAuthenticator implements Nette\Security\Authenticator
{
  
  private $database;
  private $passwords;
  private $userManager;

  public function __construct(
    Nette\Database\Explorer $database,
    Nette\Security\Passwords $passwords,
    \App\Model\UserManager $userManager
  )
  {

    $this->database = $database;
    $this->passwords = $passwords;
    $this->userManager = $userManager;

  }

  public function authenticate( string $email, string $password ): Nette\Security\IIdentity
  {

    $row = $this->database->table( 'user' )
      ->where( 'email', $email )
      ->fetch();
    
    // If no users of a given username exist
    if ( !$row ) {
      throw new Nette\Security\AuthenticationException( 'User not found' );
    }

    // Validate the password
    if ( !$this->passwords->verify( $password, $row->password ) ) {
      throw new Nette\Security\AuthenticationException( 'Invalid password' );
    }

    return new SimpleIdentity(
      $row->id,
      $this->userManager->getUserRolesSlugs( $row->id ),
      [ 'name' => $row->email ]
    );

  }

}
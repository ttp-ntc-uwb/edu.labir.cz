<?php

namespace App\Services;

use Nette;
use Nette\Mail\Message;
use Nette\Mail\FallbackMailer;
use Nette\Mail\SmtpMailer;
use Nette\Mail\SendmailMailer;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Template;
use Nette\Bridges\ApplicationLatte\TemplateFactory;
use Nette\Mail\FallbackMailerException;

/**
 * Implementation of Nette\Mail
 * 
 * Faclitates template lookup and SMTP sending.
 * 
 * - `createEmail( $template, $params ): Nette\Mail\Message`
 * - `sendEmail( Nette\Mail\Message $email): void`
 * @link https://doc.nette.org/cs/3.1/mailing
 */
class Mailer
{
	/** @var LinkGenerator */
	private $linkGenerator;

	/** @var TemplateFactory */
	private $templateFactory;

	public function __construct(
		LinkGenerator $linkGenerator,
		TemplateFactory $templateFactory
	)
	{
		$this->linkGenerator = $linkGenerator;
		$this->templateFactory = $templateFactory;
	}

	private function createTemplate(): Template
	{
		$template = $this->templateFactory->createTemplate();
		$template->getLatte()->addProvider('uiControl', $this->linkGenerator);
		return $template;
	}

	public function createEmail( string $template_name, ?array $params = [] ): Message
	{
		$template = $this->createTemplate();
		$html = $template->renderToString(
      $this->getTemplatePath( $template_name ), 
      $params
    );

		$mail = new Message;
		$mail->setHtmlBody($html);
		$mail->setFrom( "info@labir.cz", "LabIR Edu" );
		return $mail;
	}

	/**
	 * @throws Nette\Mail\FallbackMailerException
	 */
  public function sendEmail( Message $mail ): void
  {
    $mailer = new FallbackMailer([
      // new SmtpMailer([
      //  'host' => 'smtp.cesky-hosting.cz',
      //  'username' => 'test@labir.cz',
      //  'password' => 'LyiSIQRc',
      //  'secure' => 'ssl',
      // ]),
      new SendmailMailer
    ]);

    $mailer->send( $mail );

		\Tracy\Debugger::$maxLength = 1000;
		\Tracy\Debugger::barDump( $mail->body, "Mailer odeslal toto:" );
  }


  private function getTemplatePath( string $template_name ): string
  {
    return __DIR__ . "/../BaseModule/Presenters/templates/Emails/" . $template_name;
  }

}
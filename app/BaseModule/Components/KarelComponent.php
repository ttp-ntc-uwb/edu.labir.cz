<?php

namespace App\BaseModule\Components;

// use Nette\Application\UI\Control;
use App\BaseModule\Components\BaseComponent;
use Nette\Application\LinkGenerator;
use Nette\Schema\Expect;
use Nette\Schema\Processor;
use Nette\Schema\ValidationException;

final class Karel extends BaseComponent
{

  public const NAME = "karel";

  /** @var string $title */
  private $title;

  /** @var string $titleClass */
  private $titleClass;

  /** @var string $titleElement */
  private $titleElement;

  /** @var array $paragraphs */
  private $paragraphs;

  /** @var array $buttons */
  private $buttons;

  /** @var bool $ltr */
  private $ltr;

  /** @var bool $aos */
  private $aos;

  /** @var bool $imageFirst */
  private $imageFirst;

  /** @var int|null $clip */
  private $clip;

  /** @var string $breakpoint */
  private $breakpoint;

  /** @var string $mediaHref */
  private $mediaHref;

  /** @var bool $isVideo */
  private $isVideo;

  public function __construct()
  {
    $this->init();

  }

  public function injectLinkGenerator(): void
  {

  }

  private function init(): void
  {
    
    $this->title = "";
    $this->titleClass = "font-style__page-name";
    $this->titleElement = "h2";
    $this->paragraphs = [];
    $this->buttons = [];
    $this->ltr = true;
    $this->aos = false;
    $this->imageFirst = false;
    $this->clip = null;
    $this->breakpoint = "lg";
    $this->mediaHref = "";
    $this->isVideo = false;
  }

  /**
   * Set the main title of Karel
   * @param string $title
   * @return Karel
   */
  public function setTitle( string $title, string $class="font-style__page-name", string $element = "h2" ): Karel
  { 
    $this->title = $title; 
    $this->titleClass = $class;
    $this->titleElement = $element;
    return $this;
  }

  /**
   * Add a button
   * @param string $text
   * @param string $class Do not include the btn...
   * @param string $link Presenter or URL
   * @param string|bool $target
   * @return Karel
   */
  public function addButton( string $text, string $class, string $link, $target = false ): Karel
  {
    $this->buttons[] = [
      'text' => $text,
      'class' => $class,
      'link' => $link,
      'target' => $target
    ];
    return $this;
  }

  /**
   * Add a paragraph
   * @param string text
   * @param string|null class
   * @return Karel
   */
  public function addParagraph( string $text, string $class="" ): Karel
  { 
    $this->paragraphs[] = [
      'text' => $text,
      'class' => $class
    ]; 
    return $this;
  }

  /**
   * Image on the right
   * @return Karel
   */
  public function setLtr(): Karel
  { 
    $this->ltr = true; 
    return $this;
  }

  /**
   * Image on the left
   * @return Karel
   */
  public function setRtl(): Karel
  { 
    $this->ltr = false; 
    return $this;
  }

  /**
   * Set the clip path number
   * @param int clip
   * @return Karel
   */
  public function setClip( int $clip = 1 ): Karel 
  { 
    $this->clip = $clip; 
    return $this;
  }

  public function enableAos(): Karel
  {
    $this->aos = true;
    return $this;
  }

  public function disableAos(): Karel
  {
    $this->aos = false;
    return $this;
  }

  /**
   * Image shall go first on mobile
   * @return Karel
   */
  public function imageGoesFirst(): Karel
  {
    $this->imageFirst = true;
    return $this;
  }

  /**
   * Image shall go last on mobile
   * @return Karel
   */
  public function imageGoesLast(): Karel
  {
    $this->imageFirst = false;
    return $this;
  }

  /**
   * Set the collapse breakpoint
   * @param string $bp
   * @return Karel
   */
  public function setBreakpoint( string $bp ): Karel
  {
    if ( in_array($bp,["xs","md","lg","xl","xxl","xxxl"]) ) {
      $this->breakpoint = $bp;
    }
    return $this;
  }

  /**
   * Set main image
   * @param string $path
   */
  public function setImage( string $path ): Karel
  {
    $this->mediaHref = $path;
    $this->isVideo = false;
    return $this;
  }
  /**
   * Set media video
   * @param string $path
   * @param string|null $thumbnail_path
   */
  public function setVideo( string $path, string $thumbnail_path = null ): Karel
  {
    $this->mediaHref = $path;
    $this->isVideo = true;
    return $this;
  }

  /**
   * Assign data to the template object
   */

  private function populateTemplate(): void
  {

    $this->template->title = $this->title;
    $this->template->titleClass = $this->titleClass;
    $this->template->title = [
      "text" => $this->title,
      'class' => $this->titleClass,
      'element' => $this->titleElement
    ];
    $this->template->paragraphs = $this->paragraphs;
    $this->template->buttons = $this->buttons;

    $this->template->aos = $this->aos;

    // Fill the classes

    $classes = [];

    $classes[] = $this->imageFirst ? "t-karel__imagefirst" : "t-karel__textfirst";

    $classes[] = $this->ltr ? "t-karel__ltr" : "t-karel__rtl";

    $classes[] = "t-karel__collapse__" . $this->breakpoint;

    if ( $this->clip != null ) {
      $classes[] = "t-karel__in-" . $this->clip;
    }

    $this->template->classes = implode( " ", $classes );

    // Fill the media

    $this->template->media = [
      'path' => $this->mediaHref,
      'video' => $this->isVideo
    ];

  }

  /**
   * Process the input array and return formatted object
   * @param array $data
   * @return \stdClass|null
   */
  private function validateInputData( array $data ): ?\stdClass
  {
    $schema = Expect::structure([
      'title' => Expect::string(),
      'titleClass' => Expect::string("font-style__page-name"),
      'paragraphs' => Expect::listOf(
        Expect::structure([
          'text' => Expect::string()->required(),
          'class' => Expect::string(),
        ])
      ),
      'buttons' => Expect::listOf(
        Expect::structure([
          'text' => Expect::string()->required(),
          'link' => Expect::string(),
          'class' => Expect::string("btn-outline-text"),
          'target' => Expect::string("")
        ])
      ),
      'ltr' => Expect::bool(true),
      'imageFirst' => Expect::bool(true),
      'clip' => Expect::int()->min(0)->max(5),
      'breakpoint' => Expect::string("lg")
    ]);

    $processor = new Processor;
    
    // Validate the data structure
    try {

      return $processor->process($schema,$data);

    } catch( ValidationException $e ){
      
      $this->flashMessage( $e->getMessage() );
      return null;

    }
    
  }

  /**
   * Assign processed values to the component
   * @param \stdClass $input
   * @return void
   */
  public function setInputData( \stdClass $input ): void
  {

    if ( $input != null ) {

      $this->setTitle( $input->title, $input->titleClass );

      foreach ( $input->paragraphs as $paragraph ) {

        $this->addParagraph( $paragraph->text, $paragraph->class );

      }

      foreach ( $input->buttons as $button ) {

        $this->addButton( $button->text, $button->class, $button->link, $button->target );

      }

      $this->ltr = $input->ltr;
      $this->imageFirst = $input->imageFirst;
      $this->clip = $input->clip;
      $this->breakpoint = $input->breakpoint;

    }

  }


  /**
   * The main render method
   * @return void
   */
  public function render( array $data = []): void
  {

    \Tracy\Debugger::barDump( $this );

    // If data are provided, fill the component
    if ( count( $data ) > 0 ) {

      // Validate the data structure
      $processed = $this->validateInputData( $data );

      // Set the data to this component
      $this->setInputData( $processed );

    }

    // Format the data for the 
    $this->populateTemplate();

    // Render the template
    $this->template->render(__DIR__ . "/karel.latte");

  }




}
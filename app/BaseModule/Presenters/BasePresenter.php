<?php

/**
 * The basic presenter for the entire app.
 * 
 * Extended by:
 * @see \App\FrontModule\BaseFrontPresenter
 * @see \App\AdminModule\BaseAdminPresenter
 */

namespace App\BaseModule\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use Nette\Application\UI\Control;
use Contributte\MenuControl\UI\MenuComponent;
use Nette\Database\Explorer;
use Nette\Security\Permission;
use Nette\Localization\ITranslator;
use Contributte\MenuControl\UI\IMenuComponentFactory;

use App\Model\UserManager;
use App\Model\LocalityManager;
use App\Model\ExperimentManager;

use App\BaseModule\Components\Karel;

use \stdClass;

class BasePresenter extends Presenter
{


  // Properties and dependencies

  /** @var Explorer @inject */
  public $database;

  /** @var Permission @inject */
  public $acl;

  /** @var UserManager @inject */
  public $userManager;

  /** @var ExperimentManager @inject */
  public $experimentManager;

  /** @var ITranslator @inject */
  public $translator;

  /** @var IMenuComponentFactory @inject */
  public $menuFactory;


  // Following should be removed

  /** @var LocalityManager @inject */
  public $localityManager;



  const ALERT_STATES = [
    "primary","secondary","info","success","warning","danger"
  ];


  protected function beforeRender(): void
  {
    
    // Format the classes output
    $this->template->addFilter('formatClasses', function (array $classes) {
      return implode( " ", $classes );
    });



    /**
     * Flash user verification status
     */
    if ( $this->getUser()->isLoggedin() && ! $this->userManager->userHasRole( $this->getUser()->getId(), 2 ) ) {
      $this->flash( 
        "warning", 
        $this->_t( "actions.user_email_verification.notification", [
          "email" => $this->userManager->getUser( $this->getUser()->getId() )->email
        ] )
      );
    }

  }


  

  

  /**
   * Startup
   */
  public function startup(): void
  {
    
    parent::startup();

    // Initialise default values for the template

    /**
     * The template version
     * @todo should go from NEON
     */
    $this->template->v = \Tracy\Debugger::isEnabled() ? time() : "0.1";

    /**
     * Add classes conteiner for further render
     * @see self::beforeRender()
     * @see self::addClass()
     */

    $this->template->classes = ["t"];

    // Add class for logging in or out
    $this->template->classes[] = $this->getUser()->isLoggedin() ? "t__logged-in" : "t__logged-out";


    /** 
     * Administrative assets
     */
    $this->template->adminAssets = false;

    /**
     * Title content & formating 
     * @see @simple.latte
     */
    $this->template->title = false;
    $this->template->titlePrefix = false; // bool|string
    $this->template->titleSuffix = false; // bool|string

    /**
     * Layout ptoperties
     * @see @simple.latte
     * bool = no HTML entity shall be drawn
     * string = create an entity with the given class
     */
    $this->template->layoutContainer = false;
    $this->template->layoutRow = false;
    $this->template->layoutColumn = false;

    /**
     * Hubspot
     */
    $this->template->hubspot = false;

    /**
     * Initialise the content
     */
    $this->template->content = [];

    /**
     * User name
     */
    $this->template->userRealName = $this->getUser()->isLoggedIn() ? $this->userManager->getUserName( $this->getUser()->getId() ) : "Anonym";

    $this->template->locale = $this->getParameter("locale");

  }

  /**
   * Translate a string
   * @param string $message
   * @param array $data
   * @return string
   */
  public function _t(string $message, array $data = []): string
  {
    return $this->translator->translate( $message, $data );
  }

  /**
   * Translate a string and its argument
   * @param string $string
   * @param string $arg
   * @return string 
   */
  public function _targ( string $string, string $arg ): string
  {
    return $this->translator->translate( $string, [ 
      "arg" => $this->translator->translate( $arg ) 
    ] );
  }

  /**
   * Create a new component and return it
   */
  public function createContentBlock( Control $control, string $name ): Control
  {
    $this->addComponent( $control, $name );
    return $this->getComponent( $name );
  }

  public function addContent( Control $control ): void
  {
    $this->template->content[] = $control;
  }




  // LAYOUT




  /**
   * Set a @layout.latte
   * @param string $file_name 
   * @uses parent::setLayout()
   * @deprecated Why in hell the method setLayout() does not work?
   */
  public function useLayout( string $file_name ): void
  {
    $this->setLayout( __DIR__. "/templates/" . $file_name );
  }






  /**
   * Find local layouts
   * @uses Nette\Utils\Finder
   * @see self::beforeRender()
   * @see Nette\UI\Presenter::formatLayoutTemplateFiles
   */
  public function __formatLayoutTemplateFiles(): array
  {

    $layouts = [];

    foreach (Nette\Utils\Finder::findFiles('@*.latte')->from(__DIR__."/../../layouts") as $key => $file) {
      $email = strpos($key,"@") + 1;
      $length = strlen( $key ) - $email - 6;
      $name = substr( $key, $email, $length );

      $layouts[ $name ] = $key;
      \Tracy\Debugger::barDump( $name );
    }

    return $layouts;

  } 

  
  public function createComponentKarel(): Karel
  {
    return new Karel;
  }





  // MENU RELATED STUFF



  /**
   * Frontpage menu component
   */
	protected function createComponentMenuFront(): MenuComponent
	{
		return $this->menuFactory->create('front');
  }

  /**
   * Administration menu component
   */
  protected function createComponentMenuAdmin(): MenuComponent
	{
		return $this->menuFactory->create('admin');
	}

  /**
   * Social menu component
   */
  protected function createComponentMenuSocial(): MenuComponent
	{
		return $this->menuFactory->create('social');
	}

  /**
   * Partners menu component
   */
  protected function createComponentMenuPartners(): MenuComponent
	{
		return $this->menuFactory->create('partners');
	}

  /**
   * Partners menu component
   */
  protected function createComponentMenuSponsors(): MenuComponent
	{
		return $this->menuFactory->create('sponsors');
	}

  /**
   * Partners menu component
   */
  protected function createComponentMenuUser(): MenuComponent
	{
		return $this->menuFactory->create('user');
	}





  // Messages


  /**
   * This
   * @param string $type
   * @param string $msg
   * @param bool $translate default: false - strings sould already be translated
   * @return void
   * @uses parent::flashMessage()
   */
  public function flash(string $type, string $msg, ?bool $translate = false ): void
  {

    // Check if the type is valie
    if ( !in_array( $type, self::ALERT_STATES ) ) {
        $type = "info";
    }

    // Translate texts if required
    if ( $translate ) {
      $msg = $this->_t( $msg );
    }


    // Flash the message finally
    $this->flashMessage( $msg, "alert-" .$type );
    
  }


  public function flashACLError( ?string $operation=null, ?string $resource=null, ?string $item=null ): void
  {
    $this->flash(
      "danger",
      $this->formatACLMessage( $operation, $resource, $item ),
      false
    );
  }


  /**
   * Standardized & translated `flashMessage()`
   * 
   * - Format `$resource ?$item $state $msg!`
   * - Example `Instituce Bradavice byla smazána s velkou slávou!`
   * 
   * @uses `BasePresenter::formatStateMessage()`
   * 
   * @see `common.was...` - definition of states
   * @see `....cs.neon` - resources definitons
   * 
   * @param string $type info (default) / success / danger / warning
   * @param string $resource Translation from [scope].resource.[sg/pl]
   * @param string $state Translation from common.was.[action]
   *  - created
   *  - deleted
   *  - saved
   *  - updated
   *  - moved
   *  - assigned
   *  - not_found
   *  - does_not_exist
   *  - already_exists
   *  - can_not_be_deleted
   *  - not_valid
   * @param string|null $item Optional item name
   * @param string|null $genre m / f / n (default == m)
   * @param string|null $number sg / pl (default = pl)
   * @param string|null $message Optional, must be translated already.
   * 
   */
  public function flashResourceState( string $type, string $resource, string $state, ?string $item = null, ?string $genre = "m", ?string $number = "sg", ?string $message = null ): void
  {

    // Make sure the type is valid
    if ( !in_array( $type, ["info","success","warning","danger"] ) ) {
      $type = "info";
    }

    // Flash this message
    $this->flash( 
      $type,
      $this->formatStateMessage( $resource, $state, $item, $genre, $number, $message ),
    );

  }



  /**
   * Format a standardized flash message
   * @param string $resource
   * @param string $state
   * @param string|null $name
   * @return string
   * @deprecated use self::formatStateMessage()
   */
  public function formatMessage( string $resource,string $state, string $name = null ): string
  {
    $resource = $this->_t($resource);
    $state = $this->_t($state) . ".";
    return empty( $name ) ? 
      implode(" ",[$resource,$state])
      :
      implode(" ", [$resource,$name,$state]);
  }

  /**
   * Format a standardised ACL error:
   * - "You do not have sufficent permissions."
   * - "You can not [$operation] [this] [$resource] [$item]."
   * - "You can not edit Pilsen."
   * - "You can not edit this city."
   * - "You can not edit the city Pilsen."
   * 
   * @param string|null $operation
   * @param string|null $resource
   * @param string|null $item "m" / "f" / "entry name"
   * @return string|null
   * @see common.cs.neon
   */
  public function formatACLMessage( ?string $operation=null, ?string $resource=null, ?string $item=null, ?bool $plural = false ): ?string
  {

    // not_allowed
    // !$operation && !$resource
    if ( empty( $operation ) && empty( $resource ) ) {
      return $this->_t( "common.error.not_allowed" );
    }

    // init $args
    $args = [
      "op" => strtolower( $this->_t( "common.op." . $operation ) )
    ];

    // Format the number
    $number = $plural ? "pl" : "sg";


    // not_allowed_op_item
    // $operation && $item && $item != "m" && $item != "f"
    if (
      !empty( $operation ) 
      && empty( $resource )
      && !empty( $item )
      && $item !== "m"
      && $item !== "f"
    ) {
      return $this->translator->translate("common.error.not_allowed_op_item",array_merge($args,["item"=>$item]));
    }

    if ( !empty( $resource ) ) {
      // Add resource to translation
      $args["res"] = strtolower( $this->_t( implode( ".",[$resource,"res",$number,"acc"] ) ) );
    }

    // not_allowed_op_res
    // $operation && $resource && !empty
    if (
      !empty( $operation )
      && !empty( $resource )
      && empty( $item )
    ) {

      $args["res"] = strtolower( $this->_t( implode( ".",[$resource,"res.pl.gen"] ) ) );

      return $this->translator->translate( "common.error.not_allowed_op_res", $args );

    }

    // not_allowed_op_res_*
    if (
      !empty( $operation )
      && !empty( $resource )
      && !empty( $item )
    ) {

      // not_allowed_op_res_m
      if ( $item == "m" ) {
        return $this->translator->translate( "common.error.not_allowed_op_res_m", $args );
      }
      // not_allowed_op_res_f
      else if ( $item == "f" ) {
        return $this->translator->translate( "common.error.not_allowed_op_res_f", $args );
      } else {
        return $this->translator->translate( "common.error.not_allowed_op_item_res", array_merge($args,["item"=>$item]) );
      }

    }

    return null;

  }

  /**
   * Format a message about a resource state
   * @param string $resource Translation from [scope].resource.[sg/pl]
   * @param string $state Translation from common.was.[action]
   * @param string|null $item Optional item name
   * @param string|null $genre m / f / n (default == m)
   * @param string|null $number sg / pl (default = pl)
   * @param string|null $message Optional, must be translated already.
   * @see `common.cs.neon`
   * @see `common.was...`
   */
  public function formatStateMessage( string $resource, string $state, ?string $item = null, ?string $genre = "m", ?string $number = "sg", ?string $message = null ): string
  {

    $buffer = [];

    // Check if number is valid
    if ( !in_array( $number, ["sg","pl"] ) ) {
      $number = "sg";
    }

    // Check if the genre is valid
    if ( !in_array( $genre, ["m","f","n"] ) ) {
      $genre = "m";
    }

    // Load the article based on number and genre
    $article = $this->_t( implode(".",[ "common.art.def", $number , $genre ] ) );
    
    // Store article if not null
    if ($article !== "" ) {
      $buffer[] = $article;
    }

    // Store resource
    $buffer[] = $this->_t( $resource . ".res." . $number .".nom" );

    // Eventually, store the resource name
    if ( ! empty( $item ) ) {
      $buffer[] = $item;
    }

    // Store the state
    $buffer[] = $this->_t( implode(".",["common.was", $state, $number, $genre ] ) );

    

    $text = implode( " ", $buffer ) . "!";

    // Store the eventual message
    if ( ! empty( $message ) ) {
      $text .= " " . $message;
    }

    // If the item is present, return the present
    return $text;

  }

  public function formatOperationName( string $operation, ?string $pronoun = null, ?string $resource = null, ?string $item = null, ?string $num = "sg", ?string $genre = "m", ?string $punctuation = "" ): string
  {

    $buf = [];

    // Add operation
    $buf[] = $this->_t( "op.".$operation );

    // Add pronoun
    if ( !empty( $pronoun ) ) {
      $buf[] = $this->_t( implode(".",["op",$pronoun,$num, $genre] ) );
    }

    // Add resource
    if ( !empty( $resource ) ) {
      $buf[] = strtolower( $this->_t( implode( ".", [$resource,"res",$num,"acc"]) ) );
    }
    
    // Add item
    if ( !empty($item) ) {
      $buf[] = $item;
    }

    return implode( " ", $buf ) . $punctuation;

  }

  public function formatOverviewName( ?string $operation = "overview", string $resource, ?bool $my = false, ?string $genre = "m" ): string
  {
    $buf = [];
    // Add the main word
    $buf[] = $this->_t( "op.".$operation );
    // Add the MY pronoun
    if ( $my ) {
      $buf[] = $this->_t( implode(".",["op","my_overview","pl",$genre]) );
    }
    // Add the resource
    $buf[] = strtolower( $this->_t( implode(".",[$resource,"res","pl","gen"]) ));

    return implode( " ", $buf );
  }

  public function getMailTemplatePath( string $file_name ): string
  {
    return __DIR__ . "/templates/Emails/" . $file_name;
  }






  /**
   * Experiment helpers
   */

  /**
   * Loads an experiment for teaser listing along with a specified list of its taxonomies
   * @param $id Experiment ID
   * @param $taxonomies List of taxonomies from $this->taxonomyManager::TYPE_[]
   * @return stdClass The object loaded
   */
  public function aggregateExperimentForThumb( int $id, ?array $taxonomies = [ 3 ] ): stdClass
  {
    $experiment = $this->experimentManager->getExperiment( $id );

    $labels = $this->taxonomyManager->getTaxonomiesOfType( $this->taxonomyManager::TYPE_LABEL );

    $object = new stdClass();
    $object->name = $experiment->name;
    $object->excerpt = $experiment->excerpt;
    $object->thumb = $experiment->thumb;
    $object->public = $experiment->public;
    $object->slug = $experiment->slug;
    $object->taxonomies = [];

    foreach ( $taxonomies as $taxonomy ) {
      switch ( $taxonomy ) {
        case $this->taxonomyManager::TYPE_SUBJECT:
          $object->taxonomies["subjects"] = [
            "name" => "subjects",
            "color" => "gray",
            "items" => $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_SUBJECT )
          ]; break;
        case $this->taxonomyManager::TYPE_FOLDER:
          $object->taxonomies["folders"] = [
            "name" => "folders",
            "color" => "gray",
            "items" => $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_FOLDER )
          ]; break;
        case $this->taxonomyManager::TYPE_LABEL:
          $object->taxonomies["labels"] = [
            "name" => "labels",
            "color" => "gray",
            "items" => $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_LABEL )
          ]; break;
        case $this->taxonomyManager::TYPE_TAG:
          $object->taxonomies["tags"] = [
            "name" => "tags",
            "color" => "gray",
            "items" => $this->taxonomyManager->getExperimentTaxonomiesOfType( $experiment->id, $this->taxonomyManager::TYPE_TAG )
          ]; break;
      }
    }
    $object->labels = $labels;

    return $object;
  }

  public function aggregateExperimentsAll( ?array $taxonomies = [ 3 ] ): array
  {
    $experiments = $this->experimentManager->getFrontendExperiments( ! $this->getUser()->isLoggedIn() );

    $output = [];

    foreach ( $experiments as $experiment ) {
      $output[ $experiment->id ] = $this->aggregateExperimentForThumb( $experiment->id, $taxonomies );
    }

    return $output;
  }

  public function aggregateExperimentsByTaxonomy( int $taxonomy_id , bool $public_only = false, ?array $taxonomies = [ 3 ] ): array
  {
    // Load the taxonomies from the DB
    $experiments = $this->experimentManager->getFrontendExperimentsByTaxonomy( $taxonomy_id, $public_only );

    // Create the output buffer
    $output = [];

    // Iterate the experiments and aggregate its content into it
    foreach ( $experiments as $experiment ) {
      $output[ $experiment->id ] = $this->aggregateExperimentForThumb( $experiment->id, $taxonomies );
    }

    return $output;

  }

  /**
   * Load random thumbnails of experiments for frontend
   * 
   */
  public function aggregateExperimentsRandom( int $limit = 4, bool $public = false, ?array $excluded = [], ?array $taxonomies = [ 3 ] ): array
  {
    $experiments = $this->experimentManager->getFrontendExperimentsRandom( ! $this->getUser()->isLoggedIn(), $limit, $excluded );

    $output = [];

    foreach ( $experiments as $experiment ) {
      $output[ $experiment->id ] = $this->aggregateExperimentForThumb( $experiment->id, $taxonomies );
    }

    return $output;
  }


}